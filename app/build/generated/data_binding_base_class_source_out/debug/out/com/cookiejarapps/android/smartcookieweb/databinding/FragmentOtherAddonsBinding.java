// Generated by view binder compiler. Do not edit!
package com.cookiejarapps.android.smartcookieweb.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.cookiejarapps.android.smartcookieweb.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class FragmentOtherAddonsBinding implements ViewBinding {
  @NonNull
  private final LinearLayout rootView;

  @NonNull
  public final RecyclerView unsupportedAddOnsList;

  private FragmentOtherAddonsBinding(@NonNull LinearLayout rootView,
      @NonNull RecyclerView unsupportedAddOnsList) {
    this.rootView = rootView;
    this.unsupportedAddOnsList = unsupportedAddOnsList;
  }

  @Override
  @NonNull
  public LinearLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static FragmentOtherAddonsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static FragmentOtherAddonsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.fragment_other_addons, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static FragmentOtherAddonsBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.unsupported_add_ons_list;
      RecyclerView unsupportedAddOnsList = ViewBindings.findChildViewById(rootView, id);
      if (unsupportedAddOnsList == null) {
        break missingId;
      }

      return new FragmentOtherAddonsBinding((LinearLayout) rootView, unsupportedAddOnsList);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
