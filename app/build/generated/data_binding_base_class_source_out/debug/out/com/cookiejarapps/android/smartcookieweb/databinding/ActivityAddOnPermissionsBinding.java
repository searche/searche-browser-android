// Generated by view binder compiler. Do not edit!
package com.cookiejarapps.android.smartcookieweb.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.cookiejarapps.android.smartcookieweb.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityAddOnPermissionsBinding implements ViewBinding {
  @NonNull
  private final RelativeLayout rootView;

  @NonNull
  public final RecyclerView addOnsPermissions;

  @NonNull
  public final TextView learnMoreLabel;

  private ActivityAddOnPermissionsBinding(@NonNull RelativeLayout rootView,
      @NonNull RecyclerView addOnsPermissions, @NonNull TextView learnMoreLabel) {
    this.rootView = rootView;
    this.addOnsPermissions = addOnsPermissions;
    this.learnMoreLabel = learnMoreLabel;
  }

  @Override
  @NonNull
  public RelativeLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityAddOnPermissionsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityAddOnPermissionsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_add_on_permissions, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityAddOnPermissionsBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.add_ons_permissions;
      RecyclerView addOnsPermissions = ViewBindings.findChildViewById(rootView, id);
      if (addOnsPermissions == null) {
        break missingId;
      }

      id = R.id.learn_more_label;
      TextView learnMoreLabel = ViewBindings.findChildViewById(rootView, id);
      if (learnMoreLabel == null) {
        break missingId;
      }

      return new ActivityAddOnPermissionsBinding((RelativeLayout) rootView, addOnsPermissions,
          learnMoreLabel);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
