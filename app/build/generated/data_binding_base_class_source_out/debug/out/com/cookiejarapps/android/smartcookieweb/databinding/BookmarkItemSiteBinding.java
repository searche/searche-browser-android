// Generated by view binder compiler. Do not edit!
package com.cookiejarapps.android.smartcookieweb.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.cookiejarapps.android.smartcookieweb.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class BookmarkItemSiteBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final ImageButton dropdownBookmark;

  @NonNull
  public final View foreground;

  @NonNull
  public final ImageButton imageButton;

  @NonNull
  public final TextView titleTextView;

  @NonNull
  public final TextView urlTextView;

  private BookmarkItemSiteBinding(@NonNull ConstraintLayout rootView,
      @NonNull ImageButton dropdownBookmark, @NonNull View foreground,
      @NonNull ImageButton imageButton, @NonNull TextView titleTextView,
      @NonNull TextView urlTextView) {
    this.rootView = rootView;
    this.dropdownBookmark = dropdownBookmark;
    this.foreground = foreground;
    this.imageButton = imageButton;
    this.titleTextView = titleTextView;
    this.urlTextView = urlTextView;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static BookmarkItemSiteBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static BookmarkItemSiteBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.bookmark_item_site, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static BookmarkItemSiteBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.dropdownBookmark;
      ImageButton dropdownBookmark = ViewBindings.findChildViewById(rootView, id);
      if (dropdownBookmark == null) {
        break missingId;
      }

      id = R.id.foreground;
      View foreground = ViewBindings.findChildViewById(rootView, id);
      if (foreground == null) {
        break missingId;
      }

      id = R.id.imageButton;
      ImageButton imageButton = ViewBindings.findChildViewById(rootView, id);
      if (imageButton == null) {
        break missingId;
      }

      id = R.id.titleTextView;
      TextView titleTextView = ViewBindings.findChildViewById(rootView, id);
      if (titleTextView == null) {
        break missingId;
      }

      id = R.id.urlTextView;
      TextView urlTextView = ViewBindings.findChildViewById(rootView, id);
      if (urlTextView == null) {
        break missingId;
      }

      return new BookmarkItemSiteBinding((ConstraintLayout) rootView, dropdownBookmark, foreground,
          imageButton, titleTextView, urlTextView);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
