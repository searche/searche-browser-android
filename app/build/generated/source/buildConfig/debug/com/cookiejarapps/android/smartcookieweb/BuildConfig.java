/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.cookiejarapps.android.smartcookieweb;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.cookiejarapps.android.smartcookieweb";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 59;
  public static final String VERSION_NAME = "14.0";
}
