package com.cookiejarapps.android.smartcookieweb.browser.home

import androidx.navigation.NavDirections
import com.cookiejarapps.android.smartcookieweb.NavGraphDirections
import kotlin.Boolean
import kotlin.Long
import kotlin.String

public class HomeFragmentDirections private constructor() {
  public companion object {
    public fun actionGlobalHome(focusOnAddressBar: Boolean = false, focusOnCollection: Long = -1L):
        NavDirections = NavGraphDirections.actionGlobalHome(focusOnAddressBar, focusOnCollection)

    public fun actionGlobalBrowser(activeSessionId: String?): NavDirections =
        NavGraphDirections.actionGlobalBrowser(activeSessionId)

    public fun actionGlobalSearchDialog(sessionId: String?, pastedText: String? = null):
        NavDirections = NavGraphDirections.actionGlobalSearchDialog(sessionId, pastedText)
  }
}
