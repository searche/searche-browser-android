package com.cookiejarapps.android.smartcookieweb

import android.os.Bundle
import androidx.navigation.NavDirections
import kotlin.Boolean
import kotlin.Int
import kotlin.Long
import kotlin.String

public class NavGraphDirections private constructor() {
  private data class ActionGlobalHome(
    public val focusOnAddressBar: Boolean = false,
    public val focusOnCollection: Long = -1L
  ) : NavDirections {
    public override val actionId: Int = R.id.action_global_home

    public override val arguments: Bundle
      get() {
        val result = Bundle()
        result.putBoolean("focusOnAddressBar", this.focusOnAddressBar)
        result.putLong("focusOnCollection", this.focusOnCollection)
        return result
      }
  }

  private data class ActionGlobalBrowser(
    public val activeSessionId: String?
  ) : NavDirections {
    public override val actionId: Int = R.id.action_global_browser

    public override val arguments: Bundle
      get() {
        val result = Bundle()
        result.putString("activeSessionId", this.activeSessionId)
        return result
      }
  }

  private data class ActionGlobalSearchDialog(
    public val sessionId: String?,
    public val pastedText: String? = null
  ) : NavDirections {
    public override val actionId: Int = R.id.action_global_search_dialog

    public override val arguments: Bundle
      get() {
        val result = Bundle()
        result.putString("session_id", this.sessionId)
        result.putString("pastedText", this.pastedText)
        return result
      }
  }

  public companion object {
    public fun actionGlobalHome(focusOnAddressBar: Boolean = false, focusOnCollection: Long = -1L):
        NavDirections = ActionGlobalHome(focusOnAddressBar, focusOnCollection)

    public fun actionGlobalBrowser(activeSessionId: String?): NavDirections =
        ActionGlobalBrowser(activeSessionId)

    public fun actionGlobalSearchDialog(sessionId: String?, pastedText: String? = null):
        NavDirections = ActionGlobalSearchDialog(sessionId, pastedText)
  }
}
