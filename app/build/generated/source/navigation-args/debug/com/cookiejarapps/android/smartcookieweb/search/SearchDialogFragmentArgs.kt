package com.cookiejarapps.android.smartcookieweb.search

import android.os.Bundle
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavArgs
import java.lang.IllegalArgumentException
import kotlin.String
import kotlin.jvm.JvmStatic

public data class SearchDialogFragmentArgs(
  public val sessionId: String?,
  public val pastedText: String? = null
) : NavArgs {
  public fun toBundle(): Bundle {
    val result = Bundle()
    result.putString("session_id", this.sessionId)
    result.putString("pastedText", this.pastedText)
    return result
  }

  public fun toSavedStateHandle(): SavedStateHandle {
    val result = SavedStateHandle()
    result.set("session_id", this.sessionId)
    result.set("pastedText", this.pastedText)
    return result
  }

  public companion object {
    @JvmStatic
    public fun fromBundle(bundle: Bundle): SearchDialogFragmentArgs {
      bundle.setClassLoader(SearchDialogFragmentArgs::class.java.classLoader)
      val __sessionId : String?
      if (bundle.containsKey("session_id")) {
        __sessionId = bundle.getString("session_id")
      } else {
        throw IllegalArgumentException("Required argument \"session_id\" is missing and does not have an android:defaultValue")
      }
      val __pastedText : String?
      if (bundle.containsKey("pastedText")) {
        __pastedText = bundle.getString("pastedText")
      } else {
        __pastedText = null
      }
      return SearchDialogFragmentArgs(__sessionId, __pastedText)
    }

    @JvmStatic
    public fun fromSavedStateHandle(savedStateHandle: SavedStateHandle): SearchDialogFragmentArgs {
      val __sessionId : String?
      if (savedStateHandle.contains("session_id")) {
        __sessionId = savedStateHandle["session_id"]
      } else {
        throw IllegalArgumentException("Required argument \"session_id\" is missing and does not have an android:defaultValue")
      }
      val __pastedText : String?
      if (savedStateHandle.contains("pastedText")) {
        __pastedText = savedStateHandle["pastedText"]
      } else {
        __pastedText = null
      }
      return SearchDialogFragmentArgs(__sessionId, __pastedText)
    }
  }
}
