package com.cookiejarapps.android.smartcookieweb.browser.home

import android.os.Bundle
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavArgs
import java.lang.IllegalArgumentException
import kotlin.Boolean
import kotlin.Long
import kotlin.jvm.JvmStatic

public data class HomeFragmentArgs(
  public val focusOnAddressBar: Boolean = false,
  public val focusOnCollection: Long = -1L
) : NavArgs {
  public fun toBundle(): Bundle {
    val result = Bundle()
    result.putBoolean("focusOnAddressBar", this.focusOnAddressBar)
    result.putLong("focusOnCollection", this.focusOnCollection)
    return result
  }

  public fun toSavedStateHandle(): SavedStateHandle {
    val result = SavedStateHandle()
    result.set("focusOnAddressBar", this.focusOnAddressBar)
    result.set("focusOnCollection", this.focusOnCollection)
    return result
  }

  public companion object {
    @JvmStatic
    public fun fromBundle(bundle: Bundle): HomeFragmentArgs {
      bundle.setClassLoader(HomeFragmentArgs::class.java.classLoader)
      val __focusOnAddressBar : Boolean
      if (bundle.containsKey("focusOnAddressBar")) {
        __focusOnAddressBar = bundle.getBoolean("focusOnAddressBar")
      } else {
        __focusOnAddressBar = false
      }
      val __focusOnCollection : Long
      if (bundle.containsKey("focusOnCollection")) {
        __focusOnCollection = bundle.getLong("focusOnCollection")
      } else {
        __focusOnCollection = -1L
      }
      return HomeFragmentArgs(__focusOnAddressBar, __focusOnCollection)
    }

    @JvmStatic
    public fun fromSavedStateHandle(savedStateHandle: SavedStateHandle): HomeFragmentArgs {
      val __focusOnAddressBar : Boolean?
      if (savedStateHandle.contains("focusOnAddressBar")) {
        __focusOnAddressBar = savedStateHandle["focusOnAddressBar"]
        if (__focusOnAddressBar == null) {
          throw IllegalArgumentException("Argument \"focusOnAddressBar\" of type boolean does not support null values")
        }
      } else {
        __focusOnAddressBar = false
      }
      val __focusOnCollection : Long?
      if (savedStateHandle.contains("focusOnCollection")) {
        __focusOnCollection = savedStateHandle["focusOnCollection"]
        if (__focusOnCollection == null) {
          throw IllegalArgumentException("Argument \"focusOnCollection\" of type long does not support null values")
        }
      } else {
        __focusOnCollection = -1L
      }
      return HomeFragmentArgs(__focusOnAddressBar, __focusOnCollection)
    }
  }
}
