package org.mozilla.fenix.home;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002RV\u0010\u0003\u001a>\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0012\u0012\u0012\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0012\u0004\u0012\u00020\b0\u00060\u0004j\u001e\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0012\u0012\u0012\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0012\u0004\u0012\u00020\b0\u0006`\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\u00a8\u0006\u000e"}, d2 = {"Lorg/mozilla/fenix/home/SharedViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "downloadDialogState", "Ljava/util/HashMap;", "", "Lkotlin/Pair;", "Lmozilla/components/browser/state/state/content/DownloadState;", "", "Lkotlin/collections/HashMap;", "getDownloadDialogState", "()Ljava/util/HashMap;", "setDownloadDialogState", "(Ljava/util/HashMap;)V", "app_debug"})
public final class SharedViewModel extends androidx.lifecycle.ViewModel {
    
    /**
     * Stores data needed for [DynamicDownloadDialog]. See #9044
     * Format: HashMap<sessionId, Pair<DownloadState, didFail>
     */
    @org.jetbrains.annotations.NotNull()
    private java.util.HashMap<java.lang.String, kotlin.Pair<mozilla.components.browser.state.state.content.DownloadState, java.lang.Boolean>> downloadDialogState;
    
    public SharedViewModel() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.HashMap<java.lang.String, kotlin.Pair<mozilla.components.browser.state.state.content.DownloadState, java.lang.Boolean>> getDownloadDialogState() {
        return null;
    }
    
    public final void setDownloadDialogState(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, kotlin.Pair<mozilla.components.browser.state.state.content.DownloadState, java.lang.Boolean>> p0) {
    }
}