package org.mozilla.fenix.home;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u000f"}, d2 = {"Lorg/mozilla/fenix/home/HomeScreenViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "sessionToDelete", "", "getSessionToDelete", "()Ljava/lang/String;", "setSessionToDelete", "(Ljava/lang/String;)V", "shouldScrollToTopSites", "", "getShouldScrollToTopSites", "()Z", "setShouldScrollToTopSites", "(Z)V", "app_debug"})
public final class HomeScreenViewModel extends androidx.lifecycle.ViewModel {
    
    /**
     * Used to delete a specific session once the home screen is resumed
     */
    @org.jetbrains.annotations.Nullable()
    private java.lang.String sessionToDelete;
    
    /**
     * Used to remember if we need to scroll to top of the homeFragment's recycleView (top sites) see #8561
     */
    private boolean shouldScrollToTopSites = true;
    
    public HomeScreenViewModel() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSessionToDelete() {
        return null;
    }
    
    public final void setSessionToDelete(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public final boolean getShouldScrollToTopSites() {
        return false;
    }
    
    public final void setShouldScrollToTopSites(boolean p0) {
    }
}