package com.cookiejarapps.android.smartcookieweb.ssl;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\f\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"}, d2 = {"showSslDialog", "", "Landroid/content/Context;", "app_debug"})
public final class SslDialogKt {
    
    /**
     * Shows an informative dialog with the provided [SslCertificate] information.
     */
    public static final void showSslDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$showSslDialog) {
    }
}