package com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000f\b&\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u000e\b\u0001\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00010\u00032\b\u0012\u0004\u0012\u0002H\u00020\u0004:\u0001EB%\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u000bJ\u0013\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u001fJ\u001b\u0010\u001c\u001a\u00020\u001d2\u0006\u0010 \u001a\u00020\u00132\u0006\u0010\u001e\u001a\u00028\u0000\u00a2\u0006\u0002\u0010!J\u0006\u0010\"\u001a\u00020\u001dJ\u0016\u0010#\u001a\u00028\u00002\u0006\u0010 \u001a\u00020\u0013H\u0096\u0002\u00a2\u0006\u0002\u0010$J\b\u0010%\u001a\u00020\u0013H\u0016J\u000e\u0010&\u001a\u00020\u001b2\u0006\u0010\'\u001a\u00020\u0013J\u0016\u0010(\u001a\u00020\u001d2\u0006\u0010)\u001a\u00020\u00132\u0006\u0010*\u001a\u00020\u0013J%\u0010+\u001a\u00020\u001d2\u0006\u0010,\u001a\u00028\u00012\u0006\u0010\u001e\u001a\u00028\u00002\u0006\u0010\'\u001a\u00020\u0013H\u0016\u00a2\u0006\u0002\u0010-J\u001d\u0010+\u001a\u00020\u001d2\u0006\u0010,\u001a\u00028\u00012\u0006\u0010\'\u001a\u00020\u0013H\u0016\u00a2\u0006\u0002\u0010.J\'\u0010/\u001a\u00028\u00012\u0006\u0010\f\u001a\u00020\r2\b\u00100\u001a\u0004\u0018\u0001012\u0006\u00102\u001a\u00020\u0013H$\u00a2\u0006\u0002\u00103J\u001d\u0010/\u001a\u00028\u00012\u0006\u00100\u001a\u0002012\u0006\u00102\u001a\u00020\u0013H\u0016\u00a2\u0006\u0002\u00104J\'\u00105\u001a\u00020\u001d2\u0006\u00106\u001a\u0002072\u0006\u0010\'\u001a\u00020\u00132\b\u0010\u001e\u001a\u0004\u0018\u00018\u0000H\u0014\u00a2\u0006\u0002\u00108J\'\u00109\u001a\u00020\u001b2\u0006\u00106\u001a\u0002072\u0006\u0010\'\u001a\u00020\u00132\b\u0010\u001e\u001a\u0004\u0018\u00018\u0000H\u0002\u00a2\u0006\u0002\u0010:J\u0013\u0010;\u001a\u00028\u00002\u0006\u0010 \u001a\u00020\u0013\u00a2\u0006\u0002\u0010$J\u001f\u0010<\u001a\u00020\u00132\u0006\u0010\'\u001a\u00020\u00132\b\u0010\u001e\u001a\u0004\u0018\u00018\u0000H\u0004\u00a2\u0006\u0002\u0010=J\u001e\u0010>\u001a\u00028\u00002\u0006\u0010 \u001a\u00020\u00132\u0006\u0010\u001e\u001a\u00028\u0000H\u0086\u0002\u00a2\u0006\u0002\u0010?J\u0010\u0010@\u001a\u00020\u001d2\u0006\u0010\t\u001a\u00020\nH\u0004J\u0018\u0010A\u001a\u00020\u001d2\u0006\u0010\'\u001a\u00020\u00132\u0006\u0010B\u001a\u00020\u001bH\u0016J\u0006\u0010C\u001a\u00020\u0013J\u0010\u0010D\u001a\u00020\u001d2\u0006\u0010\'\u001a\u00020\u0013H\u0016R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0012\u001a\u00020\u0013@BX\u0086\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0017\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00130\u00188F\u00a2\u0006\u0006\u001a\u0004\b\u0019\u0010\u0011R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006F"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/ArrayRecyclerAdapter;", "T", "VH", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/ArrayRecyclerAdapter$ArrayViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "context", "Landroid/content/Context;", "items", "", "listener", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkRecyclerViewClickInterface;", "(Landroid/content/Context;Ljava/util/List;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkRecyclerViewClickInterface;)V", "inflater", "Landroid/view/LayoutInflater;", "itemSelected", "Landroid/util/SparseBooleanArray;", "getItems", "()Ljava/util/List;", "<set-?>", "", "selectedItemCount", "getSelectedItemCount", "()I", "selectedItems", "", "getSelectedItems", "sortMode", "", "add", "", "item", "(Ljava/lang/Object;)V", "index", "(ILjava/lang/Object;)V", "clear", "get", "(I)Ljava/lang/Object;", "getItemCount", "isSelected", "position", "move", "fromPosition", "toPosition", "onBindViewHolder", "holder", "(Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/ArrayRecyclerAdapter$ArrayViewHolder;Ljava/lang/Object;I)V", "(Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/ArrayRecyclerAdapter$ArrayViewHolder;I)V", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/ArrayRecyclerAdapter$ArrayViewHolder;", "(Landroid/view/ViewGroup;I)Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/ArrayRecyclerAdapter$ArrayViewHolder;", "onItemClick", "v", "Landroid/view/View;", "(Landroid/view/View;ILjava/lang/Object;)V", "onItemLongClick", "(Landroid/view/View;ILjava/lang/Object;)Z", "remove", "searchPosition", "(ILjava/lang/Object;)I", "set", "(ILjava/lang/Object;)Ljava/lang/Object;", "setRecyclerListener", "setSelect", "isSelect", "size", "toggle", "ArrayViewHolder", "app_debug"})
public abstract class ArrayRecyclerAdapter<T extends java.lang.Object, VH extends com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.ArrayRecyclerAdapter.ArrayViewHolder<T>> extends androidx.recyclerview.widget.RecyclerView.Adapter<VH> {
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<T> items = null;
    private com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkRecyclerViewClickInterface listener;
    private final android.view.LayoutInflater inflater = null;
    private boolean sortMode = true;
    private final android.util.SparseBooleanArray itemSelected = null;
    private int selectedItemCount = 0;
    
    public ArrayRecyclerAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<T> items, @org.jetbrains.annotations.Nullable()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkRecyclerViewClickInterface listener) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<T> getItems() {
        return null;
    }
    
    public final int getSelectedItemCount() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.Integer> getSelectedItems() {
        return null;
    }
    
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    VH holder, T item, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected abstract VH onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup parent, int viewType);
    
    public final void move(int fromPosition, int toPosition) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final int size() {
        return 0;
    }
    
    public T get(int index) {
        return null;
    }
    
    public final void add(T item) {
    }
    
    public final void add(int index, T item) {
    }
    
    public final T set(int index, T item) {
        return null;
    }
    
    public final T remove(int index) {
        return null;
    }
    
    public final void clear() {
    }
    
    protected final void setRecyclerListener(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkRecyclerViewClickInterface listener) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public VH onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    VH holder, int position) {
    }
    
    protected void onItemClick(@org.jetbrains.annotations.NotNull()
    android.view.View v, int position, @org.jetbrains.annotations.Nullable()
    T item) {
    }
    
    private final boolean onItemLongClick(android.view.View v, int position, T item) {
        return false;
    }
    
    protected final int searchPosition(int position, @org.jetbrains.annotations.Nullable()
    T item) {
        return 0;
    }
    
    public void toggle(int position) {
    }
    
    public void setSelect(int position, boolean isSelect) {
    }
    
    public final boolean isSelected(int position) {
        return false;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0016\u0018\u0000*\u0004\b\u0002\u0010\u00012\u00020\u0002B\u001f\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0010\u0010\u0005\u001a\f\u0012\u0004\u0012\u00028\u0002\u0012\u0002\b\u00030\u0006\u00a2\u0006\u0002\u0010\u0007J\u0015\u0010\r\u001a\u00020\u000e2\u0006\u0010\b\u001a\u00028\u0002H\u0016\u00a2\u0006\u0002\u0010\u000fR\u0014\u0010\b\u001a\u00028\u00028DX\u0084\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0012\u0010\u000b\u001a\u0004\u0018\u00018\u0002X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\f\u00a8\u0006\u0010"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/ArrayRecyclerAdapter$ArrayViewHolder;", "I", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "adapter", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/ArrayRecyclerAdapter;", "(Landroid/view/View;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/ArrayRecyclerAdapter;)V", "item", "getItem", "()Ljava/lang/Object;", "target", "Ljava/lang/Object;", "setUp", "", "(Ljava/lang/Object;)V", "app_debug"})
    public static class ArrayViewHolder<I extends java.lang.Object> extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private I target;
        
        public ArrayViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView, @org.jetbrains.annotations.NotNull()
        com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.ArrayRecyclerAdapter<I, ?> adapter) {
            super(null);
        }
        
        protected final I getItem() {
            return null;
        }
        
        public void setUp(I item) {
        }
    }
}