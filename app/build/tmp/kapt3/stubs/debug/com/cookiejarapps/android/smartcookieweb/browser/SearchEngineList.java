package com.cookiejarapps.android.smartcookieweb.browser;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0012\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/SearchEngineList;", "", "()V", "startPageLogo", "", "kotlin.jvm.PlatformType", "getEngines", "", "Lmozilla/components/browser/state/search/SearchEngine;", "app_debug"})
public final class SearchEngineList {
    private byte[] startPageLogo;
    
    public SearchEngineList() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<mozilla.components.browser.state.search.SearchEngine> getEngines() {
        return null;
    }
}