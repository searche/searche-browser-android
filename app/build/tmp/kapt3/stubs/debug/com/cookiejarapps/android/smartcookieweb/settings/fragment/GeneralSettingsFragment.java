package com.cookiejarapps.android.smartcookieweb.settings.fragment;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u001c\u0010\u0005\u001a\u00020\u00042\b\u0010\u0006\u001a\u0004\u0018\u00010\u00072\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0016J\b\u0010\n\u001a\u00020\u0004H\u0002J\b\u0010\u000b\u001a\u00020\u0004H\u0002\u00a8\u0006\f"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/settings/fragment/GeneralSettingsFragment;", "Lcom/cookiejarapps/android/smartcookieweb/settings/fragment/BaseSettingsFragment;", "()V", "customSearchEngineDialog", "", "onCreatePreferences", "savedInstanceState", "Landroid/os/Bundle;", "s", "", "pickHomepage", "pickSearchEngine", "app_debug"})
public final class GeneralSettingsFragment extends com.cookiejarapps.android.smartcookieweb.settings.fragment.BaseSettingsFragment {
    
    public GeneralSettingsFragment() {
        super();
    }
    
    @java.lang.Override()
    public void onCreatePreferences(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState, @org.jetbrains.annotations.Nullable()
    java.lang.String s) {
    }
    
    private final void pickHomepage() {
    }
    
    private final void pickSearchEngine() {
    }
    
    public final void customSearchEngineDialog() {
    }
}