package com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003:\u0002GHB\u0005\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\rH\u0002J\b\u0010\u001d\u001a\u00020\u001bH\u0002J\b\u0010\u001e\u001a\u00020\rH\u0002J\u0010\u0010\u001f\u001a\u00020\u00122\u0006\u0010 \u001a\u00020\rH\u0002J\u0012\u0010!\u001a\u00020\u001b2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\"\u0010$\u001a\u00020\u001b2\u0006\u0010%\u001a\u00020&2\b\u0010\'\u001a\u0004\u0018\u00010(2\u0006\u0010)\u001a\u00020&H\u0002J\u0018\u0010*\u001a\u00020\u001b2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.H\u0016J$\u0010/\u001a\u0002002\u0006\u0010-\u001a\u0002012\b\u00102\u001a\u0004\u0018\u0001032\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\b\u00104\u001a\u00020\u001bH\u0016J\u0018\u00105\u001a\u00020\u001b2\u0006\u00106\u001a\u0002002\u0006\u00107\u001a\u00020&H\u0016J\u0010\u00108\u001a\u00020\u001b2\u0006\u00107\u001a\u00020&H\u0016J\u0018\u00109\u001a\u00020\u001b2\u0006\u00106\u001a\u0002002\u0006\u00107\u001a\u00020&H\u0016J\u0018\u0010:\u001a\u00020\u00172\u0006\u00106\u001a\u0002002\u0006\u00107\u001a\u00020&H\u0016J\u0010\u0010;\u001a\u00020\u001b2\u0006\u0010<\u001a\u00020&H\u0016J\u0018\u0010=\u001a\u00020\u001b2\u0006\u00106\u001a\u0002002\u0006\u00107\u001a\u00020&H\u0016J\u001a\u0010>\u001a\u00020\u001b2\u0006\u0010?\u001a\u0002002\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\u001a\u0010@\u001a\u00020\u001b2\b\u0010A\u001a\u0004\u0018\u00010B2\u0006\u0010\u001c\u001a\u00020&H\u0002J\u0010\u0010C\u001a\u00020\u001b2\u0006\u0010 \u001a\u00020\rH\u0002J\u0010\u0010D\u001a\u00020\u001b2\u0006\u0010 \u001a\u00020\rH\u0002J\u0018\u0010E\u001a\u00020\u001b2\u0006\u00106\u001a\u0002002\u0006\u0010)\u001a\u00020&H\u0002J\u0010\u0010\u0016\u001a\u00020\u001b2\u0006\u0010F\u001a\u00020\u0017H\u0002R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\u00068BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u00020\r8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006I"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkFragment;", "Landroidx/fragment/app/Fragment;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkAdapter$OnBookmarkRecyclerListener;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathView$OnPathViewClickListener;", "()V", "_binding", "Lcom/cookiejarapps/android/smartcookieweb/databinding/FragmentBookmarkBinding;", "adapter", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkAdapter;", "binding", "getBinding", "()Lcom/cookiejarapps/android/smartcookieweb/databinding/FragmentBookmarkBinding;", "currentFolder", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;", "manager", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/repository/BookmarkManager;", "pathAdapter", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathViewAdapter;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkFragment$BookmarkPath;", "root", "getRoot", "()Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;", "showPathHeader", "", "touchListener", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/RecyclerViewItemTouchListener;", "addAllFolderPaths", "", "target", "closeDrawer", "getFirstPosition", "getPath", "folder", "onActivityCreated", "savedInstanceState", "Landroid/os/Bundle;", "onContextMenuClick", "id", "", "item", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkItem;", "index", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "inflater", "Landroid/view/MenuInflater;", "onCreateView", "Landroid/view/View;", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onIconClick", "v", "position", "onPathItemClick", "onRecyclerItemClicked", "onRecyclerItemLongClicked", "onSelectionStateChange", "items", "onShowMenu", "onViewCreated", "view", "sendUrl", "url", "", "setList", "setPath", "showContextMenu", "show", "BookmarkPath", "Touch", "app_debug"})
public final class BookmarkFragment extends androidx.fragment.app.Fragment implements com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkAdapter.OnBookmarkRecyclerListener, com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.PathView.OnPathViewClickListener {
    private com.cookiejarapps.android.smartcookieweb.databinding.FragmentBookmarkBinding _binding;
    private boolean showPathHeader = true;
    private com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.PathViewAdapter<com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkFragment.BookmarkPath> pathAdapter;
    private com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkAdapter adapter;
    private com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository.BookmarkManager manager;
    private com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem currentFolder;
    private com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.RecyclerViewItemTouchListener touchListener;
    
    public BookmarkFragment() {
        super();
    }
    
    private final com.cookiejarapps.android.smartcookieweb.databinding.FragmentBookmarkBinding getBinding() {
        return null;
    }
    
    private final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem getRoot() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onActivityCreated(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onPathItemClick(int position) {
    }
    
    private final void setList(com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder) {
    }
    
    @java.lang.Override()
    public void onRecyclerItemClicked(@org.jetbrains.annotations.NotNull()
    android.view.View v, int position) {
    }
    
    @java.lang.Override()
    public boolean onRecyclerItemLongClicked(@org.jetbrains.annotations.NotNull()
    android.view.View v, int position) {
        return false;
    }
    
    @java.lang.Override()
    public void onIconClick(@org.jetbrains.annotations.NotNull()
    android.view.View v, int position) {
    }
    
    @java.lang.Override()
    public void onShowMenu(@org.jetbrains.annotations.NotNull()
    android.view.View v, int position) {
    }
    
    private final void sendUrl(java.lang.String url, int target) {
    }
    
    private final void closeDrawer() {
    }
    
    @java.lang.Override()
    public void onCreateOptionsMenu(@org.jetbrains.annotations.NotNull()
    android.view.Menu menu, @org.jetbrains.annotations.NotNull()
    android.view.MenuInflater inflater) {
    }
    
    private final void showContextMenu(android.view.View v, int index) {
    }
    
    private final void onContextMenuClick(int id, com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem item, int index) {
    }
    
    @java.lang.Override()
    public void onSelectionStateChange(int items) {
    }
    
    @java.lang.Override()
    public void onDestroyView() {
    }
    
    private final void showPathHeader(boolean show) {
    }
    
    private final void setPath(com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder) {
    }
    
    private final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem getFirstPosition() {
        return null;
    }
    
    private final com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkFragment.BookmarkPath getPath(com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder) {
        return null;
    }
    
    private final void addAllFolderPaths(com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem target) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\t\u001a\u00020\nH\u0016J \u0010\u000b\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\bH\u0016J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u0004H\u0016\u00a8\u0006\u0010"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkFragment$Touch;", "Landroidx/recyclerview/widget/ItemTouchHelper$Callback;", "(Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkFragment;)V", "getMovementFlags", "", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "viewHolder", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "isLongPressDragEnabled", "", "onMove", "target", "onSwiped", "", "direction", "app_debug"})
    final class Touch extends androidx.recyclerview.widget.ItemTouchHelper.Callback {
        
        public Touch() {
            super();
        }
        
        @java.lang.Override()
        public int getMovementFlags(@org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView recyclerView, @org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
            return 0;
        }
        
        @java.lang.Override()
        public boolean onMove(@org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView recyclerView, @org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, @org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView.ViewHolder target) {
            return false;
        }
        
        @java.lang.Override()
        public void onSwiped(@org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int direction) {
        }
        
        @java.lang.Override()
        public boolean isLongPressDragEnabled() {
            return false;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0096\u0002J\b\u0010\u000f\u001a\u00020\u0010H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0011"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkFragment$BookmarkPath;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathView$Path;", "folder", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;", "title", "", "(Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;Ljava/lang/String;)V", "getFolder", "()Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;", "getTitle", "()Ljava/lang/String;", "equals", "", "other", "", "hashCode", "", "app_debug"})
    static final class BookmarkPath implements com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.PathView.Path {
        @org.jetbrains.annotations.NotNull()
        private final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder = null;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String title = null;
        
        public BookmarkPath(@org.jetbrains.annotations.NotNull()
        com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder, @org.jetbrains.annotations.NotNull()
        java.lang.String title) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem getFolder() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String getTitle() {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
    }
}