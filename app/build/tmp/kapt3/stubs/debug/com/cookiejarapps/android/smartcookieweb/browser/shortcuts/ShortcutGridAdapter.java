package com.cookiejarapps.android.smartcookieweb.browser.shortcuts;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\b\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0014H\u0016J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0017\u001a\u00020\u0014H\u0016J\u0015\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001bH\u0000\u00a2\u0006\u0002\b\u001dJ\u0010\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001bH\u0002J$\u0010\u001f\u001a\u0004\u0018\u00010 2\u0006\u0010\u0017\u001a\u00020\u00142\b\u0010!\u001a\u0004\u0018\u00010 2\u0006\u0010\"\u001a\u00020#H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/shortcuts/ShortcutGridAdapter;", "Landroid/widget/BaseAdapter;", "context", "Landroid/content/Context;", "shortcuts", "", "Lcom/cookiejarapps/android/smartcookieweb/browser/shortcuts/ShortcutEntity;", "(Landroid/content/Context;Ljava/util/List;)V", "imageView", "Landroid/widget/ImageView;", "layoutInflater", "Landroid/view/LayoutInflater;", "list", "getList", "()Ljava/util/List;", "setList", "(Ljava/util/List;)V", "nameView", "Landroid/widget/TextView;", "getCount", "", "getItem", "", "position", "getItemId", "", "getUrlCharacter", "", "url", "getUrlCharacter$app_debug", "getUrlHost", "getView", "Landroid/view/View;", "convertView", "parent", "Landroid/view/ViewGroup;", "app_debug"})
public final class ShortcutGridAdapter extends android.widget.BaseAdapter {
    private final android.content.Context context = null;
    private final java.util.List<com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutEntity> shortcuts = null;
    private android.view.LayoutInflater layoutInflater;
    private android.widget.ImageView imageView;
    private android.widget.TextView nameView;
    @org.jetbrains.annotations.NotNull()
    private java.util.List<com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutEntity> list;
    
    public ShortcutGridAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutEntity> shortcuts) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutEntity> getList() {
        return null;
    }
    
    public final void setList(@org.jetbrains.annotations.NotNull()
    java.util.List<com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutEntity> p0) {
    }
    
    @java.lang.Override()
    public int getCount() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.Object getItem(int position) {
        return null;
    }
    
    @java.lang.Override()
    public long getItemId(int position) {
        return 0L;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View getView(int position, @org.jetbrains.annotations.Nullable()
    android.view.View convertView, @org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent) {
        return null;
    }
    
    private final java.lang.String getUrlHost(java.lang.String url) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUrlCharacter$app_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String url) {
        return null;
    }
}