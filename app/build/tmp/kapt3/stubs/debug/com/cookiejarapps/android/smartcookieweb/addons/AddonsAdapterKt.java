package com.cookiejarapps.android.smartcookieweb.addons;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0004\u001a\f\u0010\u0003\u001a\u00020\u0004*\u00020\u0005H\u0002\u001a\f\u0010\u0006\u001a\u00020\u0004*\u00020\u0005H\u0002\u001a\f\u0010\u0007\u001a\u00020\u0004*\u00020\u0005H\u0002\u001a\f\u0010\b\u001a\u00020\u0004*\u00020\u0005H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"VIEW_HOLDER_TYPE_ADDON", "", "VIEW_HOLDER_TYPE_SECTION", "inDisabledSection", "", "Lmozilla/components/feature/addons/Addon;", "inInstalledSection", "inRecommendedSection", "inSideloadedSection", "app_debug"})
public final class AddonsAdapterKt {
    private static final int VIEW_HOLDER_TYPE_SECTION = 0;
    private static final int VIEW_HOLDER_TYPE_ADDON = 1;
    
    private static final boolean inRecommendedSection(mozilla.components.feature.addons.Addon $this$inRecommendedSection) {
        return false;
    }
    
    private static final boolean inSideloadedSection(mozilla.components.feature.addons.Addon $this$inSideloadedSection) {
        return false;
    }
    
    private static final boolean inInstalledSection(mozilla.components.feature.addons.Addon $this$inInstalledSection) {
        return false;
    }
    
    private static final boolean inDisabledSection(mozilla.components.feature.addons.Addon $this$inDisabledSection) {
        return false;
    }
}