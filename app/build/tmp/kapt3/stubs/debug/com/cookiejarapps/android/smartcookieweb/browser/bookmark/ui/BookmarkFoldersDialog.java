package com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001#B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0016\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\bJ\u0018\u0010\u0016\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\rJ\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0017\u001a\u00020\bH\u0002J\"\u0010\u001b\u001a\u00020\u00002\u0014\b\u0004\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u001e0\u001dH\u0086\b\u00f8\u0001\u0000J\u000e\u0010\u001b\u001a\u00020\u00002\u0006\u0010\u001c\u001a\u00020\u0013J\u000e\u0010\u001f\u001a\u00020\u00002\u0006\u0010 \u001a\u00020!J\u0006\u0010\"\u001a\u00020\u001aR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0007\n\u0005\b\u009920\u0001\u00a8\u0006$"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkFoldersDialog;", "", "context", "Landroid/content/Context;", "manager", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/repository/BookmarkManager;", "(Landroid/content/Context;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/repository/BookmarkManager;)V", "mCurrentFolder", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;", "mDialog", "Landroidx/appcompat/app/AlertDialog;", "mExcludeList", "", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkItem;", "mFolderList", "Ljava/util/ArrayList;", "mListView", "Landroid/widget/ListView;", "mOnFolderSelectedListener", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkFoldersDialog$OnFolderSelectedListener;", "titleText", "Landroid/widget/TextView;", "setCurrentFolder", "folder", "excludeItem", "setFolder", "", "setOnFolderSelectedListener", "l", "Lkotlin/Function1;", "", "setTitle", "title", "", "show", "OnFolderSelectedListener", "app_debug"})
public final class BookmarkFoldersDialog {
    private final android.content.Context context = null;
    private final com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository.BookmarkManager manager = null;
    private androidx.appcompat.app.AlertDialog mDialog;
    private final android.widget.ListView mListView = null;
    private com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem mCurrentFolder;
    private final java.util.ArrayList<com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem> mFolderList = null;
    private java.util.Collection<? extends com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem> mExcludeList;
    private com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkFoldersDialog.OnFolderSelectedListener mOnFolderSelectedListener;
    private final android.widget.TextView titleText = null;
    
    public BookmarkFoldersDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository.BookmarkManager manager) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkFoldersDialog setTitle(int title) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkFoldersDialog setCurrentFolder(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkFoldersDialog setCurrentFolder(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder, @org.jetbrains.annotations.Nullable()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem excludeItem) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkFoldersDialog setOnFolderSelectedListener(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkFoldersDialog.OnFolderSelectedListener l) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkFoldersDialog setOnFolderSelectedListener(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem, java.lang.Boolean> l) {
        return null;
    }
    
    private final void setFolder(com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder) {
    }
    
    public final void show() {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\b"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkFoldersDialog$OnFolderSelectedListener;", "", "onFolderSelected", "", "dialog", "Landroid/content/DialogInterface;", "folder", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;", "app_debug"})
    public static abstract interface OnFolderSelectedListener {
        
        public abstract boolean onFolderSelected(@org.jetbrains.annotations.NotNull()
        android.content.DialogInterface dialog, @org.jetbrains.annotations.NotNull()
        com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder);
    }
}