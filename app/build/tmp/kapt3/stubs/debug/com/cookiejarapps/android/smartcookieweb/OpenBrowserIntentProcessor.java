package com.cookiejarapps.android.smartcookieweb;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0014\u0010\u0004\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0005\u00a2\u0006\u0002\u0010\bJ \u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\fH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0004\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/OpenBrowserIntentProcessor;", "Lcom/cookiejarapps/android/smartcookieweb/HomeIntentProcessor;", "activity", "Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;", "getIntentSessionId", "Lkotlin/Function1;", "Lmozilla/components/support/utils/SafeIntent;", "", "(Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;Lkotlin/jvm/functions/Function1;)V", "process", "", "intent", "Landroid/content/Intent;", "navController", "Landroidx/navigation/NavController;", "out", "app_debug"})
public final class OpenBrowserIntentProcessor implements com.cookiejarapps.android.smartcookieweb.HomeIntentProcessor {
    private final com.cookiejarapps.android.smartcookieweb.BrowserActivity activity = null;
    private final kotlin.jvm.functions.Function1<mozilla.components.support.utils.SafeIntent, java.lang.String> getIntentSessionId = null;
    
    public OpenBrowserIntentProcessor(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.BrowserActivity activity, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super mozilla.components.support.utils.SafeIntent, java.lang.String> getIntentSessionId) {
        super();
    }
    
    @java.lang.Override()
    public boolean process(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent, @org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController, @org.jetbrains.annotations.NotNull()
    android.content.Intent out) {
        return false;
    }
}