package com.cookiejarapps.android.smartcookieweb.components;

import java.lang.System;

@kotlin.Suppress(names = {"LargeClass"})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u00a8\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0016\u0018\u0000 \u00aa\u00012\u00020\u0001:\u0002\u00aa\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u00a8\u0001\u001a\u00030\u00a9\u0001R\u001b\u0010\u0005\u001a\u00020\u00068FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u001b\u0010\u000b\u001a\u00020\f8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\n\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u001b\u0010\u0014\u001a\u00020\u00158FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0018\u0010\n\u001a\u0004\b\u0016\u0010\u0017R\u001b\u0010\u0019\u001a\u00020\u001a8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001d\u0010\n\u001a\u0004\b\u001b\u0010\u001cR\u001b\u0010\u001e\u001a\u00020\u001f8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\"\u0010\n\u001a\u0004\b \u0010!R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010#\u001a\u00020$8VX\u0096\u0084\u0002\u00a2\u0006\f\n\u0004\b\'\u0010\n\u001a\u0004\b%\u0010&R\u001b\u0010(\u001a\u00020)8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b,\u0010\n\u001a\u0004\b*\u0010+R\u001b\u0010-\u001a\u00020.8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b1\u0010\n\u001a\u0004\b/\u00100R\'\u00102\u001a\u000e\u0012\u0004\u0012\u000204\u0012\u0004\u0012\u000205038FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b8\u0010\n\u001a\u0004\b6\u00107R\u001b\u00109\u001a\u00020:8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b=\u0010\n\u001a\u0004\b;\u0010<R\u001b\u0010>\u001a\u00020?8VX\u0096\u0084\u0002\u00a2\u0006\f\n\u0004\bB\u0010\n\u001a\u0004\b@\u0010AR\u001b\u0010C\u001a\u00020D8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\bG\u0010\n\u001a\u0004\bE\u0010FR\u001b\u0010H\u001a\u00020I8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\bL\u0010\n\u001a\u0004\bJ\u0010KR\u001b\u0010M\u001a\u00020N8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\bQ\u0010\n\u001a\u0004\bO\u0010PR\u0014\u0010R\u001a\b\u0012\u0004\u0012\u00020I0SX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010T\u001a\u00020U8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\bX\u0010\n\u001a\u0004\bV\u0010WR\u0011\u0010Y\u001a\u00020Z\u00a2\u0006\b\n\u0000\u001a\u0004\b[\u0010\\R\u001b\u0010]\u001a\u00020^8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\ba\u0010\n\u001a\u0004\b_\u0010`R\u001b\u0010b\u001a\u00020c8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\bf\u0010\n\u001a\u0004\bd\u0010eR\u001b\u0010g\u001a\u00020h8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\bk\u0010\n\u001a\u0004\bi\u0010jR\u001b\u0010l\u001a\u00020m8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\bp\u0010\n\u001a\u0004\bn\u0010oR\u001b\u0010q\u001a\u00020r8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\bu\u0010\n\u001a\u0004\bs\u0010tR\u001b\u0010v\u001a\u00020w8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\bz\u0010\n\u001a\u0004\bx\u0010yR\u001b\u0010{\u001a\u00020|8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u007f\u0010\n\u001a\u0004\b}\u0010~R \u0010\u0080\u0001\u001a\u00030\u0081\u00018FX\u0086\u0084\u0002\u00a2\u0006\u000f\n\u0005\b\u0084\u0001\u0010\n\u001a\u0006\b\u0082\u0001\u0010\u0083\u0001R \u0010\u0085\u0001\u001a\u00030\u0086\u00018FX\u0086\u0084\u0002\u00a2\u0006\u000f\n\u0005\b\u0089\u0001\u0010\n\u001a\u0006\b\u0087\u0001\u0010\u0088\u0001R \u0010\u008a\u0001\u001a\u00030\u008b\u00018FX\u0086\u0084\u0002\u00a2\u0006\u000f\n\u0005\b\u008e\u0001\u0010\n\u001a\u0006\b\u008c\u0001\u0010\u008d\u0001R \u0010\u008f\u0001\u001a\u00030\u0090\u00018BX\u0082\u0084\u0002\u00a2\u0006\u000f\n\u0005\b\u0093\u0001\u0010\n\u001a\u0006\b\u0091\u0001\u0010\u0092\u0001R \u0010\u0094\u0001\u001a\u00030\u0095\u00018FX\u0086\u0084\u0002\u00a2\u0006\u000f\n\u0005\b\u0098\u0001\u0010\n\u001a\u0006\b\u0096\u0001\u0010\u0097\u0001R \u0010\u0099\u0001\u001a\u00030\u009a\u00018FX\u0086\u0084\u0002\u00a2\u0006\u000f\n\u0005\b\u009d\u0001\u0010\n\u001a\u0006\b\u009b\u0001\u0010\u009c\u0001R \u0010\u009e\u0001\u001a\u00030\u009f\u00018FX\u0086\u0084\u0002\u00a2\u0006\u000f\n\u0005\b\u00a2\u0001\u0010\n\u001a\u0006\b\u00a0\u0001\u0010\u00a1\u0001R \u0010\u00a3\u0001\u001a\u00030\u00a4\u00018FX\u0086\u0084\u0002\u00a2\u0006\u000f\n\u0005\b\u00a7\u0001\u0010\n\u001a\u0006\b\u00a5\u0001\u0010\u00a6\u0001\u00a8\u0006\u00ab\u0001"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/Components;", "", "applicationContext", "Landroid/content/Context;", "(Landroid/content/Context;)V", "addonCollectionProvider", "Lmozilla/components/feature/addons/amo/AddonCollectionProvider;", "getAddonCollectionProvider", "()Lmozilla/components/feature/addons/amo/AddonCollectionProvider;", "addonCollectionProvider$delegate", "Lkotlin/Lazy;", "addonManager", "Lmozilla/components/feature/addons/AddonManager;", "getAddonManager", "()Lmozilla/components/feature/addons/AddonManager;", "addonManager$delegate", "addonUpdater", "Lmozilla/components/feature/addons/update/DefaultAddonUpdater;", "getAddonUpdater", "()Lmozilla/components/feature/addons/update/DefaultAddonUpdater;", "appLinksInterceptor", "Lmozilla/components/feature/app/links/AppLinksInterceptor;", "getAppLinksInterceptor", "()Lmozilla/components/feature/app/links/AppLinksInterceptor;", "appLinksInterceptor$delegate", "appLinksUseCases", "Lmozilla/components/feature/app/links/AppLinksUseCases;", "getAppLinksUseCases", "()Lmozilla/components/feature/app/links/AppLinksUseCases;", "appLinksUseCases$delegate", "appRequestInterceptor", "Lcom/cookiejarapps/android/smartcookieweb/request/AppRequestInterceptor;", "getAppRequestInterceptor", "()Lcom/cookiejarapps/android/smartcookieweb/request/AppRequestInterceptor;", "appRequestInterceptor$delegate", "client", "Lmozilla/components/concept/fetch/Client;", "getClient", "()Lmozilla/components/concept/fetch/Client;", "client$delegate", "clipboardHandler", "Lcom/cookiejarapps/android/smartcookieweb/utils/ClipboardHandler;", "getClipboardHandler", "()Lcom/cookiejarapps/android/smartcookieweb/utils/ClipboardHandler;", "clipboardHandler$delegate", "contextMenuUseCases", "Lmozilla/components/feature/contextmenu/ContextMenuUseCases;", "getContextMenuUseCases", "()Lmozilla/components/feature/contextmenu/ContextMenuUseCases;", "contextMenuUseCases$delegate", "defaultSearchUseCase", "Lkotlin/Function1;", "", "", "getDefaultSearchUseCase", "()Lkotlin/jvm/functions/Function1;", "defaultSearchUseCase$delegate", "downloadsUseCases", "Lmozilla/components/feature/downloads/DownloadsUseCases;", "getDownloadsUseCases", "()Lmozilla/components/feature/downloads/DownloadsUseCases;", "downloadsUseCases$delegate", "engine", "Lmozilla/components/concept/engine/Engine;", "getEngine", "()Lmozilla/components/concept/engine/Engine;", "engine$delegate", "engineSettings", "Lmozilla/components/concept/engine/DefaultSettings;", "getEngineSettings", "()Lmozilla/components/concept/engine/DefaultSettings;", "engineSettings$delegate", "historyStorage", "Lmozilla/components/browser/storage/sync/PlacesHistoryStorage;", "getHistoryStorage", "()Lmozilla/components/browser/storage/sync/PlacesHistoryStorage;", "historyStorage$delegate", "icons", "Lmozilla/components/browser/icons/BrowserIcons;", "getIcons", "()Lmozilla/components/browser/icons/BrowserIcons;", "icons$delegate", "lazyHistoryStorage", "Lkotlin/Lazy;", "permissionStorage", "Lmozilla/components/browser/engine/gecko/permission/GeckoSitePermissionsStorage;", "getPermissionStorage", "()Lmozilla/components/browser/engine/gecko/permission/GeckoSitePermissionsStorage;", "permissionStorage$delegate", "preferences", "Landroid/content/SharedPreferences;", "getPreferences", "()Landroid/content/SharedPreferences;", "publicSuffixList", "Lmozilla/components/lib/publicsuffixlist/PublicSuffixList;", "getPublicSuffixList", "()Lmozilla/components/lib/publicsuffixlist/PublicSuffixList;", "publicSuffixList$delegate", "runtime", "Lorg/mozilla/geckoview/GeckoRuntime;", "getRuntime", "()Lorg/mozilla/geckoview/GeckoRuntime;", "runtime$delegate", "safeBrowsingPolicy", "", "getSafeBrowsingPolicy", "()I", "safeBrowsingPolicy$delegate", "searchUseCases", "Lmozilla/components/feature/search/SearchUseCases;", "getSearchUseCases", "()Lmozilla/components/feature/search/SearchUseCases;", "searchUseCases$delegate", "sessionStorage", "Lmozilla/components/browser/session/storage/SessionStorage;", "getSessionStorage", "()Lmozilla/components/browser/session/storage/SessionStorage;", "sessionStorage$delegate", "sessionUseCases", "Lmozilla/components/feature/session/SessionUseCases;", "getSessionUseCases", "()Lmozilla/components/feature/session/SessionUseCases;", "sessionUseCases$delegate", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "getStore", "()Lmozilla/components/browser/state/store/BrowserStore;", "store$delegate", "supportedAddonsChecker", "Lmozilla/components/feature/addons/migration/DefaultSupportedAddonsChecker;", "getSupportedAddonsChecker", "()Lmozilla/components/feature/addons/migration/DefaultSupportedAddonsChecker;", "supportedAddonsChecker$delegate", "tabsUseCases", "Lmozilla/components/feature/tabs/TabsUseCases;", "getTabsUseCases", "()Lmozilla/components/feature/tabs/TabsUseCases;", "tabsUseCases$delegate", "thumbnailStorage", "Lmozilla/components/browser/thumbnails/storage/ThumbnailStorage;", "getThumbnailStorage", "()Lmozilla/components/browser/thumbnails/storage/ThumbnailStorage;", "thumbnailStorage$delegate", "trackingPolicy", "Lmozilla/components/concept/engine/EngineSession$TrackingProtectionPolicy;", "getTrackingPolicy", "()Lmozilla/components/concept/engine/EngineSession$TrackingProtectionPolicy;", "trackingPolicy$delegate", "webAppInterceptor", "Lmozilla/components/feature/pwa/WebAppInterceptor;", "getWebAppInterceptor", "()Lmozilla/components/feature/pwa/WebAppInterceptor;", "webAppInterceptor$delegate", "webAppManifestStorage", "Lmozilla/components/feature/pwa/ManifestStorage;", "getWebAppManifestStorage", "()Lmozilla/components/feature/pwa/ManifestStorage;", "webAppManifestStorage$delegate", "webAppShortcutManager", "Lmozilla/components/feature/pwa/WebAppShortcutManager;", "getWebAppShortcutManager", "()Lmozilla/components/feature/pwa/WebAppShortcutManager;", "webAppShortcutManager$delegate", "webAppUseCases", "Lmozilla/components/feature/pwa/WebAppUseCases;", "getWebAppUseCases", "()Lmozilla/components/feature/pwa/WebAppUseCases;", "webAppUseCases$delegate", "darkEnabled", "Lmozilla/components/concept/engine/mediaquery/PreferredColorScheme;", "Companion", "app_debug"})
public class Components {
    private final android.content.Context applicationContext = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.components.Components.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BROWSER_PREFERENCES = "browser_preferences";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PREF_LAUNCH_EXTERNAL_APP = "launch_external_app";
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy publicSuffixList$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy clipboardHandler$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final android.content.SharedPreferences preferences = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy appRequestInterceptor$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy engineSettings$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final mozilla.components.feature.addons.update.DefaultAddonUpdater addonUpdater = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy engine$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy client$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy icons$delegate = null;
    private final kotlin.Lazy<mozilla.components.browser.storage.sync.PlacesHistoryStorage> lazyHistoryStorage = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy historyStorage$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy sessionStorage$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy permissionStorage$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy thumbnailStorage$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy store$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy sessionUseCases$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy addonManager$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy addonCollectionProvider$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy supportedAddonsChecker$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy searchUseCases$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy defaultSearchUseCase$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy appLinksUseCases$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy appLinksInterceptor$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy webAppInterceptor$delegate = null;
    private final kotlin.Lazy runtime$delegate = null;
    private final kotlin.Lazy trackingPolicy$delegate = null;
    private final kotlin.Lazy safeBrowsingPolicy$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy webAppManifestStorage$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy webAppShortcutManager$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy webAppUseCases$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy tabsUseCases$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy downloadsUseCases$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy contextMenuUseCases$delegate = null;
    
    public Components(@org.jetbrains.annotations.NotNull()
    android.content.Context applicationContext) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.lib.publicsuffixlist.PublicSuffixList getPublicSuffixList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.utils.ClipboardHandler getClipboardHandler() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.SharedPreferences getPreferences() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.concept.engine.mediaquery.PreferredColorScheme darkEnabled() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.request.AppRequestInterceptor getAppRequestInterceptor() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.concept.engine.DefaultSettings getEngineSettings() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.feature.addons.update.DefaultAddonUpdater getAddonUpdater() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public mozilla.components.concept.engine.Engine getEngine() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public mozilla.components.concept.fetch.Client getClient() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.browser.icons.BrowserIcons getIcons() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.browser.storage.sync.PlacesHistoryStorage getHistoryStorage() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.browser.session.storage.SessionStorage getSessionStorage() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.browser.engine.gecko.permission.GeckoSitePermissionsStorage getPermissionStorage() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.browser.thumbnails.storage.ThumbnailStorage getThumbnailStorage() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.browser.state.store.BrowserStore getStore() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.feature.session.SessionUseCases getSessionUseCases() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.feature.addons.AddonManager getAddonManager() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.feature.addons.amo.AddonCollectionProvider getAddonCollectionProvider() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.feature.addons.migration.DefaultSupportedAddonsChecker getSupportedAddonsChecker() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.feature.search.SearchUseCases getSearchUseCases() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.jvm.functions.Function1<java.lang.String, kotlin.Unit> getDefaultSearchUseCase() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.feature.app.links.AppLinksUseCases getAppLinksUseCases() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.feature.app.links.AppLinksInterceptor getAppLinksInterceptor() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.feature.pwa.WebAppInterceptor getWebAppInterceptor() {
        return null;
    }
    
    private final org.mozilla.geckoview.GeckoRuntime getRuntime() {
        return null;
    }
    
    private final mozilla.components.concept.engine.EngineSession.TrackingProtectionPolicy getTrackingPolicy() {
        return null;
    }
    
    private final int getSafeBrowsingPolicy() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.feature.pwa.ManifestStorage getWebAppManifestStorage() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.feature.pwa.WebAppShortcutManager getWebAppShortcutManager() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.feature.pwa.WebAppUseCases getWebAppUseCases() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.feature.tabs.TabsUseCases getTabsUseCases() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.feature.downloads.DownloadsUseCases getDownloadsUseCases() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.feature.contextmenu.ContextMenuUseCases getContextMenuUseCases() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/Components$Companion;", "", "()V", "BROWSER_PREFERENCES", "", "PREF_LAUNCH_EXTERNAL_APP", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}