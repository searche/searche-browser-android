package com.cookiejarapps.android.smartcookieweb.components.toolbar;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\b\u0010\u0017\u001a\u00020\u000eH\u0016J\u0010\u0010\u0018\u001a\u00020\u000e2\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\b\u0010\u001b\u001a\u00020\u000eH\u0016J\u0010\u0010\u001c\u001a\u00020\u000e2\u0006\u0010\u001d\u001a\u00020\u000bH\u0016J\u0010\u0010\u001e\u001a\u00020\u000e2\u0006\u0010\u001d\u001a\u00020\u000bH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\u0004\u0018\u00010\u00118BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/DefaultBrowserToolbarController;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarController;", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "activity", "Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;", "navController", "Landroidx/navigation/NavController;", "engineView", "Lmozilla/components/concept/engine/EngineView;", "customTabSessionId", "", "onTabCounterClicked", "Lkotlin/Function0;", "", "(Lmozilla/components/browser/state/store/BrowserStore;Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;Landroidx/navigation/NavController;Lmozilla/components/concept/engine/EngineView;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V", "currentSession", "Lmozilla/components/browser/state/state/SessionState;", "getCurrentSession", "()Lmozilla/components/browser/state/state/SessionState;", "handleScroll", "offset", "", "handleTabCounterClick", "handleTabCounterItemInteraction", "item", "Lmozilla/components/ui/tabcounter/TabCounterMenu$Item;", "handleToolbarClick", "handleToolbarPaste", "text", "handleToolbarPasteAndGo", "app_debug"})
public final class DefaultBrowserToolbarController implements com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarController {
    private final mozilla.components.browser.state.store.BrowserStore store = null;
    private final com.cookiejarapps.android.smartcookieweb.BrowserActivity activity = null;
    private final androidx.navigation.NavController navController = null;
    private final mozilla.components.concept.engine.EngineView engineView = null;
    private final java.lang.String customTabSessionId = null;
    private final kotlin.jvm.functions.Function0<kotlin.Unit> onTabCounterClicked = null;
    
    public DefaultBrowserToolbarController(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore store, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.BrowserActivity activity, @org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController, @org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.EngineView engineView, @org.jetbrains.annotations.Nullable()
    java.lang.String customTabSessionId, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> onTabCounterClicked) {
        super();
    }
    
    private final mozilla.components.browser.state.state.SessionState getCurrentSession() {
        return null;
    }
    
    @java.lang.Override()
    public void handleToolbarPaste(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    @java.lang.Override()
    public void handleToolbarPasteAndGo(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    @java.lang.Override()
    public void handleToolbarClick() {
    }
    
    @java.lang.Override()
    public void handleTabCounterClick() {
    }
    
    @java.lang.Override()
    public void handleTabCounterItemInteraction(@org.jetbrains.annotations.NotNull()
    mozilla.components.ui.tabcounter.TabCounterMenu.Item item) {
    }
    
    @java.lang.Override()
    public void handleScroll(int offset) {
    }
}