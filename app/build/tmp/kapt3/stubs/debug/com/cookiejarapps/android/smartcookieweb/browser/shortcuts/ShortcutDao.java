package com.cookiejarapps.android.smartcookieweb.browser.shortcuts;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0010\u0015\n\u0002\b\u0002\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u0010\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\bH\'J\u000e\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\nH\'J!\u0010\u000b\u001a\u00020\u00032\u0012\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00050\f\"\u00020\u0005H\'\u00a2\u0006\u0002\u0010\rJ\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00050\n2\u0006\u0010\u000f\u001a\u00020\u0010H\'J\u0010\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'\u00a8\u0006\u0012"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/shortcuts/ShortcutDao;", "", "delete", "", "item", "Lcom/cookiejarapps/android/smartcookieweb/browser/shortcuts/ShortcutEntity;", "findByUrl", "urlFind", "", "getAll", "", "insertAll", "", "([Lcom/cookiejarapps/android/smartcookieweb/browser/shortcuts/ShortcutEntity;)V", "loadAllByIds", "shortcutIds", "", "update", "app_debug"})
public abstract interface ShortcutDao {
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM shortcutentity")
    public abstract java.util.List<com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutEntity> getAll();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM shortcutentity WHERE uid IN (:shortcutIds)")
    public abstract java.util.List<com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutEntity> loadAllByIds(@org.jetbrains.annotations.NotNull()
    int[] shortcutIds);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM shortcutentity WHERE url LIKE :urlFind LIMIT 1")
    public abstract com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutEntity findByUrl(@org.jetbrains.annotations.NotNull()
    java.lang.String urlFind);
    
    @androidx.room.Update()
    public abstract void update(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutEntity item);
    
    @androidx.room.Insert()
    public abstract void insertAll(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutEntity... item);
    
    @androidx.room.Delete()
    public abstract void delete(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutEntity item);
}