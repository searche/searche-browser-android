package com.cookiejarapps.android.smartcookieweb.components.toolbar;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001\u0012B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\nJ\u001b\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f2\u0006\u0010\u000e\u001a\u00020\u000fH\u0001\u00a2\u0006\u0002\b\u0010J\u000e\u0010\u0011\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\u000f\u00a8\u0006\u0013"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/TabCounterMenu;", "Lmozilla/components/ui/tabcounter/TabCounterMenu;", "context", "Landroid/content/Context;", "onItemTapped", "Lkotlin/Function1;", "Lmozilla/components/ui/tabcounter/TabCounterMenu$Item;", "", "iconColor", "", "(Landroid/content/Context;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;)V", "menuItems", "", "Lmozilla/components/concept/menu/candidate/MenuCandidate;", "toolbarPosition", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarPosition;", "menuItems$app_debug", "updateMenu", "ExtendedItem", "app_debug"})
public final class TabCounterMenu extends mozilla.components.ui.tabcounter.TabCounterMenu {
    
    public TabCounterMenu(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super mozilla.components.ui.tabcounter.TabCounterMenu.Item, kotlin.Unit> onItemTapped, @org.jetbrains.annotations.Nullable()
    java.lang.Integer iconColor) {
        super(null, null, null);
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.annotation.VisibleForTesting()
    public final java.util.List<mozilla.components.concept.menu.candidate.MenuCandidate> menuItems$app_debug(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarPosition toolbarPosition) {
        return null;
    }
    
    public final void updateMenu(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarPosition toolbarPosition) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0016\u0018\u00002\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/TabCounterMenu$ExtendedItem;", "Lmozilla/components/ui/tabcounter/TabCounterMenu$Item;", "()V", "DuplicateTab", "app_debug"})
    public static class ExtendedItem extends mozilla.components.ui.tabcounter.TabCounterMenu.Item {
        
        public ExtendedItem() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/TabCounterMenu$ExtendedItem$DuplicateTab;", "Lmozilla/components/ui/tabcounter/TabCounterMenu$Item;", "()V", "app_debug"})
        public static final class DuplicateTab extends mozilla.components.ui.tabcounter.TabCounterMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.components.toolbar.TabCounterMenu.ExtendedItem.DuplicateTab INSTANCE = null;
            
            private DuplicateTab() {
                super();
            }
        }
    }
}