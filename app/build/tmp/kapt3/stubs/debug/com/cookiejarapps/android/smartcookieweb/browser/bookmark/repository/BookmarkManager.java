package com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\t\u0018\u0000 02\u00020\u0001:\u00010B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u0017J\u0016\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u0017J\u0010\u0010\u0019\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u00020\u000fH\u0002J\u0010\u0010\u001b\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\nH\u0002J\b\u0010\u001c\u001a\u00020\u0014H\u0002J\u0013\u0010\u001d\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u001e\u001a\u00020\u001fH\u0086\u0002J\u001a\u0010\u001d\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010\t\u001a\u00020\nH\u0002J\u0006\u0010 \u001a\u00020!J\u0010\u0010\"\u001a\u00020!2\b\u0010#\u001a\u0004\u0018\u00010$J\u001e\u0010%\u001a\u00020\u00142\u0006\u0010&\u001a\u00020\n2\u0006\u0010\'\u001a\u00020\n2\u0006\u0010\u0011\u001a\u00020(J\u0016\u0010)\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u0017J\u0018\u0010*\u001a\u00020!2\u0006\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0016\u0010+\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\n2\u0006\u0010,\u001a\u00020(J\u000e\u0010-\u001a\u00020\u00142\u0006\u0010#\u001a\u00020$J\u0010\u0010.\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u00020\u000fH\u0002J\u0006\u0010/\u001a\u00020!R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001e\u0010\r\u001a\u0012\u0012\u0004\u0012\u00020\u000f0\u000ej\b\u0012\u0004\u0012\u00020\u000f`\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00061"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/repository/BookmarkManager;", "Ljava/io/Serializable;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "file", "Ljava/io/File;", "getFile", "()Ljava/io/File;", "root", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;", "getRoot", "()Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;", "siteComparator", "Ljava/util/Comparator;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkSiteItem;", "Lkotlin/Comparator;", "siteIndex", "Ljava/util/ArrayList;", "add", "", "folder", "item", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkItem;", "addFirst", "addToIndex", "site", "addToIndexFromFolder", "createIndex", "get", "id", "", "initialize", "", "isBookmarked", "url", "", "moveTo", "from", "to", "", "moveToFirst", "recursiveRemove", "remove", "index", "removeAll", "removeSiteFromIndex", "save", "Companion", "app_debug"})
public final class BookmarkManager implements java.io.Serializable {
    @org.jetbrains.annotations.NotNull()
    private final java.io.File file = null;
    @org.jetbrains.annotations.NotNull()
    private final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem root = null;
    private final java.util.ArrayList<com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkSiteItem> siteIndex = null;
    private final java.util.Comparator<com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkSiteItem> siteComparator = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository.BookmarkManager.Companion Companion = null;
    private static com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository.BookmarkManager instance;
    
    private BookmarkManager(android.content.Context context) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.io.File getFile() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem getRoot() {
        return null;
    }
    
    public final boolean initialize() {
        return false;
    }
    
    public final boolean save() {
        return false;
    }
    
    public final void addFirst(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem item) {
    }
    
    public final void add(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem item) {
    }
    
    public final void moveToFirst(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem item) {
    }
    
    public final void remove(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder, int index) {
    }
    
    public final void removeAll(@org.jetbrains.annotations.NotNull()
    java.lang.String url) {
    }
    
    private final boolean recursiveRemove(com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder, com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem item) {
        return false;
    }
    
    public final void moveTo(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem from, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem to, int siteIndex) {
    }
    
    public final boolean isBookmarked(@org.jetbrains.annotations.Nullable()
    java.lang.String url) {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem get(long id) {
        return null;
    }
    
    private final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem get(long id, com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem root) {
        return null;
    }
    
    private final void createIndex() {
    }
    
    private final void addToIndexFromFolder(com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder) {
    }
    
    private final void addToIndex(com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkSiteItem site) {
    }
    
    private final void removeSiteFromIndex(com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkSiteItem site) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/repository/BookmarkManager$Companion;", "", "()V", "instance", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/repository/BookmarkManager;", "getInstance", "context", "Landroid/content/Context;", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository.BookmarkManager getInstance(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
    }
}