package com.cookiejarapps.android.smartcookieweb.search.toolbar;

import java.lang.System;

/**
 * View that contains and configures the BrowserToolbar to only be used in its editing mode.
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u000e\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u000f\u001a\u00020\t8\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017\u00a8\u0006\u001c"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/toolbar/ToolbarView;", "", "context", "Landroid/content/Context;", "interactor", "Lcom/cookiejarapps/android/smartcookieweb/search/toolbar/ToolbarInteractor;", "historyStorage", "Lmozilla/components/concept/storage/HistoryStorage;", "isPrivate", "", "view", "Lmozilla/components/browser/toolbar/BrowserToolbar;", "engine", "Lmozilla/components/concept/engine/Engine;", "(Landroid/content/Context;Lcom/cookiejarapps/android/smartcookieweb/search/toolbar/ToolbarInteractor;Lmozilla/components/concept/storage/HistoryStorage;ZLmozilla/components/browser/toolbar/BrowserToolbar;Lmozilla/components/concept/engine/Engine;)V", "isInitialized", "isInitialized$app_debug$annotations", "()V", "isInitialized$app_debug", "()Z", "setInitialized$app_debug", "(Z)V", "getView", "()Lmozilla/components/browser/toolbar/BrowserToolbar;", "update", "", "searchState", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentState;", "app_debug"})
public final class ToolbarView {
    private final android.content.Context context = null;
    private final com.cookiejarapps.android.smartcookieweb.search.toolbar.ToolbarInteractor interactor = null;
    private final mozilla.components.concept.storage.HistoryStorage historyStorage = null;
    private final boolean isPrivate = false;
    @org.jetbrains.annotations.NotNull()
    private final mozilla.components.browser.toolbar.BrowserToolbar view = null;
    private boolean isInitialized = false;
    
    public ToolbarView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.search.toolbar.ToolbarInteractor interactor, @org.jetbrains.annotations.Nullable()
    mozilla.components.concept.storage.HistoryStorage historyStorage, boolean isPrivate, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.toolbar.BrowserToolbar view, @org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.Engine engine) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.browser.toolbar.BrowserToolbar getView() {
        return null;
    }
    
    public final boolean isInitialized$app_debug() {
        return false;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void isInitialized$app_debug$annotations() {
    }
    
    public final void setInitialized$app_debug(boolean p0) {
    }
    
    public final void update(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.search.SearchFragmentState searchState) {
    }
}