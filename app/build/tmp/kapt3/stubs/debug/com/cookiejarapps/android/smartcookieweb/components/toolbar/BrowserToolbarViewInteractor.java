package com.cookiejarapps.android.smartcookieweb.components.toolbar;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tH&J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tH&J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\rH&J\b\u0010\u000e\u001a\u00020\u0003H&J\u0010\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0010H&\u00a8\u0006\u0011"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarViewInteractor;", "", "onBrowserToolbarClicked", "", "onBrowserToolbarMenuItemTapped", "item", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "onBrowserToolbarPaste", "text", "", "onBrowserToolbarPasteAndGo", "onScrolled", "offset", "", "onTabCounterClicked", "onTabCounterMenuItemTapped", "Lmozilla/components/ui/tabcounter/TabCounterMenu$Item;", "app_debug"})
public abstract interface BrowserToolbarViewInteractor {
    
    public abstract void onBrowserToolbarPaste(@org.jetbrains.annotations.NotNull()
    java.lang.String text);
    
    public abstract void onBrowserToolbarPasteAndGo(@org.jetbrains.annotations.NotNull()
    java.lang.String text);
    
    public abstract void onBrowserToolbarClicked();
    
    public abstract void onBrowserToolbarMenuItemTapped(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item item);
    
    public abstract void onTabCounterClicked();
    
    public abstract void onTabCounterMenuItemTapped(@org.jetbrains.annotations.NotNull()
    mozilla.components.ui.tabcounter.TabCounterMenu.Item item);
    
    public abstract void onScrolled(int offset);
}