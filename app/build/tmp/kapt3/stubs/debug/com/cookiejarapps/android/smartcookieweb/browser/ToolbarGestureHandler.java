package com.cookiejarapps.android.smartcookieweb.browser;

import java.lang.System;

@kotlin.Suppress(names = {"LargeClass", "TooManyFunctions"})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 72\u00020\u0001:\u0003789B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u0010\u001f\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020!H\u0002J\u0018\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u001e2\u0006\u0010%\u001a\u00020&H\u0002J\b\u0010\'\u001a\u00020(H\u0002J\u0010\u0010)\u001a\u00020*2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0018\u0010+\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010,\u001a\u00020\u001eH\u0016J\u0018\u0010-\u001a\u00020*2\u0006\u0010.\u001a\u00020/2\u0006\u00100\u001a\u00020/H\u0016J\u0018\u00101\u001a\u00020\u001c2\u0006\u00102\u001a\u00020\u001e2\u0006\u00103\u001a\u00020\u001eH\u0016J\u0010\u00104\u001a\u00020\u001c2\u0006\u00105\u001a\u00020(H\u0002J\f\u00106\u001a\u00020**\u00020/H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u00020\u00118BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0015\u0010\u0016R\u0018\u0010\u0017\u001a\u00020\u0011*\u00020\u00188BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0019\u0010\u001a\u00a8\u0006:"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/ToolbarGestureHandler;", "Lcom/cookiejarapps/android/smartcookieweb/browser/SwipeGestureListener;", "activity", "Landroid/app/Activity;", "contentLayout", "Landroid/view/View;", "tabPreview", "Lcom/cookiejarapps/android/smartcookieweb/browser/FakeTab;", "toolbarLayout", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "selectTabUseCase", "Lmozilla/components/feature/tabs/TabsUseCases$SelectTabUseCase;", "(Landroid/app/Activity;Landroid/view/View;Lcom/cookiejarapps/android/smartcookieweb/browser/FakeTab;Landroid/view/View;Lmozilla/components/browser/state/store/BrowserStore;Lmozilla/components/feature/tabs/TabsUseCases$SelectTabUseCase;)V", "gestureDirection", "Lcom/cookiejarapps/android/smartcookieweb/browser/ToolbarGestureHandler$GestureDirection;", "minimumFlingVelocity", "", "previewOffset", "touchSlop", "windowWidth", "getWindowWidth", "()I", "visibleWidth", "Landroid/graphics/Rect;", "getVisibleWidth", "(Landroid/graphics/Rect;)I", "animateCanceledGesture", "", "velocityX", "", "animateToNextTab", "tab", "Lmozilla/components/browser/state/state/TabSessionState;", "getAnimator", "Landroid/animation/ValueAnimator;", "finalContextX", "duration", "", "getDestination", "Lcom/cookiejarapps/android/smartcookieweb/browser/ToolbarGestureHandler$Destination;", "isGestureComplete", "", "onSwipeFinished", "velocityY", "onSwipeStarted", "start", "Landroid/graphics/PointF;", "next", "onSwipeUpdate", "distanceX", "distanceY", "preparePreview", "destination", "isInToolbar", "Companion", "Destination", "GestureDirection", "app_debug"})
public final class ToolbarGestureHandler implements com.cookiejarapps.android.smartcookieweb.browser.SwipeGestureListener {
    private final android.app.Activity activity = null;
    private final android.view.View contentLayout = null;
    private final com.cookiejarapps.android.smartcookieweb.browser.FakeTab tabPreview = null;
    private final android.view.View toolbarLayout = null;
    private final mozilla.components.browser.state.store.BrowserStore store = null;
    private final mozilla.components.feature.tabs.TabsUseCases.SelectTabUseCase selectTabUseCase = null;
    private final int previewOffset = 0;
    private final int touchSlop = 0;
    private final int minimumFlingVelocity = 0;
    private com.cookiejarapps.android.smartcookieweb.browser.ToolbarGestureHandler.GestureDirection gestureDirection = com.cookiejarapps.android.smartcookieweb.browser.ToolbarGestureHandler.GestureDirection.LEFT_TO_RIGHT;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.browser.ToolbarGestureHandler.Companion Companion = null;
    
    /**
     * The percentage of the tab preview that needs to be visible to consider the
     * tab switching gesture complete.
     */
    private static final double GESTURE_FINISH_PERCENT = 0.25;
    
    /**
     * The percentage of the content view that can be hidden by the tab switching gesture if
     * there is not tab available to switch to
     */
    private static final double OVERSCROLL_HIDE_PERCENT = 0.2;
    
    /**
     * Animation duration when switching to another tab
     */
    private static final long FINISHED_GESTURE_ANIMATION_DURATION = 250L;
    
    /**
     * Animation duration gesture is canceled due to the swipe not being far enough
     */
    private static final long CANCELED_GESTURE_ANIMATION_DURATION = 200L;
    
    /**
     * Animation duration gesture is canceled due to a swipe in the opposite direction
     */
    private static final long CANCELED_FLING_ANIMATION_DURATION = 150L;
    
    public ToolbarGestureHandler(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    android.view.View contentLayout, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.FakeTab tabPreview, @org.jetbrains.annotations.NotNull()
    android.view.View toolbarLayout, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore store, @org.jetbrains.annotations.NotNull()
    mozilla.components.feature.tabs.TabsUseCases.SelectTabUseCase selectTabUseCase) {
        super();
    }
    
    private final int getWindowWidth() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean onSwipeStarted(@org.jetbrains.annotations.NotNull()
    android.graphics.PointF start, @org.jetbrains.annotations.NotNull()
    android.graphics.PointF next) {
        return false;
    }
    
    @java.lang.Override()
    public void onSwipeUpdate(float distanceX, float distanceY) {
    }
    
    @java.lang.Override()
    public void onSwipeFinished(float velocityX, float velocityY) {
    }
    
    private final com.cookiejarapps.android.smartcookieweb.browser.ToolbarGestureHandler.Destination getDestination() {
        return null;
    }
    
    private final void preparePreview(com.cookiejarapps.android.smartcookieweb.browser.ToolbarGestureHandler.Destination destination) {
    }
    
    private final boolean isGestureComplete(float velocityX) {
        return false;
    }
    
    private final android.animation.ValueAnimator getAnimator(float finalContextX, long duration) {
        return null;
    }
    
    private final void animateToNextTab(mozilla.components.browser.state.state.TabSessionState tab) {
    }
    
    private final void animateCanceledGesture(float velocityX) {
    }
    
    private final boolean isInToolbar(android.graphics.PointF $this$isInToolbar) {
        return false;
    }
    
    private final int getVisibleWidth(android.graphics.Rect $this$visibleWidth) {
        return 0;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0082\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/ToolbarGestureHandler$GestureDirection;", "", "(Ljava/lang/String;I)V", "LEFT_TO_RIGHT", "RIGHT_TO_LEFT", "app_debug"})
    static enum GestureDirection {
        /*public static final*/ LEFT_TO_RIGHT /* = new LEFT_TO_RIGHT() */,
        /*public static final*/ RIGHT_TO_LEFT /* = new RIGHT_TO_LEFT() */;
        
        GestureDirection() {
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b2\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0002\u0005\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/ToolbarGestureHandler$Destination;", "", "()V", "None", "Tab", "Lcom/cookiejarapps/android/smartcookieweb/browser/ToolbarGestureHandler$Destination$Tab;", "Lcom/cookiejarapps/android/smartcookieweb/browser/ToolbarGestureHandler$Destination$None;", "app_debug"})
    static abstract class Destination {
        
        private Destination() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0011"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/ToolbarGestureHandler$Destination$Tab;", "Lcom/cookiejarapps/android/smartcookieweb/browser/ToolbarGestureHandler$Destination;", "tab", "Lmozilla/components/browser/state/state/TabSessionState;", "(Lmozilla/components/browser/state/state/TabSessionState;)V", "getTab", "()Lmozilla/components/browser/state/state/TabSessionState;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_debug"})
        public static final class Tab extends com.cookiejarapps.android.smartcookieweb.browser.ToolbarGestureHandler.Destination {
            @org.jetbrains.annotations.NotNull()
            private final mozilla.components.browser.state.state.TabSessionState tab = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.cookiejarapps.android.smartcookieweb.browser.ToolbarGestureHandler.Destination.Tab copy(@org.jetbrains.annotations.NotNull()
            mozilla.components.browser.state.state.TabSessionState tab) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Tab(@org.jetbrains.annotations.NotNull()
            mozilla.components.browser.state.state.TabSessionState tab) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final mozilla.components.browser.state.state.TabSessionState component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final mozilla.components.browser.state.state.TabSessionState getTab() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/ToolbarGestureHandler$Destination$None;", "Lcom/cookiejarapps/android/smartcookieweb/browser/ToolbarGestureHandler$Destination;", "()V", "app_debug"})
        public static final class None extends com.cookiejarapps.android.smartcookieweb.browser.ToolbarGestureHandler.Destination {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.browser.ToolbarGestureHandler.Destination.None INSTANCE = null;
            
            private None() {
                super();
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\bX\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/ToolbarGestureHandler$Companion;", "", "()V", "CANCELED_FLING_ANIMATION_DURATION", "", "CANCELED_GESTURE_ANIMATION_DURATION", "FINISHED_GESTURE_ANIMATION_DURATION", "GESTURE_FINISH_PERCENT", "", "OVERSCROLL_HIDE_PERCENT", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}