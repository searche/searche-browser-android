package com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0088\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b&\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u0002*\u0004\b\u0001\u0010\u00032\u00020\u0004B3\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\b\u0010\t\u001a\u0004\u0018\u00018\u0000\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\f\u001a\u00028\u0001\u00a2\u0006\u0002\u0010\rJ\b\u00108\u001a\u00020-H\u0002J\b\u00109\u001a\u00020:H\u0004J\'\u0010;\u001a\u00020<2\u0006\u0010=\u001a\u00020:2\b\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00028\u0001H\u0014\u00a2\u0006\u0002\u0010>J)\u0010?\u001a\u0004\u0018\u00018\u00002\b\u0010@\u001a\u0004\u0018\u00018\u00002\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u000bH$\u00a2\u0006\u0002\u0010AJ\u0018\u0010B\u001a\u00020C2\u0006\u0010D\u001a\u00020E2\u0006\u0010F\u001a\u00020-H\u0016J4\u0010G\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u001a\b\u0004\u0010H\u001a\u0014\u0012\u0004\u0012\u00020E\u0012\u0004\u0012\u00020J\u0012\u0004\u0012\u00020<0IH\u0086\b\u00f8\u0001\u0000J\u001a\u0010G\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0006\u0010K\u001a\u00020\'J\u0006\u0010L\u001a\u00020<R\u0014\u0010\u000e\u001a\u00020\u000fX\u0084\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0005\u001a\u00020\u0006X\u0084\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\u00020\u0015X\u0084\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u0019X\u0084\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0014\u0010\u001c\u001a\u00020\u001dX\u0084\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0018\u0010\t\u001a\u0004\u0018\u00018\u0000X\u0084\u0004\u00a2\u0006\n\n\u0002\u0010\"\u001a\u0004\b \u0010!R\u0014\u0010#\u001a\u00020\bX\u0084\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u001c\u0010&\u001a\u0004\u0018\u00010\'X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u001a\u0010,\u001a\u00020-X\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u0014\u00102\u001a\u000203X\u0084\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b4\u00105R\u0014\u00106\u001a\u000203X\u0084\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b7\u00105\u0082\u0002\u0007\n\u0005\b\u009920\u0001\u00a8\u0006M"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/AddBookmarkDialog;", "S", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkItem;", "T", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkFoldersDialog$OnFolderSelectedListener;", "context", "Landroid/content/Context;", "manager", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/repository/BookmarkManager;", "mItem", "title", "", "url", "(Landroid/content/Context;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/repository/BookmarkManager;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkItem;Ljava/lang/String;Ljava/lang/Object;)V", "addToTopCheckBox", "Landroid/widget/CheckBox;", "getAddToTopCheckBox", "()Landroid/widget/CheckBox;", "getContext", "()Landroid/content/Context;", "folderButton", "Landroidx/appcompat/widget/AppCompatTextView;", "getFolderButton", "()Landroidx/appcompat/widget/AppCompatTextView;", "folderTextView", "Landroid/widget/TextView;", "getFolderTextView", "()Landroid/widget/TextView;", "mDialog", "Landroidx/appcompat/app/AlertDialog;", "getMDialog", "()Landroidx/appcompat/app/AlertDialog;", "getMItem", "()Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkItem;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkItem;", "mManager", "getMManager", "()Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/repository/BookmarkManager;", "mOnClickListener", "Landroid/content/DialogInterface$OnClickListener;", "getMOnClickListener", "()Landroid/content/DialogInterface$OnClickListener;", "setMOnClickListener", "(Landroid/content/DialogInterface$OnClickListener;)V", "mParent", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;", "getMParent", "()Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;", "setMParent", "(Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;)V", "titleEditText", "Landroid/widget/EditText;", "getTitleEditText", "()Landroid/widget/EditText;", "urlEditText", "getUrlEditText", "getRootPosition", "inflateView", "Landroid/view/View;", "initView", "", "view", "(Landroid/view/View;Ljava/lang/String;Ljava/lang/Object;)V", "makeItem", "item", "(Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkItem;Ljava/lang/String;Ljava/lang/String;)Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkItem;", "onFolderSelected", "", "dialog", "Landroid/content/DialogInterface;", "folder", "setOnClickListener", "listener", "Lkotlin/Function2;", "", "l", "show", "app_debug"})
public abstract class AddBookmarkDialog<S extends com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem, T extends java.lang.Object> implements com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkFoldersDialog.OnFolderSelectedListener {
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    @org.jetbrains.annotations.Nullable()
    private final S mItem = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.appcompat.app.AlertDialog mDialog = null;
    @org.jetbrains.annotations.NotNull()
    private final android.widget.EditText titleEditText = null;
    @org.jetbrains.annotations.NotNull()
    private final android.widget.EditText urlEditText = null;
    @org.jetbrains.annotations.NotNull()
    private final android.widget.TextView folderTextView = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.appcompat.widget.AppCompatTextView folderButton = null;
    @org.jetbrains.annotations.NotNull()
    private final android.widget.CheckBox addToTopCheckBox = null;
    @org.jetbrains.annotations.Nullable()
    private android.content.DialogInterface.OnClickListener mOnClickListener;
    @org.jetbrains.annotations.NotNull()
    private final com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository.BookmarkManager mManager = null;
    protected com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem mParent;
    
    public AddBookmarkDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository.BookmarkManager manager, @org.jetbrains.annotations.Nullable()
    S mItem, @org.jetbrains.annotations.Nullable()
    java.lang.String title, T url) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final android.content.Context getContext() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final S getMItem() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final androidx.appcompat.app.AlertDialog getMDialog() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final android.widget.EditText getTitleEditText() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final android.widget.EditText getUrlEditText() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final android.widget.TextView getFolderTextView() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final androidx.appcompat.widget.AppCompatTextView getFolderButton() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final android.widget.CheckBox getAddToTopCheckBox() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final android.content.DialogInterface.OnClickListener getMOnClickListener() {
        return null;
    }
    
    protected final void setMOnClickListener(@org.jetbrains.annotations.Nullable()
    android.content.DialogInterface.OnClickListener p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository.BookmarkManager getMManager() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem getMParent() {
        return null;
    }
    
    protected final void setMParent(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final android.view.View inflateView() {
        return null;
    }
    
    protected void initView(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    java.lang.String title, T url) {
    }
    
    private final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem getRootPosition() {
        return null;
    }
    
    public final void show() {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected abstract S makeItem(@org.jetbrains.annotations.Nullable()
    S item, @org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String url);
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.AddBookmarkDialog<S, T> setOnClickListener(@org.jetbrains.annotations.NotNull()
    android.content.DialogInterface.OnClickListener l) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.AddBookmarkDialog<S, T> setOnClickListener(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function2<? super android.content.DialogInterface, ? super java.lang.Integer, kotlin.Unit> listener) {
        return null;
    }
    
    @java.lang.Override()
    public boolean onFolderSelected(@org.jetbrains.annotations.NotNull()
    android.content.DialogInterface dialog, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder) {
        return false;
    }
}