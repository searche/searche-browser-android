package com.cookiejarapps.android.smartcookieweb.components.toolbar;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\b\u0010\u0006\u001a\u00020\u0003H&J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tH&J\b\u0010\n\u001a\u00020\u0003H&J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\rH&J\u0010\u0010\u000e\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\rH&\u00a8\u0006\u000f"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarController;", "", "handleScroll", "", "offset", "", "handleTabCounterClick", "handleTabCounterItemInteraction", "item", "Lmozilla/components/ui/tabcounter/TabCounterMenu$Item;", "handleToolbarClick", "handleToolbarPaste", "text", "", "handleToolbarPasteAndGo", "app_debug"})
public abstract interface BrowserToolbarController {
    
    public abstract void handleScroll(int offset);
    
    public abstract void handleToolbarPaste(@org.jetbrains.annotations.NotNull()
    java.lang.String text);
    
    public abstract void handleToolbarPasteAndGo(@org.jetbrains.annotations.NotNull()
    java.lang.String text);
    
    public abstract void handleToolbarClick();
    
    public abstract void handleTabCounterClick();
    
    public abstract void handleTabCounterItemInteraction(@org.jetbrains.annotations.NotNull()
    mozilla.components.ui.tabcounter.TabCounterMenu.Item item);
}