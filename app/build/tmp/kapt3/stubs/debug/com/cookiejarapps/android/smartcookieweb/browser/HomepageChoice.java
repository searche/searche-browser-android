package com.cookiejarapps.android.smartcookieweb.browser;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005\u00a8\u0006\u0006"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/HomepageChoice;", "", "(Ljava/lang/String;I)V", "VIEW", "BLANK_PAGE", "CUSTOM_PAGE", "app_debug"})
public enum HomepageChoice {
    /*public static final*/ VIEW /* = new VIEW() */,
    /*public static final*/ BLANK_PAGE /* = new BLANK_PAGE() */,
    /*public static final*/ CUSTOM_PAGE /* = new CUSTOM_PAGE() */;
    
    HomepageChoice() {
    }
}