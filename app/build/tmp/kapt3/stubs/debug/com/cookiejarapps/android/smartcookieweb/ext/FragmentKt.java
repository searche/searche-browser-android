package com.cookiejarapps.android.smartcookieweb.ext;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000(\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a/\u0010\u0005\u001a\u00020\u0006*\u00020\u00022\n\b\u0001\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\t\u001a\u00020\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010\r\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004\u00a8\u0006\u000e"}, d2 = {"components", "Lcom/cookiejarapps/android/smartcookieweb/components/Components;", "Landroidx/fragment/app/Fragment;", "getComponents", "(Landroidx/fragment/app/Fragment;)Lcom/cookiejarapps/android/smartcookieweb/components/Components;", "nav", "", "id", "", "directions", "Landroidx/navigation/NavDirections;", "options", "Landroidx/navigation/NavOptions;", "(Landroidx/fragment/app/Fragment;Ljava/lang/Integer;Landroidx/navigation/NavDirections;Landroidx/navigation/NavOptions;)V", "app_debug"})
public final class FragmentKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.components.Components getComponents(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $this$components) {
        return null;
    }
    
    public static final void nav(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $this$nav, @org.jetbrains.annotations.Nullable()
    @androidx.annotation.IdRes()
    java.lang.Integer id, @org.jetbrains.annotations.NotNull()
    androidx.navigation.NavDirections directions, @org.jetbrains.annotations.Nullable()
    androidx.navigation.NavOptions options) {
    }
}