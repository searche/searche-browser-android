package com.cookiejarapps.android.smartcookieweb.search.awesomebar;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tH&J\b\u0010\n\u001a\u00020\u0003H&J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\u0006H&J\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u0006H&\u00a8\u0006\u000f"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/awesomebar/AwesomeBarInteractor;", "", "onClickSearchEngineSettings", "", "onExistingSessionSelected", "tabId", "", "onSearchShortcutEngineSelected", "searchEngine", "Lmozilla/components/browser/state/search/SearchEngine;", "onSearchShortcutsButtonClicked", "onSearchTermsTapped", "searchTerms", "onUrlTapped", "url", "app_debug"})
public abstract interface AwesomeBarInteractor {
    
    public abstract void onUrlTapped(@org.jetbrains.annotations.NotNull()
    java.lang.String url);
    
    public abstract void onSearchTermsTapped(@org.jetbrains.annotations.NotNull()
    java.lang.String searchTerms);
    
    public abstract void onSearchShortcutEngineSelected(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.search.SearchEngine searchEngine);
    
    public abstract void onClickSearchEngineSettings();
    
    public abstract void onExistingSessionSelected(@org.jetbrains.annotations.NotNull()
    java.lang.String tabId);
    
    public abstract void onSearchShortcutsButtonClicked();
}