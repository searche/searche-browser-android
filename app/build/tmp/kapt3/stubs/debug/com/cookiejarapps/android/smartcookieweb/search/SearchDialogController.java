package com.cookiejarapps.android.smartcookieweb.search;

import java.lang.System;

@kotlin.Suppress(names = {"TooManyFunctions", "LongParameterList"})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001BW\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r\u0012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0002\u0010\u0011J\b\u0010\u0012\u001a\u00020\u000eH\u0016J\b\u0010\u0013\u001a\u00020\u000eH\u0016J\u0010\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u000e2\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\b\u0010\u001a\u001a\u00020\u000eH\u0016J\u0010\u0010\u001b\u001a\u00020\u000e2\u0006\u0010\u001c\u001a\u00020\u0016H\u0016J\u0010\u0010\u001d\u001a\u00020\u000e2\u0006\u0010\u001e\u001a\u00020\u0016H\u0016J\u0010\u0010\u001f\u001a\u00020\u000e2\u0006\u0010 \u001a\u00020\u0016H\u0016J\u0010\u0010!\u001a\u00020\u000e2\u0006\u0010 \u001a\u00020\u0016H\u0016J\u0010\u0010\"\u001a\u00020\u000e2\u0006\u0010 \u001a\u00020\u0016H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/SearchDialogController;", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchController;", "activity", "Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "tabsUseCases", "Lmozilla/components/feature/tabs/TabsUseCases;", "fragmentStore", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentStore;", "navController", "Landroidx/navigation/NavController;", "dismissDialog", "Lkotlin/Function0;", "", "clearToolbarFocus", "focusToolbar", "(Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;Lmozilla/components/browser/state/store/BrowserStore;Lmozilla/components/feature/tabs/TabsUseCases;Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentStore;Landroidx/navigation/NavController;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "handleClickSearchEngineSettings", "handleEditingCancelled", "handleExistingSessionSelected", "tabId", "", "handleSearchShortcutEngineSelected", "searchEngine", "Lmozilla/components/browser/state/search/SearchEngine;", "handleSearchShortcutsButtonClicked", "handleSearchTermsTapped", "searchTerms", "handleTextChanged", "text", "handleUrlCommitted", "url", "handleUrlTapped", "openSearchOrUrl", "app_debug"})
public final class SearchDialogController implements com.cookiejarapps.android.smartcookieweb.search.SearchController {
    private final com.cookiejarapps.android.smartcookieweb.BrowserActivity activity = null;
    private final mozilla.components.browser.state.store.BrowserStore store = null;
    private final mozilla.components.feature.tabs.TabsUseCases tabsUseCases = null;
    private final com.cookiejarapps.android.smartcookieweb.search.SearchFragmentStore fragmentStore = null;
    private final androidx.navigation.NavController navController = null;
    private final kotlin.jvm.functions.Function0<kotlin.Unit> dismissDialog = null;
    private final kotlin.jvm.functions.Function0<kotlin.Unit> clearToolbarFocus = null;
    private final kotlin.jvm.functions.Function0<kotlin.Unit> focusToolbar = null;
    
    public SearchDialogController(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.BrowserActivity activity, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore store, @org.jetbrains.annotations.NotNull()
    mozilla.components.feature.tabs.TabsUseCases tabsUseCases, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.search.SearchFragmentStore fragmentStore, @org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> dismissDialog, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> clearToolbarFocus, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> focusToolbar) {
        super();
    }
    
    @java.lang.Override()
    public void handleUrlCommitted(@org.jetbrains.annotations.NotNull()
    java.lang.String url) {
    }
    
    private final void openSearchOrUrl(java.lang.String url) {
    }
    
    @java.lang.Override()
    public void handleEditingCancelled() {
    }
    
    @java.lang.Override()
    public void handleTextChanged(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    @java.lang.Override()
    public void handleUrlTapped(@org.jetbrains.annotations.NotNull()
    java.lang.String url) {
    }
    
    @java.lang.Override()
    public void handleSearchTermsTapped(@org.jetbrains.annotations.NotNull()
    java.lang.String searchTerms) {
    }
    
    @java.lang.Override()
    public void handleSearchShortcutEngineSelected(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.search.SearchEngine searchEngine) {
    }
    
    @java.lang.Override()
    public void handleSearchShortcutsButtonClicked() {
    }
    
    @java.lang.Override()
    public void handleClickSearchEngineSettings() {
    }
    
    @java.lang.Override()
    public void handleExistingSessionSelected(@org.jetbrains.annotations.NotNull()
    java.lang.String tabId) {
    }
}