package com.cookiejarapps.android.smartcookieweb.components.toolbar;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0002\u00a8\u0006\u0005"}, d2 = {"updateSearchTermsOfSelectedSession", "", "Lmozilla/components/browser/state/store/BrowserStore;", "searchTerms", "", "app_debug"})
public final class BrowserToolbarControllerKt {
    
    private static final void updateSearchTermsOfSelectedSession(mozilla.components.browser.state.store.BrowserStore $this$updateSearchTermsOfSelectedSession, java.lang.String searchTerms) {
    }
}