package com.cookiejarapps.android.smartcookieweb.search;

import java.lang.System;

/**
 * The state for the Search Screen
 *
 * @property query The current search query string
 * @property url The current URL of the tab (if this fragment is shown for an already existing tab)
 * @property searchTerms The search terms used to search previously in this tab (if this fragment is shown
 * for an already existing tab)
 * @property searchEngineSource The current selected search engine with the context of how it was selected
 * @property defaultEngine The current default search engine (or null if none is available yet)
 * @property showSearchSuggestions Whether or not to show search suggestions from the search engine in the AwesomeBar
 * @property showSearchSuggestionsHint Whether or not to show search suggestions in private hint panel
 * @property showSearchShortcuts Whether or not to show search shortcuts in the AwesomeBar
 * @property areShortcutsAvailable Whether or not there are >=2 search engines installed
 * so to know to present users with certain options or not.
 * @property showSearchShortcutsSetting Whether the setting for showing search shortcuts is enabled
 * or disabled.
 * @property showClipboardSuggestions Whether or not to show clipboard suggestion in the AwesomeBar
 * @property showHistorySuggestions Whether or not to show history suggestions in the AwesomeBar
 * @property showBookmarkSuggestions Whether or not to show the bookmark suggestion in the AwesomeBar
 * @property pastedText The text pasted from the long press toolbar menu
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b2\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u008d\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\u000b\u0012\u0006\u0010\r\u001a\u00020\u000b\u0012\u0006\u0010\u000e\u001a\u00020\u000b\u0012\u0006\u0010\u000f\u001a\u00020\u000b\u0012\u0006\u0010\u0010\u001a\u00020\u000b\u0012\u0006\u0010\u0011\u001a\u00020\u000b\u0012\u0006\u0010\u0012\u001a\u00020\u000b\u0012\u0006\u0010\u0013\u001a\u00020\u000b\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0016J\t\u0010+\u001a\u00020\u0003H\u00c6\u0003J\t\u0010,\u001a\u00020\u000bH\u00c6\u0003J\t\u0010-\u001a\u00020\u000bH\u00c6\u0003J\t\u0010.\u001a\u00020\u000bH\u00c6\u0003J\t\u0010/\u001a\u00020\u000bH\u00c6\u0003J\t\u00100\u001a\u00020\u000bH\u00c6\u0003J\u000b\u00101\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u00102\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u00103\u001a\u00020\u0003H\u00c6\u0003J\t\u00104\u001a\u00020\u0003H\u00c6\u0003J\t\u00105\u001a\u00020\u0007H\u00c6\u0003J\u000b\u00106\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\t\u00107\u001a\u00020\u000bH\u00c6\u0003J\t\u00108\u001a\u00020\u000bH\u00c6\u0003J\t\u00109\u001a\u00020\u000bH\u00c6\u0003J\t\u0010:\u001a\u00020\u000bH\u00c6\u0003J\u00af\u0001\u0010;\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\u000b2\b\b\u0002\u0010\r\u001a\u00020\u000b2\b\b\u0002\u0010\u000e\u001a\u00020\u000b2\b\b\u0002\u0010\u000f\u001a\u00020\u000b2\b\b\u0002\u0010\u0010\u001a\u00020\u000b2\b\b\u0002\u0010\u0011\u001a\u00020\u000b2\b\b\u0002\u0010\u0012\u001a\u00020\u000b2\b\b\u0002\u0010\u0013\u001a\u00020\u000b2\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010<\u001a\u00020\u000b2\b\u0010=\u001a\u0004\u0018\u00010>H\u00d6\u0003J\t\u0010?\u001a\u00020@H\u00d6\u0001J\t\u0010A\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u000e\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0013\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0013\u0010\u0015\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u001cR\u0011\u0010\u0012\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\u0018R\u0011\u0010\u0010\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0018R\u0011\u0010\u0011\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010\u0018R\u0011\u0010\r\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010\u0018R\u0011\u0010\u000f\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010\u0018R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\u0018R\u0011\u0010\f\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\'\u0010\u0018R\u0011\u0010\u0013\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b(\u0010\u0018R\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010\u001cR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010\u001c\u00a8\u0006B"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentState;", "Lmozilla/components/lib/state/State;", "query", "", "url", "searchTerms", "searchEngineSource", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchEngineSource;", "defaultEngine", "Lmozilla/components/browser/state/search/SearchEngine;", "showSearchSuggestions", "", "showSearchSuggestionsHint", "showSearchShortcuts", "areShortcutsAvailable", "showSearchShortcutsSetting", "showClipboardSuggestions", "showHistorySuggestions", "showBookmarkSuggestions", "showSyncedTabsSuggestions", "tabId", "pastedText", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/cookiejarapps/android/smartcookieweb/search/SearchEngineSource;Lmozilla/components/browser/state/search/SearchEngine;ZZZZZZZZZLjava/lang/String;Ljava/lang/String;)V", "getAreShortcutsAvailable", "()Z", "getDefaultEngine", "()Lmozilla/components/browser/state/search/SearchEngine;", "getPastedText", "()Ljava/lang/String;", "getQuery", "getSearchEngineSource", "()Lcom/cookiejarapps/android/smartcookieweb/search/SearchEngineSource;", "getSearchTerms", "getShowBookmarkSuggestions", "getShowClipboardSuggestions", "getShowHistorySuggestions", "getShowSearchShortcuts", "getShowSearchShortcutsSetting", "getShowSearchSuggestions", "getShowSearchSuggestionsHint", "getShowSyncedTabsSuggestions", "getTabId", "getUrl", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "", "hashCode", "", "toString", "app_debug"})
public final class SearchFragmentState implements mozilla.components.lib.state.State {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String query = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String url = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String searchTerms = null;
    @org.jetbrains.annotations.NotNull()
    private final com.cookiejarapps.android.smartcookieweb.search.SearchEngineSource searchEngineSource = null;
    @org.jetbrains.annotations.Nullable()
    private final mozilla.components.browser.state.search.SearchEngine defaultEngine = null;
    private final boolean showSearchSuggestions = false;
    private final boolean showSearchSuggestionsHint = false;
    private final boolean showSearchShortcuts = false;
    private final boolean areShortcutsAvailable = false;
    private final boolean showSearchShortcutsSetting = false;
    private final boolean showClipboardSuggestions = false;
    private final boolean showHistorySuggestions = false;
    private final boolean showBookmarkSuggestions = false;
    private final boolean showSyncedTabsSuggestions = false;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String tabId = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String pastedText = null;
    
    /**
     * The state for the Search Screen
     *
     * @property query The current search query string
     * @property url The current URL of the tab (if this fragment is shown for an already existing tab)
     * @property searchTerms The search terms used to search previously in this tab (if this fragment is shown
     * for an already existing tab)
     * @property searchEngineSource The current selected search engine with the context of how it was selected
     * @property defaultEngine The current default search engine (or null if none is available yet)
     * @property showSearchSuggestions Whether or not to show search suggestions from the search engine in the AwesomeBar
     * @property showSearchSuggestionsHint Whether or not to show search suggestions in private hint panel
     * @property showSearchShortcuts Whether or not to show search shortcuts in the AwesomeBar
     * @property areShortcutsAvailable Whether or not there are >=2 search engines installed
     * so to know to present users with certain options or not.
     * @property showSearchShortcutsSetting Whether the setting for showing search shortcuts is enabled
     * or disabled.
     * @property showClipboardSuggestions Whether or not to show clipboard suggestion in the AwesomeBar
     * @property showHistorySuggestions Whether or not to show history suggestions in the AwesomeBar
     * @property showBookmarkSuggestions Whether or not to show the bookmark suggestion in the AwesomeBar
     * @property pastedText The text pasted from the long press toolbar menu
     */
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.search.SearchFragmentState copy(@org.jetbrains.annotations.NotNull()
    java.lang.String query, @org.jetbrains.annotations.NotNull()
    java.lang.String url, @org.jetbrains.annotations.NotNull()
    java.lang.String searchTerms, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.search.SearchEngineSource searchEngineSource, @org.jetbrains.annotations.Nullable()
    mozilla.components.browser.state.search.SearchEngine defaultEngine, boolean showSearchSuggestions, boolean showSearchSuggestionsHint, boolean showSearchShortcuts, boolean areShortcutsAvailable, boolean showSearchShortcutsSetting, boolean showClipboardSuggestions, boolean showHistorySuggestions, boolean showBookmarkSuggestions, boolean showSyncedTabsSuggestions, @org.jetbrains.annotations.Nullable()
    java.lang.String tabId, @org.jetbrains.annotations.Nullable()
    java.lang.String pastedText) {
        return null;
    }
    
    /**
     * The state for the Search Screen
     *
     * @property query The current search query string
     * @property url The current URL of the tab (if this fragment is shown for an already existing tab)
     * @property searchTerms The search terms used to search previously in this tab (if this fragment is shown
     * for an already existing tab)
     * @property searchEngineSource The current selected search engine with the context of how it was selected
     * @property defaultEngine The current default search engine (or null if none is available yet)
     * @property showSearchSuggestions Whether or not to show search suggestions from the search engine in the AwesomeBar
     * @property showSearchSuggestionsHint Whether or not to show search suggestions in private hint panel
     * @property showSearchShortcuts Whether or not to show search shortcuts in the AwesomeBar
     * @property areShortcutsAvailable Whether or not there are >=2 search engines installed
     * so to know to present users with certain options or not.
     * @property showSearchShortcutsSetting Whether the setting for showing search shortcuts is enabled
     * or disabled.
     * @property showClipboardSuggestions Whether or not to show clipboard suggestion in the AwesomeBar
     * @property showHistorySuggestions Whether or not to show history suggestions in the AwesomeBar
     * @property showBookmarkSuggestions Whether or not to show the bookmark suggestion in the AwesomeBar
     * @property pastedText The text pasted from the long press toolbar menu
     */
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    /**
     * The state for the Search Screen
     *
     * @property query The current search query string
     * @property url The current URL of the tab (if this fragment is shown for an already existing tab)
     * @property searchTerms The search terms used to search previously in this tab (if this fragment is shown
     * for an already existing tab)
     * @property searchEngineSource The current selected search engine with the context of how it was selected
     * @property defaultEngine The current default search engine (or null if none is available yet)
     * @property showSearchSuggestions Whether or not to show search suggestions from the search engine in the AwesomeBar
     * @property showSearchSuggestionsHint Whether or not to show search suggestions in private hint panel
     * @property showSearchShortcuts Whether or not to show search shortcuts in the AwesomeBar
     * @property areShortcutsAvailable Whether or not there are >=2 search engines installed
     * so to know to present users with certain options or not.
     * @property showSearchShortcutsSetting Whether the setting for showing search shortcuts is enabled
     * or disabled.
     * @property showClipboardSuggestions Whether or not to show clipboard suggestion in the AwesomeBar
     * @property showHistorySuggestions Whether or not to show history suggestions in the AwesomeBar
     * @property showBookmarkSuggestions Whether or not to show the bookmark suggestion in the AwesomeBar
     * @property pastedText The text pasted from the long press toolbar menu
     */
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    /**
     * The state for the Search Screen
     *
     * @property query The current search query string
     * @property url The current URL of the tab (if this fragment is shown for an already existing tab)
     * @property searchTerms The search terms used to search previously in this tab (if this fragment is shown
     * for an already existing tab)
     * @property searchEngineSource The current selected search engine with the context of how it was selected
     * @property defaultEngine The current default search engine (or null if none is available yet)
     * @property showSearchSuggestions Whether or not to show search suggestions from the search engine in the AwesomeBar
     * @property showSearchSuggestionsHint Whether or not to show search suggestions in private hint panel
     * @property showSearchShortcuts Whether or not to show search shortcuts in the AwesomeBar
     * @property areShortcutsAvailable Whether or not there are >=2 search engines installed
     * so to know to present users with certain options or not.
     * @property showSearchShortcutsSetting Whether the setting for showing search shortcuts is enabled
     * or disabled.
     * @property showClipboardSuggestions Whether or not to show clipboard suggestion in the AwesomeBar
     * @property showHistorySuggestions Whether or not to show history suggestions in the AwesomeBar
     * @property showBookmarkSuggestions Whether or not to show the bookmark suggestion in the AwesomeBar
     * @property pastedText The text pasted from the long press toolbar menu
     */
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public SearchFragmentState(@org.jetbrains.annotations.NotNull()
    java.lang.String query, @org.jetbrains.annotations.NotNull()
    java.lang.String url, @org.jetbrains.annotations.NotNull()
    java.lang.String searchTerms, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.search.SearchEngineSource searchEngineSource, @org.jetbrains.annotations.Nullable()
    mozilla.components.browser.state.search.SearchEngine defaultEngine, boolean showSearchSuggestions, boolean showSearchSuggestionsHint, boolean showSearchShortcuts, boolean areShortcutsAvailable, boolean showSearchShortcutsSetting, boolean showClipboardSuggestions, boolean showHistorySuggestions, boolean showBookmarkSuggestions, boolean showSyncedTabsSuggestions, @org.jetbrains.annotations.Nullable()
    java.lang.String tabId, @org.jetbrains.annotations.Nullable()
    java.lang.String pastedText) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQuery() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUrl() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSearchTerms() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.search.SearchEngineSource component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.search.SearchEngineSource getSearchEngineSource() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final mozilla.components.browser.state.search.SearchEngine component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final mozilla.components.browser.state.search.SearchEngine getDefaultEngine() {
        return null;
    }
    
    public final boolean component6() {
        return false;
    }
    
    public final boolean getShowSearchSuggestions() {
        return false;
    }
    
    public final boolean component7() {
        return false;
    }
    
    public final boolean getShowSearchSuggestionsHint() {
        return false;
    }
    
    public final boolean component8() {
        return false;
    }
    
    public final boolean getShowSearchShortcuts() {
        return false;
    }
    
    public final boolean component9() {
        return false;
    }
    
    public final boolean getAreShortcutsAvailable() {
        return false;
    }
    
    public final boolean component10() {
        return false;
    }
    
    public final boolean getShowSearchShortcutsSetting() {
        return false;
    }
    
    public final boolean component11() {
        return false;
    }
    
    public final boolean getShowClipboardSuggestions() {
        return false;
    }
    
    public final boolean component12() {
        return false;
    }
    
    public final boolean getShowHistorySuggestions() {
        return false;
    }
    
    public final boolean component13() {
        return false;
    }
    
    public final boolean getShowBookmarkSuggestions() {
        return false;
    }
    
    public final boolean component14() {
        return false;
    }
    
    public final boolean getShowSyncedTabsSuggestions() {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component15() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTabId() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component16() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPastedText() {
        return null;
    }
}