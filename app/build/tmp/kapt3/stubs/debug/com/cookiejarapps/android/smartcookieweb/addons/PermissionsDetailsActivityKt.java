package com.cookiejarapps.android.smartcookieweb.addons;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u000e\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0002"}, d2 = {"LEARN_MORE_URL", "", "app_debug"})
public final class PermissionsDetailsActivityKt {
    private static final java.lang.String LEARN_MORE_URL = "https://smartcookieweb.com/help-biscuit/extensions/#permission-requests";
}