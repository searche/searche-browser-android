package com.cookiejarapps.android.smartcookieweb.search;

import java.lang.System;

/**
 * Interactor for the search screen
 * Provides implementations for the AwesomeBarView and ToolbarView
 */
@kotlin.Suppress(names = {"TooManyFunctions"})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\b\u0010\u0006\u001a\u00020\u0007H\u0016J\b\u0010\b\u001a\u00020\u0007H\u0016J\u0010\u0010\t\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\f\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u0007H\u0016J\u0010\u0010\u0010\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u000bH\u0016J\u0010\u0010\u0012\u001a\u00020\u00072\u0006\u0010\u0013\u001a\u00020\u000bH\u0016J\u0010\u0010\u0014\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00020\u000bH\u0016J\u0010\u0010\u0016\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00020\u000bH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/SearchDialogInteractor;", "Lcom/cookiejarapps/android/smartcookieweb/search/awesomebar/AwesomeBarInteractor;", "Lcom/cookiejarapps/android/smartcookieweb/search/toolbar/ToolbarInteractor;", "searchController", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchDialogController;", "(Lcom/cookiejarapps/android/smartcookieweb/search/SearchDialogController;)V", "onClickSearchEngineSettings", "", "onEditingCanceled", "onExistingSessionSelected", "tabId", "", "onSearchShortcutEngineSelected", "searchEngine", "Lmozilla/components/browser/state/search/SearchEngine;", "onSearchShortcutsButtonClicked", "onSearchTermsTapped", "searchTerms", "onTextChanged", "text", "onUrlCommitted", "url", "onUrlTapped", "app_debug"})
public final class SearchDialogInteractor implements com.cookiejarapps.android.smartcookieweb.search.awesomebar.AwesomeBarInteractor, com.cookiejarapps.android.smartcookieweb.search.toolbar.ToolbarInteractor {
    private final com.cookiejarapps.android.smartcookieweb.search.SearchDialogController searchController = null;
    
    public SearchDialogInteractor(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.search.SearchDialogController searchController) {
        super();
    }
    
    @java.lang.Override()
    public void onUrlCommitted(@org.jetbrains.annotations.NotNull()
    java.lang.String url) {
    }
    
    @java.lang.Override()
    public void onEditingCanceled() {
    }
    
    @java.lang.Override()
    public void onTextChanged(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    @java.lang.Override()
    public void onUrlTapped(@org.jetbrains.annotations.NotNull()
    java.lang.String url) {
    }
    
    @java.lang.Override()
    public void onSearchTermsTapped(@org.jetbrains.annotations.NotNull()
    java.lang.String searchTerms) {
    }
    
    @java.lang.Override()
    public void onSearchShortcutEngineSelected(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.search.SearchEngine searchEngine) {
    }
    
    @java.lang.Override()
    public void onSearchShortcutsButtonClicked() {
    }
    
    @java.lang.Override()
    public void onClickSearchEngineSettings() {
    }
    
    @java.lang.Override()
    public void onExistingSessionSelected(@org.jetbrains.annotations.NotNull()
    java.lang.String tabId) {
    }
}