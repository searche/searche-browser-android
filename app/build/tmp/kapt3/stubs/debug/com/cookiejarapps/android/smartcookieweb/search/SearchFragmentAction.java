package com.cookiejarapps.android.smartcookieweb.search;

import java.lang.System;

/**
 * Actions to dispatch through the `SearchStore` to modify `SearchState` through the reducer.
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0006\u0003\u0004\u0005\u0006\u0007\bB\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0006\t\n\u000b\f\r\u000e\u00a8\u0006\u000f"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction;", "Lmozilla/components/lib/state/Action;", "()V", "AllowSearchSuggestionsInPrivateModePrompt", "SearchShortcutEngineSelected", "SetShowSearchSuggestions", "ShowSearchShortcutEnginePicker", "UpdateQuery", "UpdateSearchState", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction$SetShowSearchSuggestions;", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction$SearchShortcutEngineSelected;", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction$ShowSearchShortcutEnginePicker;", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction$AllowSearchSuggestionsInPrivateModePrompt;", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction$UpdateQuery;", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction$UpdateSearchState;", "app_debug"})
public abstract class SearchFragmentAction implements mozilla.components.lib.state.Action {
    
    private SearchFragmentAction() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\u00032\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00d6\u0003J\t\u0010\f\u001a\u00020\rH\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction$SetShowSearchSuggestions;", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction;", "show", "", "(Z)V", "getShow", "()Z", "component1", "copy", "equals", "other", "", "hashCode", "", "toString", "", "app_debug"})
    public static final class SetShowSearchSuggestions extends com.cookiejarapps.android.smartcookieweb.search.SearchFragmentAction {
        private final boolean show = false;
        
        @org.jetbrains.annotations.NotNull()
        public final com.cookiejarapps.android.smartcookieweb.search.SearchFragmentAction.SetShowSearchSuggestions copy(boolean show) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public SetShowSearchSuggestions(boolean show) {
            super();
        }
        
        public final boolean component1() {
            return false;
        }
        
        public final boolean getShow() {
            return false;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0011"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction$SearchShortcutEngineSelected;", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction;", "engine", "Lmozilla/components/browser/state/search/SearchEngine;", "(Lmozilla/components/browser/state/search/SearchEngine;)V", "getEngine", "()Lmozilla/components/browser/state/search/SearchEngine;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_debug"})
    public static final class SearchShortcutEngineSelected extends com.cookiejarapps.android.smartcookieweb.search.SearchFragmentAction {
        @org.jetbrains.annotations.NotNull()
        private final mozilla.components.browser.state.search.SearchEngine engine = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.cookiejarapps.android.smartcookieweb.search.SearchFragmentAction.SearchShortcutEngineSelected copy(@org.jetbrains.annotations.NotNull()
        mozilla.components.browser.state.search.SearchEngine engine) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public SearchShortcutEngineSelected(@org.jetbrains.annotations.NotNull()
        mozilla.components.browser.state.search.SearchEngine engine) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final mozilla.components.browser.state.search.SearchEngine component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final mozilla.components.browser.state.search.SearchEngine getEngine() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\u00032\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00d6\u0003J\t\u0010\f\u001a\u00020\rH\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction$ShowSearchShortcutEnginePicker;", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction;", "show", "", "(Z)V", "getShow", "()Z", "component1", "copy", "equals", "other", "", "hashCode", "", "toString", "", "app_debug"})
    public static final class ShowSearchShortcutEnginePicker extends com.cookiejarapps.android.smartcookieweb.search.SearchFragmentAction {
        private final boolean show = false;
        
        @org.jetbrains.annotations.NotNull()
        public final com.cookiejarapps.android.smartcookieweb.search.SearchFragmentAction.ShowSearchShortcutEnginePicker copy(boolean show) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public ShowSearchShortcutEnginePicker(boolean show) {
            super();
        }
        
        public final boolean component1() {
            return false;
        }
        
        public final boolean getShow() {
            return false;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\u00032\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00d6\u0003J\t\u0010\f\u001a\u00020\rH\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction$AllowSearchSuggestionsInPrivateModePrompt;", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction;", "show", "", "(Z)V", "getShow", "()Z", "component1", "copy", "equals", "other", "", "hashCode", "", "toString", "", "app_debug"})
    public static final class AllowSearchSuggestionsInPrivateModePrompt extends com.cookiejarapps.android.smartcookieweb.search.SearchFragmentAction {
        private final boolean show = false;
        
        @org.jetbrains.annotations.NotNull()
        public final com.cookiejarapps.android.smartcookieweb.search.SearchFragmentAction.AllowSearchSuggestionsInPrivateModePrompt copy(boolean show) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public AllowSearchSuggestionsInPrivateModePrompt(boolean show) {
            super();
        }
        
        public final boolean component1() {
            return false;
        }
        
        public final boolean getShow() {
            return false;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction$UpdateQuery;", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction;", "query", "", "(Ljava/lang/String;)V", "getQuery", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_debug"})
    public static final class UpdateQuery extends com.cookiejarapps.android.smartcookieweb.search.SearchFragmentAction {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String query = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.cookiejarapps.android.smartcookieweb.search.SearchFragmentAction.UpdateQuery copy(@org.jetbrains.annotations.NotNull()
        java.lang.String query) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public UpdateQuery(@org.jetbrains.annotations.NotNull()
        java.lang.String query) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getQuery() {
            return null;
        }
    }
    
    /**
     * Updates the local `SearchFragmentState` from the global `SearchState` in `BrowserStore`.
     */
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0011"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction$UpdateSearchState;", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction;", "search", "Lmozilla/components/browser/state/state/SearchState;", "(Lmozilla/components/browser/state/state/SearchState;)V", "getSearch", "()Lmozilla/components/browser/state/state/SearchState;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_debug"})
    public static final class UpdateSearchState extends com.cookiejarapps.android.smartcookieweb.search.SearchFragmentAction {
        @org.jetbrains.annotations.NotNull()
        private final mozilla.components.browser.state.state.SearchState search = null;
        
        /**
         * Updates the local `SearchFragmentState` from the global `SearchState` in `BrowserStore`.
         */
        @org.jetbrains.annotations.NotNull()
        public final com.cookiejarapps.android.smartcookieweb.search.SearchFragmentAction.UpdateSearchState copy(@org.jetbrains.annotations.NotNull()
        mozilla.components.browser.state.state.SearchState search) {
            return null;
        }
        
        /**
         * Updates the local `SearchFragmentState` from the global `SearchState` in `BrowserStore`.
         */
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        /**
         * Updates the local `SearchFragmentState` from the global `SearchState` in `BrowserStore`.
         */
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        /**
         * Updates the local `SearchFragmentState` from the global `SearchState` in `BrowserStore`.
         */
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public UpdateSearchState(@org.jetbrains.annotations.NotNull()
        mozilla.components.browser.state.state.SearchState search) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final mozilla.components.browser.state.state.SearchState component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final mozilla.components.browser.state.state.SearchState getSearch() {
            return null;
        }
    }
}