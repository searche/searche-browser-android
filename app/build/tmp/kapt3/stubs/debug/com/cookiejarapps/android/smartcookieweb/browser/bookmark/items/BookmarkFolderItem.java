package com.cookiejarapps.android.smartcookieweb.browser.bookmark.items;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 ,2\u00020\u00012\u00020\u0002:\u0001,B!\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0000\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u000e\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0000J\u000e\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u0001J\u000e\u0010\u001d\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0000J\u0006\u0010\u001e\u001a\u00020\u001aJ\u0011\u0010\u001f\u001a\u00020\u00012\u0006\u0010 \u001a\u00020\u0016H\u0086\u0002J\u000e\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$J\u001a\u0010%\u001a\u00020\"2\b\u0010&\u001a\u0004\u0018\u00010\u00042\u0006\u0010#\u001a\u00020$H\u0014J\u0006\u0010\'\u001a\u00020\u0016J\u000e\u0010(\u001a\u00020\"2\u0006\u0010)\u001a\u00020*J\u0010\u0010+\u001a\u00020\"2\u0006\u0010)\u001a\u00020*H\u0014R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00010\n8F\u00a2\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00010\u000eX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0000X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\u00020\u00168TX\u0094\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018\u00a8\u0006-"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkItem;", "Ljava/io/Serializable;", "title", "", "parent", "id", "", "(Ljava/lang/String;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;J)V", "itemList", "", "getItemList", "()Ljava/util/List;", "list", "Ljava/util/ArrayList;", "getList$app_debug", "()Ljava/util/ArrayList;", "getParent", "()Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;", "setParent", "(Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;)V", "type", "", "getType", "()I", "add", "", "folder", "item", "addAtStart", "clear", "get", "index", "readForRoot", "", "reader", "Lcom/squareup/moshi/JsonReader;", "readMain", "name", "size", "writeForRoot", "writer", "Lcom/squareup/moshi/JsonWriter;", "writeMain", "Companion", "app_debug"})
public final class BookmarkFolderItem extends com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem implements java.io.Serializable {
    @org.jetbrains.annotations.Nullable()
    private com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem parent;
    @org.jetbrains.annotations.NotNull()
    private final java.util.ArrayList<com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem> list = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String COLUMN_NAME_LIST = "2";
    public static final int BOOKMARK_ITEM_ID = 1;
    
    public BookmarkFolderItem(@org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem parent, long id) {
        super(null, 0L);
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem getParent() {
        return null;
    }
    
    public final void setParent(@org.jetbrains.annotations.Nullable()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem> getList$app_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem> getItemList() {
        return null;
    }
    
    @java.lang.Override()
    protected int getType() {
        return 0;
    }
    
    public final void add(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem item) {
    }
    
    public final void add(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder) {
    }
    
    public final void addAtStart(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem folder) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem get(int index) {
        return null;
    }
    
    public final int size() {
        return 0;
    }
    
    public final void clear() {
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.io.IOException.class})
    @java.lang.Override()
    protected boolean writeMain(@org.jetbrains.annotations.NotNull()
    com.squareup.moshi.JsonWriter writer) throws java.io.IOException {
        return false;
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.io.IOException.class})
    @java.lang.Override()
    protected boolean readMain(@org.jetbrains.annotations.Nullable()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    com.squareup.moshi.JsonReader reader) throws java.io.IOException {
        return false;
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.io.IOException.class})
    public final boolean writeForRoot(@org.jetbrains.annotations.NotNull()
    com.squareup.moshi.JsonWriter writer) throws java.io.IOException {
        return false;
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.io.IOException.class})
    public final boolean readForRoot(@org.jetbrains.annotations.NotNull()
    com.squareup.moshi.JsonReader reader) throws java.io.IOException {
        return false;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem$Companion;", "", "()V", "BOOKMARK_ITEM_ID", "", "COLUMN_NAME_LIST", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}