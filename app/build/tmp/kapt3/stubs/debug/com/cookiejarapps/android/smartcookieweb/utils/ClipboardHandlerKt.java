package com.cookiejarapps.android.smartcookieweb.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0003"}, d2 = {"MIME_TYPE_TEXT_HTML", "", "MIME_TYPE_TEXT_PLAIN", "app_debug"})
public final class ClipboardHandlerKt {
    private static final java.lang.String MIME_TYPE_TEXT_PLAIN = "text/plain";
    private static final java.lang.String MIME_TYPE_TEXT_HTML = "text/html";
}