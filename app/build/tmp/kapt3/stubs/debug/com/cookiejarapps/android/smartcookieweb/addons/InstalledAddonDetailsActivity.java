package com.cookiejarapps.android.smartcookieweb.addons;

import java.lang.System;

/**
 * An activity to show the details of a installed add-on.
 */
@kotlin.Suppress(names = {"LargeClass"})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0010\u0010\n\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0010\u0010\f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0010\u0010\r\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0010\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0010\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0010\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0010\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0012\u0010\u0012\u001a\u00020\u00062\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0014J\u0014\u0010\u0015\u001a\u00020\u0006*\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/addons/InstalledAddonDetailsActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "scope", "Lkotlinx/coroutines/CoroutineScope;", "bindAddon", "", "addon", "Lmozilla/components/feature/addons/Addon;", "bindAllowInPrivateBrowsingSwitch", "bindDetails", "bindEnableSwitch", "bindPermissions", "bindRate", "bindRemoveButton", "bindReport", "bindSettings", "bindUI", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "setState", "Landroidx/appcompat/widget/SwitchCompat;", "checked", "", "app_debug"})
public final class InstalledAddonDetailsActivity extends androidx.appcompat.app.AppCompatActivity {
    private final kotlinx.coroutines.CoroutineScope scope = null;
    
    public InstalledAddonDetailsActivity() {
        super();
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void bindAddon(mozilla.components.feature.addons.Addon addon) {
    }
    
    private final void bindUI(mozilla.components.feature.addons.Addon addon) {
    }
    
    private final void bindEnableSwitch(mozilla.components.feature.addons.Addon addon) {
    }
    
    private final void bindSettings(mozilla.components.feature.addons.Addon addon) {
    }
    
    private final void bindDetails(mozilla.components.feature.addons.Addon addon) {
    }
    
    private final void bindPermissions(mozilla.components.feature.addons.Addon addon) {
    }
    
    private final void bindRate(mozilla.components.feature.addons.Addon addon) {
    }
    
    private final void bindReport(mozilla.components.feature.addons.Addon addon) {
    }
    
    private final void bindAllowInPrivateBrowsingSwitch(mozilla.components.feature.addons.Addon addon) {
    }
    
    private final void bindRemoveButton(mozilla.components.feature.addons.Addon addon) {
    }
    
    private final void setState(androidx.appcompat.widget.SwitchCompat $this$setState, boolean checked) {
    }
}