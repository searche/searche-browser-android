package com.cookiejarapps.android.smartcookieweb.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\f\u0010\u001a\u001a\u00020\u001b*\u00020\u0006H\u0002J\f\u0010\u001c\u001a\u00020\u001b*\u00020\u0006H\u0002J\f\u0010\u001d\u001a\u00020\u001b*\u00020\u0006H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u0004\u0018\u00010\n8@X\u0081\u0004\u00a2\u0006\f\u0012\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000eR(\u0010\u0010\u001a\u0004\u0018\u00010\n2\b\u0010\u000f\u001a\u0004\u0018\u00010\n8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b\u0011\u0010\u000e\"\u0004\b\u0012\u0010\u0013R\u0013\u0010\u0014\u001a\u0004\u0018\u00010\n8F\u00a2\u0006\u0006\u001a\u0004\b\u0015\u0010\u000eR\u001a\u0010\u0016\u001a\u0004\u0018\u00010\u0017*\u00020\u00068BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019\u00a8\u0006\u001e"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/utils/ClipboardHandler;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "clipboard", "Landroid/content/ClipboardManager;", "getContext", "()Landroid/content/Context;", "firstSafePrimaryClipItemText", "", "getFirstSafePrimaryClipItemText$app_debug$annotations", "()V", "getFirstSafePrimaryClipItemText$app_debug", "()Ljava/lang/String;", "value", "text", "getText", "setText", "(Ljava/lang/String;)V", "url", "getUrl", "firstPrimaryClipItem", "Landroid/content/ClipData$Item;", "getFirstPrimaryClipItem", "(Landroid/content/ClipboardManager;)Landroid/content/ClipData$Item;", "isPrimaryClipEmpty", "", "isPrimaryClipHtmlText", "isPrimaryClipPlainText", "app_debug"})
public final class ClipboardHandler {
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    private final android.content.ClipboardManager clipboard = null;
    
    public ClipboardHandler(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getText() {
        return null;
    }
    
    public final void setText(@org.jetbrains.annotations.Nullable()
    java.lang.String value) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUrl() {
        return null;
    }
    
    private final boolean isPrimaryClipPlainText(android.content.ClipboardManager $this$isPrimaryClipPlainText) {
        return false;
    }
    
    private final boolean isPrimaryClipHtmlText(android.content.ClipboardManager $this$isPrimaryClipHtmlText) {
        return false;
    }
    
    private final boolean isPrimaryClipEmpty(android.content.ClipboardManager $this$isPrimaryClipEmpty) {
        return false;
    }
    
    private final android.content.ClipData.Item getFirstPrimaryClipItem(android.content.ClipboardManager $this$firstPrimaryClipItem) {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getFirstSafePrimaryClipItemText$app_debug$annotations() {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFirstSafePrimaryClipItemText$app_debug() {
        return null;
    }
}