package com.cookiejarapps.android.smartcookieweb;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J \u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/OpenSpecificTabIntentProcessor;", "Lcom/cookiejarapps/android/smartcookieweb/HomeIntentProcessor;", "activity", "Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;", "(Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;)V", "process", "", "intent", "Landroid/content/Intent;", "navController", "Landroidx/navigation/NavController;", "out", "app_debug"})
public final class OpenSpecificTabIntentProcessor implements com.cookiejarapps.android.smartcookieweb.HomeIntentProcessor {
    private final com.cookiejarapps.android.smartcookieweb.BrowserActivity activity = null;
    
    public OpenSpecificTabIntentProcessor(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.BrowserActivity activity) {
        super();
    }
    
    @java.lang.Override()
    public boolean process(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent, @org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController, @org.jetbrains.annotations.NotNull()
    android.content.Intent out) {
        return false;
    }
}