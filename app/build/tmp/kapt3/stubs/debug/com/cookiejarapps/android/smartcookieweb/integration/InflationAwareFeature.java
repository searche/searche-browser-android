package com.cookiejarapps.android.smartcookieweb.integration;

import java.lang.System;

/**
 * A base feature class that enables lazy inflation of a view needed by a feature.
 *
 * When a feature needs to be launched (e.g. by user interaction) calling [launch]
 * will inflate the view only then, start the feature, and then executes [onLaunch]
 * for any feature-specific startup needs.
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\b&\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\b\u0010\u0014\u001a\u00020\u0015H\u0007J\b\u0010\u0016\u001a\u00020\u0017H\u0016J\u0018\u0010\u0018\u001a\u00020\u00152\u0006\u0010\r\u001a\u00020\u000f2\u0006\u0010\u0006\u001a\u00020\u0001H&J\u0010\u0010\u0019\u001a\u00020\u00012\u0006\u0010\r\u001a\u00020\u000fH&J\b\u0010\u001a\u001a\u00020\u0015H\u0016J\b\u0010\u001b\u001a\u00020\u0015H\u0016R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0001X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eX\u0080.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013\u00a8\u0006\u001c"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/integration/InflationAwareFeature;", "Lmozilla/components/support/base/feature/LifecycleAwareFeature;", "Lmozilla/components/support/base/feature/UserInteractionHandler;", "stub", "Landroid/view/ViewStub;", "(Landroid/view/ViewStub;)V", "feature", "getFeature$app_debug", "()Lmozilla/components/support/base/feature/LifecycleAwareFeature;", "setFeature$app_debug", "(Lmozilla/components/support/base/feature/LifecycleAwareFeature;)V", "stubListener", "Landroid/view/ViewStub$OnInflateListener;", "view", "Ljava/lang/ref/WeakReference;", "Landroid/view/View;", "getView$app_debug", "()Ljava/lang/ref/WeakReference;", "setView$app_debug", "(Ljava/lang/ref/WeakReference;)V", "launch", "", "onBackPressed", "", "onLaunch", "onViewInflated", "start", "stop", "app_debug"})
public abstract class InflationAwareFeature implements mozilla.components.support.base.feature.LifecycleAwareFeature, mozilla.components.support.base.feature.UserInteractionHandler {
    private final android.view.ViewStub stub = null;
    public java.lang.ref.WeakReference<android.view.View> view;
    @org.jetbrains.annotations.Nullable()
    private mozilla.components.support.base.feature.LifecycleAwareFeature feature;
    private final android.view.ViewStub.OnInflateListener stubListener = null;
    
    public InflationAwareFeature(@org.jetbrains.annotations.NotNull()
    android.view.ViewStub stub) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.ref.WeakReference<android.view.View> getView$app_debug() {
        return null;
    }
    
    public final void setView$app_debug(@org.jetbrains.annotations.NotNull()
    java.lang.ref.WeakReference<android.view.View> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final mozilla.components.support.base.feature.LifecycleAwareFeature getFeature$app_debug() {
        return null;
    }
    
    public final void setFeature$app_debug(@org.jetbrains.annotations.Nullable()
    mozilla.components.support.base.feature.LifecycleAwareFeature p0) {
    }
    
    /**
     * Invoked when a view-dependent feature needs to be started along with the feature itself.
     */
    @androidx.annotation.UiThread()
    public final void launch() {
    }
    
    /**
     * Implementation notes: This implemented method does nothing since we only start the feature
     * when the view is inflated.
     */
    @java.lang.Override()
    public void start() {
    }
    
    @java.lang.Override()
    public void stop() {
    }
    
    /**
     * Called when the feature gets the option to handle the user pressing the back key.
     *
     * @return true if the feature also implements [UserInteractionHandler] and the feature has
     * been initiated.
     */
    @java.lang.Override()
    public boolean onBackPressed() {
        return false;
    }
    
    /**
     * Invoked when the view has been inflated for the feature to be created with it.
     *
     * @param view The newly created view.
     * @return The feature initiated with the view.
     */
    @org.jetbrains.annotations.NotNull()
    public abstract mozilla.components.support.base.feature.LifecycleAwareFeature onViewInflated(@org.jetbrains.annotations.NotNull()
    android.view.View view);
    
    /**
     * Invoked after the feature is instantiated. If the feature already exists,
     * this is invoked immediately.
     *
     * @param view The view that is attached to the feature.
     * @param feature The feature that was instantiated.
     */
    public abstract void onLaunch(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.NotNull()
    mozilla.components.support.base.feature.LifecycleAwareFeature feature);
    
    public boolean onHomePressed() {
        return false;
    }
}