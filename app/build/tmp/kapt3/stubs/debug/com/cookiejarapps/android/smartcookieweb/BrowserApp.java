package com.cookiejarapps.android.smartcookieweb;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000b\u001a\u00020\fH\u0016J\u0010\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\b\u0010\u0010\u001a\u00020\u0011H\u0002R\u001b\u0010\u0003\u001a\u00020\u00048FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/BrowserApp;", "Landroid/app/Application;", "()V", "components", "Lcom/cookiejarapps/android/smartcookieweb/components/Components;", "getComponents", "()Lcom/cookiejarapps/android/smartcookieweb/components/Components;", "components$delegate", "Lkotlin/Lazy;", "logger", "Lmozilla/components/support/base/log/logger/Logger;", "onCreate", "", "onTrimMemory", "level", "", "restoreBrowserState", "Lkotlinx/coroutines/Job;", "app_debug"})
public final class BrowserApp extends android.app.Application {
    private final mozilla.components.support.base.log.logger.Logger logger = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy components$delegate = null;
    
    public BrowserApp() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.components.Components getComponents() {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    private final kotlinx.coroutines.Job restoreBrowserState() {
        return null;
    }
    
    @java.lang.Override()
    public void onTrimMemory(int level) {
    }
}