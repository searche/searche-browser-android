package com.cookiejarapps.android.smartcookieweb;

import java.lang.System;

@kotlin.Suppress(names = {"LongParameterList"})
@kotlinx.coroutines.ExperimentalCoroutinesApi()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001BM\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011\u00a8\u0006\u0012"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/TabButtonFeature;", "", "toolbar", "Lmozilla/components/concept/toolbar/Toolbar;", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "sessionId", "", "lifecycleOwner", "Landroidx/lifecycle/LifecycleOwner;", "showTabs", "Lkotlin/Function0;", "", "tabCounterMenu", "Lmozilla/components/ui/tabcounter/TabCounterMenu;", "countBasedOnSelectedTabType", "", "(Lmozilla/components/concept/toolbar/Toolbar;Lmozilla/components/browser/state/store/BrowserStore;Ljava/lang/String;Landroidx/lifecycle/LifecycleOwner;Lkotlin/jvm/functions/Function0;Lmozilla/components/ui/tabcounter/TabCounterMenu;Z)V", "app_debug"})
public final class TabButtonFeature {
    
    public TabButtonFeature(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.toolbar.Toolbar toolbar, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore store, @org.jetbrains.annotations.Nullable()
    java.lang.String sessionId, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LifecycleOwner lifecycleOwner, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> showTabs, @org.jetbrains.annotations.Nullable()
    mozilla.components.ui.tabcounter.TabCounterMenu tabCounterMenu, boolean countBasedOnSelectedTabType) {
        super();
    }
}