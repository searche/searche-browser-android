package com.cookiejarapps.android.smartcookieweb.theme;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u00048G\u00a2\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/theme/FirefoxTheme;", "", "()V", "colors", "Lcom/cookiejarapps/android/smartcookieweb/theme/FirefoxColors;", "getColors", "()Lcom/cookiejarapps/android/smartcookieweb/theme/FirefoxColors;", "app_debug"})
public final class FirefoxTheme {
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.theme.FirefoxTheme INSTANCE = null;
    
    private FirefoxTheme() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.compose.runtime.Composable()
    public final com.cookiejarapps.android.smartcookieweb.theme.FirefoxColors getColors() {
        return null;
    }
}