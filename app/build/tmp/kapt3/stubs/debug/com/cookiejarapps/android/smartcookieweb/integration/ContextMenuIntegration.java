package com.cookiejarapps.android.smartcookieweb.integration;

import java.lang.System;

@kotlin.Suppress(names = {"LongParameterList", "UndocumentedPublicClass"})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001BA\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\u0002\u0010\u0010J\b\u0010\u0018\u001a\u00020\u0019H\u0016J\b\u0010\u001a\u001a\u00020\u0019H\u0016R\u0017\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/integration/ContextMenuIntegration;", "Lmozilla/components/support/base/feature/LifecycleAwareFeature;", "context", "Landroid/content/Context;", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "browserStore", "Lmozilla/components/browser/state/store/BrowserStore;", "tabsUseCases", "Lmozilla/components/feature/tabs/TabsUseCases;", "contextMenuUseCases", "Lmozilla/components/feature/contextmenu/ContextMenuUseCases;", "parentView", "Landroid/view/View;", "sessionId", "", "(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;Lmozilla/components/browser/state/store/BrowserStore;Lmozilla/components/feature/tabs/TabsUseCases;Lmozilla/components/feature/contextmenu/ContextMenuUseCases;Landroid/view/View;Ljava/lang/String;)V", "candidates", "", "Lmozilla/components/feature/contextmenu/ContextMenuCandidate;", "getCandidates", "()Ljava/util/List;", "feature", "Lmozilla/components/feature/contextmenu/ContextMenuFeature;", "start", "", "stop", "app_debug"})
public final class ContextMenuIntegration implements mozilla.components.support.base.feature.LifecycleAwareFeature {
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<mozilla.components.feature.contextmenu.ContextMenuCandidate> candidates = null;
    private final mozilla.components.feature.contextmenu.ContextMenuFeature feature = null;
    
    public ContextMenuIntegration(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentManager fragmentManager, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore browserStore, @org.jetbrains.annotations.NotNull()
    mozilla.components.feature.tabs.TabsUseCases tabsUseCases, @org.jetbrains.annotations.NotNull()
    mozilla.components.feature.contextmenu.ContextMenuUseCases contextMenuUseCases, @org.jetbrains.annotations.NotNull()
    android.view.View parentView, @org.jetbrains.annotations.Nullable()
    java.lang.String sessionId) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<mozilla.components.feature.contextmenu.ContextMenuCandidate> getCandidates() {
        return null;
    }
    
    @java.lang.Override()
    public void start() {
    }
    
    @java.lang.Override()
    public void stop() {
    }
}