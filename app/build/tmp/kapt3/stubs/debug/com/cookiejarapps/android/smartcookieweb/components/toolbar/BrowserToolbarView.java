package com.cookiejarapps.android.smartcookieweb.components.toolbar;

import java.lang.System;

@java.lang.SuppressWarnings(value = {"LargeClass"})
@kotlinx.coroutines.ExperimentalCoroutinesApi()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\fJ\u0006\u0010%\u001a\u00020&J\u0006\u0010\'\u001a\u00020&J\u0006\u0010(\u001a\u00020&J\r\u0010)\u001a\u00020&H\u0001\u00a2\u0006\u0002\b*J\u0015\u0010+\u001a\u00020&2\u0006\u0010\u0004\u001a\u00020,H\u0001\u00a2\u0006\u0002\b-J\u0010\u0010.\u001a\u00020&2\b\b\u0002\u0010/\u001a\u00020\u000eJ\u0014\u00100\u001a\u00020&*\u0002012\u0006\u0010\u001e\u001a\u00020\u0014H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u00020\u000e8@X\u0081\u0004\u00a2\u0006\f\u0012\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0013\u001a\n \u0015*\u0004\u0018\u00010\u00140\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0018\u001a\u00020\u0019\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0010\u0010\u001c\u001a\u00020\u001d8\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u001e\u001a\u00020\u001f8\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\b \u0010\u0010\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$\u00a8\u00062"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarView;", "", "container", "Landroid/view/ViewGroup;", "toolbarPosition", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarPosition;", "interactor", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarViewInteractor;", "customTabSession", "Lmozilla/components/browser/state/state/CustomTabSessionState;", "lifecycleOwner", "Landroidx/lifecycle/LifecycleOwner;", "(Landroid/view/ViewGroup;Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarPosition;Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarViewInteractor;Lmozilla/components/browser/state/state/CustomTabSessionState;Landroidx/lifecycle/LifecycleOwner;)V", "isPwaTabOrTwaTab", "", "isPwaTabOrTwaTab$app_debug$annotations", "()V", "isPwaTabOrTwaTab$app_debug", "()Z", "layout", "Landroid/view/View;", "kotlin.jvm.PlatformType", "settings", "Lcom/cookiejarapps/android/smartcookieweb/preferences/UserPreferences;", "toolbarIntegration", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarIntegration;", "getToolbarIntegration", "()Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarIntegration;", "toolbarLayout", "", "view", "Lmozilla/components/browser/toolbar/BrowserToolbar;", "getView$app_debug$annotations", "getView$app_debug", "()Lmozilla/components/browser/toolbar/BrowserToolbar;", "setView$app_debug", "(Lmozilla/components/browser/toolbar/BrowserToolbar;)V", "collapse", "", "dismissMenu", "expand", "expandToolbarAndMakeItFixed", "expandToolbarAndMakeItFixed$app_debug", "setDynamicToolbarBehavior", "Lmozilla/components/browser/toolbar/behavior/ToolbarPosition;", "setDynamicToolbarBehavior$app_debug", "setToolbarBehavior", "shouldDisableScroll", "performHapticIfNeeded", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "app_debug"})
public final class BrowserToolbarView {
    private final android.view.ViewGroup container = null;
    private final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarPosition toolbarPosition = null;
    private final com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarViewInteractor interactor = null;
    private final mozilla.components.browser.state.state.CustomTabSessionState customTabSession = null;
    private final androidx.lifecycle.LifecycleOwner lifecycleOwner = null;
    private final com.cookiejarapps.android.smartcookieweb.preferences.UserPreferences settings = null;
    @androidx.annotation.LayoutRes()
    private final int toolbarLayout = 0;
    private final android.view.View layout = null;
    @org.jetbrains.annotations.NotNull()
    private mozilla.components.browser.toolbar.BrowserToolbar view;
    @org.jetbrains.annotations.NotNull()
    private final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarIntegration toolbarIntegration = null;
    
    public BrowserToolbarView(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup container, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarPosition toolbarPosition, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarViewInteractor interactor, @org.jetbrains.annotations.Nullable()
    mozilla.components.browser.state.state.CustomTabSessionState customTabSession, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.browser.toolbar.BrowserToolbar getView$app_debug() {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getView$app_debug$annotations() {
    }
    
    public final void setView$app_debug(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.toolbar.BrowserToolbar p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarIntegration getToolbarIntegration() {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void isPwaTabOrTwaTab$app_debug$annotations() {
    }
    
    public final boolean isPwaTabOrTwaTab$app_debug() {
        return false;
    }
    
    public final void expand() {
    }
    
    public final void collapse() {
    }
    
    public final void dismissMenu() {
    }
    
    /**
     * Sets whether the toolbar will have a dynamic behavior (to be scrolled) or not.
     *
     * This will intrinsically check and disable the dynamic behavior if
     * - this is disabled in app settings
     * - toolbar is placed at the bottom and tab shows a PWA or TWA
     *
     * Also if the user has not explicitly set a toolbar position and has a screen reader enabled
     * the toolbar will be placed at the top and in a fixed position.
     *
     * @param shouldDisableScroll force disable of the dynamic behavior irrespective of the intrinsic checks.
     */
    public final void setToolbarBehavior(boolean shouldDisableScroll) {
    }
    
    @androidx.annotation.VisibleForTesting()
    public final void expandToolbarAndMakeItFixed$app_debug() {
    }
    
    @androidx.annotation.VisibleForTesting()
    public final void setDynamicToolbarBehavior$app_debug(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.toolbar.behavior.ToolbarPosition toolbarPosition) {
    }
    
    @kotlin.Suppress(names = {"ComplexCondition"})
    private final void performHapticIfNeeded(com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item $this$performHapticIfNeeded, android.view.View view) {
    }
}