package com.cookiejarapps.android.smartcookieweb.search;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/SearchDialogFragmentDirections;", "", "()V", "Companion", "app_debug"})
public final class SearchDialogFragmentDirections {
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.search.SearchDialogFragmentDirections.Companion Companion = null;
    
    private SearchDialogFragmentDirections() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u001a\u0010\u0007\u001a\u00020\u00042\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000bJ\u001c\u0010\f\u001a\u00020\u00042\b\u0010\r\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0006\u00a8\u0006\u000f"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/SearchDialogFragmentDirections$Companion;", "", "()V", "actionGlobalBrowser", "Landroidx/navigation/NavDirections;", "activeSessionId", "", "actionGlobalHome", "focusOnAddressBar", "", "focusOnCollection", "", "actionGlobalSearchDialog", "sessionId", "pastedText", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.navigation.NavDirections actionGlobalHome(boolean focusOnAddressBar, long focusOnCollection) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.navigation.NavDirections actionGlobalBrowser(@org.jetbrains.annotations.Nullable()
        java.lang.String activeSessionId) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.navigation.NavDirections actionGlobalSearchDialog(@org.jetbrains.annotations.Nullable()
        java.lang.String sessionId, @org.jetbrains.annotations.Nullable()
        java.lang.String pastedText) {
            return null;
        }
    }
}