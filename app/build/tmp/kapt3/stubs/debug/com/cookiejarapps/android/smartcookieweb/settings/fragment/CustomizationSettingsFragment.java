package com.cookiejarapps.android.smartcookieweb.settings.fragment;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016J\b\u0010\t\u001a\u00020\u0004H\u0002J\b\u0010\n\u001a\u00020\u0004H\u0002\u00a8\u0006\u000b"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/settings/fragment/CustomizationSettingsFragment;", "Lcom/cookiejarapps/android/smartcookieweb/settings/fragment/BaseSettingsFragment;", "()V", "onCreatePreferences", "", "savedInstanceState", "Landroid/os/Bundle;", "s", "", "pickAppTheme", "pickWebTheme", "app_debug"})
public final class CustomizationSettingsFragment extends com.cookiejarapps.android.smartcookieweb.settings.fragment.BaseSettingsFragment {
    
    public CustomizationSettingsFragment() {
        super();
    }
    
    @java.lang.Override()
    public void onCreatePreferences(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState, @org.jetbrains.annotations.Nullable()
    java.lang.String s) {
    }
    
    private final void pickAppTheme() {
    }
    
    private final void pickWebTheme() {
    }
}