package com.cookiejarapps.android.smartcookieweb.components.toolbar;

import java.lang.System;

@kotlinx.coroutines.ExperimentalCoroutinesApi()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0007\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\bJ\u0006\u0010\u000b\u001a\u00020\fJ\u0012\u0010\r\u001a\u00020\f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016J\u0012\u0010\u0010\u001a\u00020\f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016J\u0006\u0010\u0011\u001a\u00020\fJ\u0006\u0010\u0012\u001a\u00020\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/MenuPresenter;", "Landroid/view/View$OnAttachStateChangeListener;", "menuToolbar", "Lmozilla/components/browser/toolbar/BrowserToolbar;", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "sessionId", "", "(Lmozilla/components/browser/toolbar/BrowserToolbar;Lmozilla/components/browser/state/store/BrowserStore;Ljava/lang/String;)V", "scope", "Lkotlinx/coroutines/CoroutineScope;", "invalidateActions", "", "onViewAttachedToWindow", "v", "Landroid/view/View;", "onViewDetachedFromWindow", "start", "stop", "app_debug"})
public final class MenuPresenter implements android.view.View.OnAttachStateChangeListener {
    private final mozilla.components.browser.toolbar.BrowserToolbar menuToolbar = null;
    private final mozilla.components.browser.state.store.BrowserStore store = null;
    private final java.lang.String sessionId = null;
    private kotlinx.coroutines.CoroutineScope scope;
    
    public MenuPresenter(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.toolbar.BrowserToolbar menuToolbar, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore store, @org.jetbrains.annotations.Nullable()
    java.lang.String sessionId) {
        super();
    }
    
    public final void start() {
    }
    
    public final void stop() {
    }
    
    public final void invalidateActions() {
    }
    
    @java.lang.Override()
    public void onViewDetachedFromWindow(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    @java.lang.Override()
    public void onViewAttachedToWindow(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
}