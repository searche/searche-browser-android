package com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0016\u0018\u0000 \u001f2\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0004\u001d\u001e\u001f B#\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000eH\u0016J \u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u000eH\u0016J\"\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u000eH\u0014J \u0010\u001a\u001a\u00020\u00112\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0002H\u0004R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0084\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f\u00a8\u0006!"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkAdapter;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/ArrayRecyclerAdapter;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkItem;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkAdapter$BookmarkItemHolder;", "context", "Landroid/content/Context;", "list", "", "bookmarkItemListener", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkAdapter$OnBookmarkRecyclerListener;", "(Landroid/content/Context;Ljava/util/List;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkAdapter$OnBookmarkRecyclerListener;)V", "getContext", "()Landroid/content/Context;", "getItemViewType", "", "position", "onBindViewHolder", "", "holder", "item", "onCreateViewHolder", "inflater", "Landroid/view/LayoutInflater;", "parent", "Landroid/view/ViewGroup;", "viewType", "onOverflowButtonClick", "v", "Landroid/view/View;", "BookmarkItemHolder", "BookmarkSiteHolder", "Companion", "OnBookmarkRecyclerListener", "app_debug"})
public class BookmarkAdapter extends com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.ArrayRecyclerAdapter<com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem, com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkAdapter.BookmarkItemHolder> {
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    private final com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkAdapter.OnBookmarkRecyclerListener bookmarkItemListener = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkAdapter.Companion Companion = null;
    public static final int TYPE_SITE = 1;
    public static final int TYPE_FOLDER = 2;
    
    public BookmarkAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem> list, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkAdapter.OnBookmarkRecyclerListener bookmarkItemListener) {
        super(null, null, null);
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final android.content.Context getContext() {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkAdapter.BookmarkItemHolder holder, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem item, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    protected com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkAdapter.BookmarkItemHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    protected final void onOverflowButtonClick(@org.jetbrains.annotations.NotNull()
    android.view.View v, int position, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem item) {
    }
    
    @java.lang.Override()
    public int getItemViewType(int position) {
        return 0;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u000b"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkAdapter$BookmarkSiteHolder;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkAdapter$BookmarkItemHolder;", "itemView", "Landroid/view/View;", "adapter", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkAdapter;", "(Landroid/view/View;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkAdapter;)V", "url", "Landroid/widget/TextView;", "getUrl", "()Landroid/widget/TextView;", "app_debug"})
    public static final class BookmarkSiteHolder extends com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkAdapter.BookmarkItemHolder {
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView url = null;
        
        public BookmarkSiteHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView, @org.jetbrains.annotations.NotNull()
        com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkAdapter adapter) {
            super(null, null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getUrl() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0016\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0002H\u0016R\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\f\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000bR\u0011\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011\u00a8\u0006\u0015"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkAdapter$BookmarkItemHolder;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/ArrayRecyclerAdapter$ArrayViewHolder;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkItem;", "itemView", "Landroid/view/View;", "adapter", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkAdapter;", "(Landroid/view/View;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkAdapter;)V", "icon", "Landroid/widget/ImageButton;", "getIcon", "()Landroid/widget/ImageButton;", "more", "getMore", "title", "Landroid/widget/TextView;", "getTitle", "()Landroid/widget/TextView;", "setUp", "", "item", "app_debug"})
    public static class BookmarkItemHolder extends com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.ArrayRecyclerAdapter.ArrayViewHolder<com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem> {
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView title = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.ImageButton icon = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.ImageButton more = null;
        
        public BookmarkItemHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView, @org.jetbrains.annotations.NotNull()
        com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkAdapter adapter) {
            super(null, null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getTitle() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageButton getIcon() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageButton getMore() {
            return null;
        }
        
        @java.lang.Override()
        public void setUp(@org.jetbrains.annotations.NotNull()
        com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem item) {
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\u0007H&J\u0018\u0010\n\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\u000b"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkAdapter$OnBookmarkRecyclerListener;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkRecyclerViewClickInterface;", "onIconClick", "", "v", "Landroid/view/View;", "position", "", "onSelectionStateChange", "items", "onShowMenu", "app_debug"})
    public static abstract interface OnBookmarkRecyclerListener extends com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.BookmarkRecyclerViewClickInterface {
        
        public abstract void onIconClick(@org.jetbrains.annotations.NotNull()
        android.view.View v, int position);
        
        public abstract void onShowMenu(@org.jetbrains.annotations.NotNull()
        android.view.View v, int position);
        
        public abstract void onSelectionStateChange(int items);
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkAdapter$Companion;", "", "()V", "TYPE_FOLDER", "", "TYPE_SITE", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}