package com.cookiejarapps.android.smartcookieweb.ext;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004\"\u0015\u0010\u0005\u001a\u00020\u0006*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\t"}, d2 = {"application", "Lcom/cookiejarapps/android/smartcookieweb/BrowserApp;", "Landroid/content/Context;", "getApplication", "(Landroid/content/Context;)Lcom/cookiejarapps/android/smartcookieweb/BrowserApp;", "components", "Lcom/cookiejarapps/android/smartcookieweb/components/Components;", "getComponents", "(Landroid/content/Context;)Lcom/cookiejarapps/android/smartcookieweb/components/Components;", "app_debug"})
public final class ContextKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.BrowserApp getApplication(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$application) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.components.Components getComponents(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$components) {
        return null;
    }
}