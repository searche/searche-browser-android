package com.cookiejarapps.android.smartcookieweb.preferences;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\t\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u001a\n\u0002\u0010\u0007\n\u0002\b\u001b\n\u0002\u0018\u0002\n\u0002\bC\n\u0002\u0018\u0002\n\u0002\b\u0010\u0018\u0000 \u00b2\u00012\u00020\u0001:\u0002\u00b2\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R+\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\b\f\u0010\r\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR+\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\b\u0011\u0010\r\u001a\u0004\b\u000f\u0010\t\"\u0004\b\u0010\u0010\u000bR+\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\b\u0018\u0010\r\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R+\u0010\u0019\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\b\u001c\u0010\r\u001a\u0004\b\u001a\u0010\u0015\"\u0004\b\u001b\u0010\u0017R+\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u0005\u001a\u00020\u001d8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\b#\u0010\r\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R+\u0010$\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\b\'\u0010\r\u001a\u0004\b%\u0010\u0015\"\u0004\b&\u0010\u0017R+\u0010)\u001a\u00020(2\u0006\u0010\u0005\u001a\u00020(8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\b.\u0010\r\u001a\u0004\b*\u0010+\"\u0004\b,\u0010-R+\u0010/\u001a\u00020(2\u0006\u0010\u0005\u001a\u00020(8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\b2\u0010\r\u001a\u0004\b0\u0010+\"\u0004\b1\u0010-R+\u00103\u001a\u00020(2\u0006\u0010\u0005\u001a\u00020(8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\b6\u0010\r\u001a\u0004\b4\u0010+\"\u0004\b5\u0010-R+\u00107\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\b:\u0010\r\u001a\u0004\b8\u0010\u0015\"\u0004\b9\u0010\u0017R+\u0010;\u001a\u00020(2\u0006\u0010\u0005\u001a\u00020(8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\b>\u0010\r\u001a\u0004\b<\u0010+\"\u0004\b=\u0010-R+\u0010?\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\bB\u0010\r\u001a\u0004\b@\u0010\u0015\"\u0004\bA\u0010\u0017R+\u0010D\u001a\u00020C2\u0006\u0010\u0005\u001a\u00020C8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\bI\u0010\r\u001a\u0004\bE\u0010F\"\u0004\bG\u0010HR+\u0010J\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\bM\u0010\r\u001a\u0004\bK\u0010\u0015\"\u0004\bL\u0010\u0017R+\u0010N\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\bQ\u0010\r\u001a\u0004\bO\u0010\t\"\u0004\bP\u0010\u000bR+\u0010R\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\bU\u0010\r\u001a\u0004\bS\u0010\u0015\"\u0004\bT\u0010\u0017R+\u0010V\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\bY\u0010\r\u001a\u0004\bW\u0010\u0015\"\u0004\bX\u0010\u0017R+\u0010Z\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\b]\u0010\r\u001a\u0004\b[\u0010\u0015\"\u0004\b\\\u0010\u0017R\u0014\u0010^\u001a\u00020_X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b`\u0010aR+\u0010b\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\be\u0010\r\u001a\u0004\bc\u0010\u0015\"\u0004\bd\u0010\u0017R+\u0010f\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\bi\u0010\r\u001a\u0004\bg\u0010\u0015\"\u0004\bh\u0010\u0017R+\u0010j\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\bm\u0010\r\u001a\u0004\bk\u0010\u0015\"\u0004\bl\u0010\u0017R+\u0010n\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\bq\u0010\r\u001a\u0004\bo\u0010\t\"\u0004\bp\u0010\u000bR+\u0010r\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\bu\u0010\r\u001a\u0004\bs\u0010\u0015\"\u0004\bt\u0010\u0017R+\u0010v\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\by\u0010\r\u001a\u0004\bw\u0010\u0015\"\u0004\bx\u0010\u0017R$\u0010{\u001a\u00020\u00122\u0006\u0010z\u001a\u00020\u00128F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b|\u0010\u0015\"\u0004\b}\u0010\u0017R-\u0010~\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0014\n\u0005\b\u0081\u0001\u0010\r\u001a\u0004\b\u007f\u0010\u0015\"\u0005\b\u0080\u0001\u0010\u0017R/\u0010\u0082\u0001\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0015\n\u0005\b\u0085\u0001\u0010\r\u001a\u0005\b\u0083\u0001\u0010\u0015\"\u0005\b\u0084\u0001\u0010\u0017R/\u0010\u0086\u0001\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0015\n\u0005\b\u0089\u0001\u0010\r\u001a\u0005\b\u0087\u0001\u0010\u0015\"\u0005\b\u0088\u0001\u0010\u0017R/\u0010\u008a\u0001\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0015\n\u0005\b\u008d\u0001\u0010\r\u001a\u0005\b\u008b\u0001\u0010\u0015\"\u0005\b\u008c\u0001\u0010\u0017R/\u0010\u008e\u0001\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0015\n\u0005\b\u0091\u0001\u0010\r\u001a\u0005\b\u008f\u0001\u0010\u0015\"\u0005\b\u0090\u0001\u0010\u0017R/\u0010\u0092\u0001\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0015\n\u0005\b\u0095\u0001\u0010\r\u001a\u0005\b\u0093\u0001\u0010\u0015\"\u0005\b\u0094\u0001\u0010\u0017R/\u0010\u0096\u0001\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0015\n\u0005\b\u0099\u0001\u0010\r\u001a\u0005\b\u0097\u0001\u0010\u0015\"\u0005\b\u0098\u0001\u0010\u0017R/\u0010\u009a\u0001\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0015\n\u0005\b\u009d\u0001\u0010\r\u001a\u0005\b\u009b\u0001\u0010\u0015\"\u0005\b\u009c\u0001\u0010\u0017R/\u0010\u009e\u0001\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0086\u008e\u0002\u00a2\u0006\u0015\n\u0005\b\u00a1\u0001\u0010\r\u001a\u0005\b\u009f\u0001\u0010\t\"\u0005\b\u00a0\u0001\u0010\u000bR\u0015\u0010\u00a2\u0001\u001a\u00030\u00a3\u00018F\u00a2\u0006\b\u001a\u0006\b\u00a4\u0001\u0010\u00a5\u0001R/\u0010\u00a6\u0001\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0015\n\u0005\b\u00a9\u0001\u0010\r\u001a\u0005\b\u00a7\u0001\u0010\u0015\"\u0005\b\u00a8\u0001\u0010\u0017R/\u0010\u00aa\u0001\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0015\n\u0005\b\u00ad\u0001\u0010\r\u001a\u0005\b\u00ab\u0001\u0010\u0015\"\u0005\b\u00ac\u0001\u0010\u0017R/\u0010\u00ae\u0001\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0086\u008e\u0002\u00a2\u0006\u0015\n\u0005\b\u00b1\u0001\u0010\r\u001a\u0005\b\u00af\u0001\u0010\t\"\u0005\b\u00b0\u0001\u0010\u000b\u00a8\u0006\u00b3\u0001"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/preferences/UserPreferences;", "Lmozilla/components/support/ktx/android/content/PreferencesHolder;", "appContext", "Landroid/content/Context;", "(Landroid/content/Context;)V", "<set-?>", "", "addonSort", "getAddonSort", "()I", "setAddonSort", "(I)V", "addonSort$delegate", "Lkotlin/properties/ReadWriteProperty;", "appThemeChoice", "getAppThemeChoice", "setAppThemeChoice", "appThemeChoice$delegate", "", "autoFontSize", "getAutoFontSize", "()Z", "setAutoFontSize", "(Z)V", "autoFontSize$delegate", "bookmarkFolder", "getBookmarkFolder", "setBookmarkFolder", "bookmarkFolder$delegate", "", "bookmarkFolderId", "getBookmarkFolderId", "()J", "setBookmarkFolderId", "(J)V", "bookmarkFolderId$delegate", "customAddonCollection", "getCustomAddonCollection", "setCustomAddonCollection", "customAddonCollection$delegate", "", "customAddonCollectionName", "getCustomAddonCollectionName", "()Ljava/lang/String;", "setCustomAddonCollectionName", "(Ljava/lang/String;)V", "customAddonCollectionName$delegate", "customAddonCollectionUser", "getCustomAddonCollectionUser", "setCustomAddonCollectionUser", "customAddonCollectionUser$delegate", "customHomepageUrl", "getCustomHomepageUrl", "setCustomHomepageUrl", "customHomepageUrl$delegate", "customSearchEngine", "getCustomSearchEngine", "setCustomSearchEngine", "customSearchEngine$delegate", "customSearchEngineURL", "getCustomSearchEngineURL", "setCustomSearchEngineURL", "customSearchEngineURL$delegate", "firstLaunch", "getFirstLaunch", "setFirstLaunch", "firstLaunch$delegate", "", "fontSizeFactor", "getFontSizeFactor", "()F", "setFontSizeFactor", "(F)V", "fontSizeFactor$delegate", "hideBarWhileScrolling", "getHideBarWhileScrolling", "setHideBarWhileScrolling", "hideBarWhileScrolling$delegate", "homepageType", "getHomepageType", "setHomepageType", "homepageType$delegate", "javaScriptEnabled", "getJavaScriptEnabled", "setJavaScriptEnabled", "javaScriptEnabled$delegate", "lastKnownPrivate", "getLastKnownPrivate", "setLastKnownPrivate", "lastKnownPrivate$delegate", "launchInApp", "getLaunchInApp", "setLaunchInApp", "launchInApp$delegate", "preferences", "Landroid/content/SharedPreferences;", "getPreferences", "()Landroid/content/SharedPreferences;", "promptExternalDownloader", "getPromptExternalDownloader", "setPromptExternalDownloader", "promptExternalDownloader$delegate", "remoteDebugging", "getRemoteDebugging", "setRemoteDebugging", "remoteDebugging$delegate", "safeBrowsing", "getSafeBrowsing", "setSafeBrowsing", "safeBrowsing$delegate", "searchEngineChoice", "getSearchEngineChoice", "setSearchEngineChoice", "searchEngineChoice$delegate", "searchSuggestionsEnabled", "getSearchSuggestionsEnabled", "setSearchSuggestionsEnabled", "searchSuggestionsEnabled$delegate", "shortcutDrawerOpen", "getShortcutDrawerOpen", "setShortcutDrawerOpen", "shortcutDrawerOpen$delegate", "value", "shouldUseBottomToolbar", "getShouldUseBottomToolbar", "setShouldUseBottomToolbar", "showAddonsInBar", "getShowAddonsInBar", "setShowAddonsInBar", "showAddonsInBar$delegate", "showShortcuts", "getShowShortcuts", "setShowShortcuts", "showShortcuts$delegate", "showTabsInGrid", "getShowTabsInGrid", "setShowTabsInGrid", "showTabsInGrid$delegate", "showUrlProtocol", "getShowUrlProtocol", "setShowUrlProtocol", "showUrlProtocol$delegate", "shownCollectionDisclaimer", "getShownCollectionDisclaimer", "setShownCollectionDisclaimer", "shownCollectionDisclaimer$delegate", "stackFromBottom", "getStackFromBottom", "setStackFromBottom", "stackFromBottom$delegate", "swapDrawers", "getSwapDrawers", "setSwapDrawers", "swapDrawers$delegate", "swipeToRefresh", "getSwipeToRefresh", "setSwipeToRefresh", "swipeToRefresh$delegate", "toolbarPosition", "getToolbarPosition", "setToolbarPosition", "toolbarPosition$delegate", "toolbarPositionType", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarPosition;", "getToolbarPositionType", "()Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarPosition;", "trackingProtection", "getTrackingProtection", "setTrackingProtection", "trackingProtection$delegate", "trustThirdPartyCerts", "getTrustThirdPartyCerts", "setTrustThirdPartyCerts", "trustThirdPartyCerts$delegate", "webThemeChoice", "getWebThemeChoice", "setWebThemeChoice", "webThemeChoice$delegate", "Companion", "app_debug"})
public final class UserPreferences implements mozilla.components.support.ktx.android.content.PreferencesHolder {
    @org.jetbrains.annotations.NotNull()
    private final android.content.SharedPreferences preferences = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty bookmarkFolder$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty bookmarkFolderId$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty shortcutDrawerOpen$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty lastKnownPrivate$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty firstLaunch$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty javaScriptEnabled$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty showAddonsInBar$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty searchEngineChoice$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty customSearchEngine$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty customSearchEngineURL$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty toolbarPosition$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty homepageType$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty customHomepageUrl$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty appThemeChoice$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty webThemeChoice$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty launchInApp$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty customAddonCollection$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty shownCollectionDisclaimer$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty customAddonCollectionUser$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty customAddonCollectionName$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty autoFontSize$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty fontSizeFactor$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty hideBarWhileScrolling$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty swapDrawers$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty stackFromBottom$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty showTabsInGrid$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty swipeToRefresh$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty remoteDebugging$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty promptExternalDownloader$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty addonSort$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty showUrlProtocol$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty searchSuggestionsEnabled$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty safeBrowsing$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty trackingProtection$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty showShortcuts$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.properties.ReadWriteProperty trustThirdPartyCerts$delegate = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.preferences.UserPreferences.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SCW_PREFERENCES = "scw_preferences";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String JAVA_SCRIPT_ENABLED = "java_script_enabled";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SHOW_ADDONS_IN_BAR = "show_addons_in_bar";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SEARCH_ENGINE = "search_engine";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CUSTOM_SEARCH_ENGINE = "custom_search_engine";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CUSTOM_SEARCH_ENGINE_URL = "custom_search_engine_url";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TOOLBAR_POSITION = "toolbar_position";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String HOMEPAGE_TYPE = "homepage_type";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String HOMEPAGE_URL = "homepage_url";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String APP_THEME_CHOICE = "app_theme_choice";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WEB_THEME_CHOICE = "web_theme_choice";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LAUNCH_IN_APP = "launch_in_app";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CUSTOM_ADDON_BOOL = "custom_addon_bool";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SHOWN_ADDON_DISCLAIMER = "shown_disclaimer";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String COLLECTION_NAME = "collection_name";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String COLLECTION_USER = "collection_user";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String AUTO_FONT_SIZE = "auto_font_size";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String FONT_SIZE_FACTOR = "font_size_factor";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String HIDE_URL_BAR = "hide_url_bar";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SWAP_DRAWERS = "swap_drawers";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String STACK_FROM_BOTTOM = "stack_from_bottom";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SHOW_TABS_IN_GRID = "show_tabs_in_grid";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SWIPE_TO_REFRESH = "swipe_to_refresh";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String REMOTE_DEBUGGING = "remote_debugging";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PROMPT_EXTERNAL_DOWNLOADER = "prompt_external_downloader";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SHOW_URL_PROTOCOL = "show_url_protocol";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SEARCH_SUGGESTIONS = "search_suggestions";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SAFE_BROWSING = "safe_browsing";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TRACKING_PROTECTION = "tracking_protection";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SHOW_SHORTCUTS = "show_shortcuts";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TRUST_THIRD_PARTY_CERTS = "trust_third_party_certs";
    
    public UserPreferences(@org.jetbrains.annotations.NotNull()
    android.content.Context appContext) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.content.SharedPreferences getPreferences() {
        return null;
    }
    
    public final boolean getBookmarkFolder() {
        return false;
    }
    
    public final void setBookmarkFolder(boolean p0) {
    }
    
    public final long getBookmarkFolderId() {
        return 0L;
    }
    
    public final void setBookmarkFolderId(long p0) {
    }
    
    public final boolean getShortcutDrawerOpen() {
        return false;
    }
    
    public final void setShortcutDrawerOpen(boolean p0) {
    }
    
    public final boolean getLastKnownPrivate() {
        return false;
    }
    
    public final void setLastKnownPrivate(boolean p0) {
    }
    
    public final boolean getFirstLaunch() {
        return false;
    }
    
    public final void setFirstLaunch(boolean p0) {
    }
    
    public final boolean getJavaScriptEnabled() {
        return false;
    }
    
    public final void setJavaScriptEnabled(boolean p0) {
    }
    
    public final boolean getShowAddonsInBar() {
        return false;
    }
    
    public final void setShowAddonsInBar(boolean p0) {
    }
    
    public final int getSearchEngineChoice() {
        return 0;
    }
    
    public final void setSearchEngineChoice(int p0) {
    }
    
    public final boolean getCustomSearchEngine() {
        return false;
    }
    
    public final void setCustomSearchEngine(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCustomSearchEngineURL() {
        return null;
    }
    
    public final void setCustomSearchEngineURL(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public final int getToolbarPosition() {
        return 0;
    }
    
    public final void setToolbarPosition(int p0) {
    }
    
    public final int getHomepageType() {
        return 0;
    }
    
    public final void setHomepageType(int p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCustomHomepageUrl() {
        return null;
    }
    
    public final void setCustomHomepageUrl(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public final int getAppThemeChoice() {
        return 0;
    }
    
    public final void setAppThemeChoice(int p0) {
    }
    
    public final int getWebThemeChoice() {
        return 0;
    }
    
    public final void setWebThemeChoice(int p0) {
    }
    
    public final boolean getLaunchInApp() {
        return false;
    }
    
    public final void setLaunchInApp(boolean p0) {
    }
    
    public final boolean getCustomAddonCollection() {
        return false;
    }
    
    public final void setCustomAddonCollection(boolean p0) {
    }
    
    public final boolean getShownCollectionDisclaimer() {
        return false;
    }
    
    public final void setShownCollectionDisclaimer(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCustomAddonCollectionUser() {
        return null;
    }
    
    public final void setCustomAddonCollectionUser(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCustomAddonCollectionName() {
        return null;
    }
    
    public final void setCustomAddonCollectionName(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public final boolean getAutoFontSize() {
        return false;
    }
    
    public final void setAutoFontSize(boolean p0) {
    }
    
    public final float getFontSizeFactor() {
        return 0.0F;
    }
    
    public final void setFontSizeFactor(float p0) {
    }
    
    public final boolean getHideBarWhileScrolling() {
        return false;
    }
    
    public final void setHideBarWhileScrolling(boolean p0) {
    }
    
    public final boolean getSwapDrawers() {
        return false;
    }
    
    public final void setSwapDrawers(boolean p0) {
    }
    
    public final boolean getStackFromBottom() {
        return false;
    }
    
    public final void setStackFromBottom(boolean p0) {
    }
    
    public final boolean getShowTabsInGrid() {
        return false;
    }
    
    public final void setShowTabsInGrid(boolean p0) {
    }
    
    public final boolean getSwipeToRefresh() {
        return false;
    }
    
    public final void setSwipeToRefresh(boolean p0) {
    }
    
    public final boolean getRemoteDebugging() {
        return false;
    }
    
    public final void setRemoteDebugging(boolean p0) {
    }
    
    public final boolean getPromptExternalDownloader() {
        return false;
    }
    
    public final void setPromptExternalDownloader(boolean p0) {
    }
    
    public final int getAddonSort() {
        return 0;
    }
    
    public final void setAddonSort(int p0) {
    }
    
    public final boolean getShowUrlProtocol() {
        return false;
    }
    
    public final void setShowUrlProtocol(boolean p0) {
    }
    
    public final boolean getSearchSuggestionsEnabled() {
        return false;
    }
    
    public final void setSearchSuggestionsEnabled(boolean p0) {
    }
    
    public final boolean getSafeBrowsing() {
        return false;
    }
    
    public final void setSafeBrowsing(boolean p0) {
    }
    
    public final boolean getTrackingProtection() {
        return false;
    }
    
    public final void setTrackingProtection(boolean p0) {
    }
    
    public final boolean getShowShortcuts() {
        return false;
    }
    
    public final void setShowShortcuts(boolean p0) {
    }
    
    public final boolean getTrustThirdPartyCerts() {
        return false;
    }
    
    public final void setTrustThirdPartyCerts(boolean p0) {
    }
    
    public final boolean getShouldUseBottomToolbar() {
        return false;
    }
    
    public final void setShouldUseBottomToolbar(boolean value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarPosition getToolbarPositionType() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u001f\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/preferences/UserPreferences$Companion;", "", "()V", "APP_THEME_CHOICE", "", "AUTO_FONT_SIZE", "COLLECTION_NAME", "COLLECTION_USER", "CUSTOM_ADDON_BOOL", "CUSTOM_SEARCH_ENGINE", "CUSTOM_SEARCH_ENGINE_URL", "FONT_SIZE_FACTOR", "HIDE_URL_BAR", "HOMEPAGE_TYPE", "HOMEPAGE_URL", "JAVA_SCRIPT_ENABLED", "LAUNCH_IN_APP", "PROMPT_EXTERNAL_DOWNLOADER", "REMOTE_DEBUGGING", "SAFE_BROWSING", "SCW_PREFERENCES", "SEARCH_ENGINE", "SEARCH_SUGGESTIONS", "SHOWN_ADDON_DISCLAIMER", "SHOW_ADDONS_IN_BAR", "SHOW_SHORTCUTS", "SHOW_TABS_IN_GRID", "SHOW_URL_PROTOCOL", "STACK_FROM_BOTTOM", "SWAP_DRAWERS", "SWIPE_TO_REFRESH", "TOOLBAR_POSITION", "TRACKING_PROTECTION", "TRUST_THIRD_PARTY_CERTS", "WEB_THEME_CHOICE", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}