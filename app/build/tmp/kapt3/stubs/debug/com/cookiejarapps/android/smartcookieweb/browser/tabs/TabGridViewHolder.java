package com.cookiejarapps.android.smartcookieweb.browser.tabs;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 72\u00020\u0001:\u00017B\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J(\u0010+\u001a\u00020,2\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010-\u001a\u00020.2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010/\u001a\u000200H\u0016J\r\u00101\u001a\u00020,H\u0001\u00a2\u0006\u0002\b2J\r\u00103\u001a\u00020,H\u0001\u00a2\u0006\u0002\b4J\u0010\u00105\u001a\u00020,2\u0006\u00106\u001a\u00020.H\u0016R\u001c\u0010\u0007\u001a\u00020\b8\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u001e\u0010\r\u001a\u0004\u0018\u00010\u000e8\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\b\u000f\u0010\n\u001a\u0004\b\u0010\u0010\u0011R&\u0010\u0012\u001a\u0004\u0018\u00010\u00138\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\b\u0014\u0010\n\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u001c\u0010\u0019\u001a\u0004\u0018\u00010\u001aX\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u001c\u0010\u001f\u001a\u00020 8\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\b!\u0010\n\u001a\u0004\b\"\u0010#R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010&\u001a\u00020\'8\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\b(\u0010\n\u001a\u0004\b)\u0010*\u00a8\u00068"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/tabs/TabGridViewHolder;", "Lmozilla/components/browser/tabstray/TabViewHolder;", "itemView", "Landroid/view/View;", "thumbnailLoader", "Lmozilla/components/concept/base/images/ImageLoader;", "(Landroid/view/View;Lmozilla/components/concept/base/images/ImageLoader;)V", "closeView", "Landroidx/appcompat/widget/AppCompatImageButton;", "getCloseView$app_debug$annotations", "()V", "getCloseView$app_debug", "()Landroidx/appcompat/widget/AppCompatImageButton;", "iconView", "Landroid/widget/ImageView;", "getIconView$app_debug$annotations", "getIconView$app_debug", "()Landroid/widget/ImageView;", "styling", "Lmozilla/components/browser/tabstray/TabsTrayStyling;", "getStyling$app_debug$annotations", "getStyling$app_debug", "()Lmozilla/components/browser/tabstray/TabsTrayStyling;", "setStyling$app_debug", "(Lmozilla/components/browser/tabstray/TabsTrayStyling;)V", "tab", "Lmozilla/components/browser/state/state/TabSessionState;", "getTab", "()Lmozilla/components/browser/state/state/TabSessionState;", "setTab", "(Lmozilla/components/browser/state/state/TabSessionState;)V", "tabBackground", "Landroidx/constraintlayout/widget/ConstraintLayout;", "getTabBackground$app_debug$annotations", "getTabBackground$app_debug", "()Landroidx/constraintlayout/widget/ConstraintLayout;", "thumbnailView", "Lmozilla/components/browser/tabstray/thumbnail/TabThumbnailView;", "titleView", "Landroid/widget/TextView;", "getTitleView$app_debug$annotations", "getTitleView$app_debug", "()Landroid/widget/TextView;", "bind", "", "isSelected", "", "delegate", "Lmozilla/components/browser/tabstray/TabsTray$Delegate;", "showItemAsNotSelected", "showItemAsNotSelected$app_debug", "showItemAsSelected", "showItemAsSelected$app_debug", "updateSelectedTabIndicator", "showAsSelected", "Companion", "app_debug"})
public final class TabGridViewHolder extends mozilla.components.browser.tabstray.TabViewHolder {
    private final mozilla.components.concept.base.images.ImageLoader thumbnailLoader = null;
    @org.jetbrains.annotations.Nullable()
    private final android.widget.ImageView iconView = null;
    @org.jetbrains.annotations.NotNull()
    private final android.widget.TextView titleView = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.constraintlayout.widget.ConstraintLayout tabBackground = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.appcompat.widget.AppCompatImageButton closeView = null;
    private final mozilla.components.browser.tabstray.thumbnail.TabThumbnailView thumbnailView = null;
    @org.jetbrains.annotations.Nullable()
    private mozilla.components.browser.state.state.TabSessionState tab;
    @org.jetbrains.annotations.Nullable()
    private mozilla.components.browser.tabstray.TabsTrayStyling styling;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.browser.tabs.TabGridViewHolder.Companion Companion = null;
    @androidx.annotation.Dimension(unit = androidx.annotation.Dimension.DP)
    private static final int THUMBNAIL_SIZE = 100;
    
    public TabGridViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.View itemView, @org.jetbrains.annotations.Nullable()
    mozilla.components.concept.base.images.ImageLoader thumbnailLoader) {
        super(null);
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.ImageView getIconView$app_debug() {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getIconView$app_debug$annotations() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getTitleView$app_debug() {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getTitleView$app_debug$annotations() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.constraintlayout.widget.ConstraintLayout getTabBackground$app_debug() {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getTabBackground$app_debug$annotations() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.appcompat.widget.AppCompatImageButton getCloseView$app_debug() {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getCloseView$app_debug$annotations() {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public mozilla.components.browser.state.state.TabSessionState getTab() {
        return null;
    }
    
    @java.lang.Override()
    public void setTab(@org.jetbrains.annotations.Nullable()
    mozilla.components.browser.state.state.TabSessionState p0) {
    }
    
    @java.lang.Override()
    public void bind(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.state.TabSessionState tab, boolean isSelected, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.tabstray.TabsTrayStyling styling, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.tabstray.TabsTray.Delegate delegate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final mozilla.components.browser.tabstray.TabsTrayStyling getStyling$app_debug() {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getStyling$app_debug$annotations() {
    }
    
    public final void setStyling$app_debug(@org.jetbrains.annotations.Nullable()
    mozilla.components.browser.tabstray.TabsTrayStyling p0) {
    }
    
    @java.lang.Override()
    public void updateSelectedTabIndicator(boolean showAsSelected) {
    }
    
    @androidx.annotation.VisibleForTesting()
    public final void showItemAsSelected$app_debug() {
    }
    
    @androidx.annotation.VisibleForTesting()
    public final void showItemAsNotSelected$app_debug() {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0002X\u0083T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/tabs/TabGridViewHolder$Companion;", "", "()V", "THUMBNAIL_SIZE", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}