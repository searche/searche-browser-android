package com.cookiejarapps.android.smartcookieweb.media;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001b\u0010\u0003\u001a\u00020\u00048TX\u0094\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\t"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/media/MediaSessionService;", "Lmozilla/components/feature/media/service/AbstractMediaSessionService;", "()V", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "getStore", "()Lmozilla/components/browser/state/store/BrowserStore;", "store$delegate", "Lkotlin/Lazy;", "app_debug"})
public final class MediaSessionService extends mozilla.components.feature.media.service.AbstractMediaSessionService {
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy store$delegate = null;
    
    public MediaSessionService() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    protected mozilla.components.browser.state.store.BrowserStore getStore() {
        return null;
    }
}