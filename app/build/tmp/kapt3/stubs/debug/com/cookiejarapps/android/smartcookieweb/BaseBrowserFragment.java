package com.cookiejarapps.android.smartcookieweb;

import java.lang.System;

/**
 * Base fragment extended by [BrowserFragment].
 * This class only contains shared code focused on the main browsing content.
 * UI code specific to the app or to custom tabs can be found in the subclasses.
 */
@kotlin.Suppress(names = {"TooManyFunctions", "LargeClass"})
@kotlinx.coroutines.ExperimentalCoroutinesApi()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u00cc\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0015\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\b\'\u0018\u0000 \u00b6\u00012\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0002\u00b6\u0001B\u0005\u00a2\u0006\u0002\u0010\u0005J\u0015\u0010a\u001a\u00020b2\u0006\u0010c\u001a\u00020dH\u0001\u00a2\u0006\u0002\beJ\u0015\u0010f\u001a\u00020b2\u0006\u0010g\u001a\u00020\u001cH\u0001\u00a2\u0006\u0002\bhJ\b\u0010i\u001a\u00020jH\u0004J\u000f\u0010k\u001a\u0004\u0018\u00010lH\u0001\u00a2\u0006\u0002\bmJ\u0010\u0010n\u001a\u00020b2\u0006\u0010o\u001a\u00020pH\u0002J\u0015\u0010q\u001a\u00020b2\u0006\u0010r\u001a\u00020jH\u0001\u00a2\u0006\u0002\bsJ\u0010\u0010t\u001a\u00020b2\u0006\u0010u\u001a\u00020vH\u0002J\u001d\u0010t\u001a\u00020b2\u0006\u0010u\u001a\u00020v2\u0006\u0010w\u001a\u00020lH\u0011\u00a2\u0006\u0002\bxJ\u001d\u0010y\u001a\u00020b2\u0006\u0010c\u001a\u00020d2\u0006\u0010z\u001a\u00020{H\u0001\u00a2\u0006\u0002\b|J\u0015\u0010}\u001a\u00020b2\u0006\u0010c\u001a\u00020dH\u0001\u00a2\u0006\u0002\b~J\u0011\u0010\u007f\u001a\u00020b2\u0007\u0010\u0080\u0001\u001a\u00020\u001cH\u0016J\'\u0010\u0081\u0001\u001a\u00020\u001c2\u0007\u0010\u0082\u0001\u001a\u00020j2\n\u0010\u0083\u0001\u001a\u0005\u0018\u00010\u0084\u00012\u0007\u0010\u0085\u0001\u001a\u00020jH\u0016J\t\u0010\u0086\u0001\u001a\u00020\u001cH\u0017J\u0013\u0010\u0087\u0001\u001a\u00020b2\b\u0010\u0088\u0001\u001a\u00030\u0089\u0001H\u0016J+\u0010\u008a\u0001\u001a\u00020v2\b\u0010\u008b\u0001\u001a\u00030\u008c\u00012\n\u0010\u008d\u0001\u001a\u0005\u0018\u00010\u008e\u00012\n\u0010\u008f\u0001\u001a\u0005\u0018\u00010\u0090\u0001H\u0017J\t\u0010\u0091\u0001\u001a\u00020bH\u0016J\t\u0010\u0092\u0001\u001a\u00020\u001cH\u0016J\t\u0010\u0093\u0001\u001a\u00020bH\u0017J\u0010\u0010\u0094\u0001\u001a\u00020b2\u0007\u0010\u0080\u0001\u001a\u00020\u001cJ0\u0010\u0095\u0001\u001a\u00020b2\u0007\u0010\u0082\u0001\u001a\u00020j2\u000e\u0010\u0096\u0001\u001a\t\u0012\u0004\u0012\u00020+0\u0097\u00012\b\u0010\u0098\u0001\u001a\u00030\u0099\u0001\u00a2\u0006\u0003\u0010\u009a\u0001J\t\u0010\u009b\u0001\u001a\u00020bH\u0017J\u0011\u0010\u009c\u0001\u001a\u00020b2\b\u0010\u009d\u0001\u001a\u00030\u0090\u0001J\t\u0010\u009e\u0001\u001a\u00020bH\u0017J\u001b\u0010\u009f\u0001\u001a\u00020b2\u0006\u0010u\u001a\u00020v2\n\u0010\u008f\u0001\u001a\u0005\u0018\u00010\u0090\u0001J\u0013\u0010\u00a0\u0001\u001a\u00020b2\n\u0010\u008f\u0001\u001a\u0005\u0018\u00010\u0090\u0001J\u0012\u0010\u00a1\u0001\u001a\u00020b2\u0007\u0010\u00a2\u0001\u001a\u00020lH\u0002J\t\u0010\u00a3\u0001\u001a\u00020\u001cH\u0014J<\u0010\u00a4\u0001\u001a\u00020b2\t\u0010\u00a5\u0001\u001a\u0004\u0018\u00010+2\u0006\u0010c\u001a\u00020d2\u0006\u0010u\u001a\u00020v2\b\u0010\u00a6\u0001\u001a\u00030\u00a7\u00012\u0006\u0010r\u001a\u00020jH\u0001\u00a2\u0006\u0003\b\u00a8\u0001J\u000f\u0010\u00a9\u0001\u001a\u00020bH\u0000\u00a2\u0006\u0003\b\u00aa\u0001J(\u0010\u00ab\u0001\u001a\u00020b2\t\u0010\u00a5\u0001\u001a\u0004\u0018\u00010+2\b\u0010\u00ac\u0001\u001a\u00030\u00ad\u00012\b\u0010\u00ae\u0001\u001a\u00030\u00af\u0001H\u0002J\u0017\u0010\u00b0\u0001\u001a\u00020\u001c2\u0006\u0010g\u001a\u00020\u001cH\u0001\u00a2\u0006\u0003\b\u00b1\u0001J\u0018\u0010\u00b2\u0001\u001a\u00020b2\u0007\u0010\u00a2\u0001\u001a\u00020lH\u0001\u00a2\u0006\u0003\b\u00b3\u0001J\u0012\u0010\u00b4\u0001\u001a\u00020b2\u0007\u0010\u00b5\u0001\u001a\u00020jH\u0002R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R&\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\b\f\u0010\u0005\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u00020\u00078DX\u0084\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0015\u0010\u0016R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u001b\u001a\u00020\u001c8\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\b\u001d\u0010\u0005\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\u0014\u0010\"\u001a\u00020\t8DX\u0084\u0004\u00a2\u0006\u0006\u001a\u0004\b#\u0010$R\u001a\u0010%\u001a\u00020\u000b8@X\u0081\u0004\u00a2\u0006\f\u0012\u0004\b&\u0010\u0005\u001a\u0004\b\'\u0010\u000eR\u0014\u0010(\u001a\b\u0012\u0004\u0012\u00020)0\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010*\u001a\u0004\u0018\u00010+X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010-\"\u0004\b.\u0010/R\u0014\u00100\u001a\b\u0012\u0004\u0012\u0002010\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u00102\u001a\b\u0012\u0004\u0012\u0002030\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u00104\u001a\b\u0012\u0004\u0012\u0002050\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u00106\u001a\b\u0012\u0004\u0012\u0002070\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u00108\u001a\u0002098BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b<\u0010=\u001a\u0004\b:\u0010;R\u0010\u0010>\u001a\u0004\u0018\u00010?X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010@\u001a\u0004\u0018\u00010AX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010B\u001a\b\u0012\u0004\u0012\u00020C0\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010D\u001a\b\u0012\u0004\u0012\u00020E0\u0012\u00a2\u0006\b\n\u0000\u001a\u0004\bF\u0010GR\u0014\u0010H\u001a\b\u0012\u0004\u0012\u00020I0\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010J\u001a\b\u0012\u0004\u0012\u00020K0\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010L\u001a\b\u0012\u0004\u0012\u00020M0\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010N\u001a\b\u0012\u0004\u0012\u00020O0\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010P\u001a\u00020Q8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\bT\u0010=\u001a\u0004\bR\u0010SR\u0014\u0010U\u001a\b\u0012\u0004\u0012\u00020V0\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010W\u001a\b\u0012\u0004\u0012\u00020X0\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010Y\u001a\b\u0012\u0004\u0012\u00020Z0\u0012X\u0084\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b[\u0010GR\u0014\u0010\\\u001a\b\u0012\u0004\u0012\u00020]0\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010^\u001a\u00020\u001cX\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b_\u0010\u001f\"\u0004\b`\u0010!\u00a8\u0006\u00b7\u0001"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/BaseBrowserFragment;", "Landroidx/fragment/app/Fragment;", "Lmozilla/components/support/base/feature/UserInteractionHandler;", "Lmozilla/components/support/base/feature/ActivityResultHandler;", "Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;", "()V", "_binding", "Lcom/cookiejarapps/android/smartcookieweb/databinding/FragmentBrowserBinding;", "_browserInteractor", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarViewInteractor;", "_browserToolbarView", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarView;", "get_browserToolbarView$app_debug$annotations", "get_browserToolbarView$app_debug", "()Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarView;", "set_browserToolbarView$app_debug", "(Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarView;)V", "appLinksFeature", "Lmozilla/components/support/base/feature/ViewBoundFeatureWrapper;", "Lmozilla/components/feature/app/links/AppLinksFeature;", "binding", "getBinding", "()Lcom/cookiejarapps/android/smartcookieweb/databinding/FragmentBrowserBinding;", "browserAnimator", "Lcom/cookiejarapps/android/smartcookieweb/BrowserAnimator;", "browserFragmentStore", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserFragmentStore;", "browserInitialized", "", "getBrowserInitialized$app_debug$annotations", "getBrowserInitialized$app_debug", "()Z", "setBrowserInitialized$app_debug", "(Z)V", "browserInteractor", "getBrowserInteractor", "()Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarViewInteractor;", "browserToolbarView", "getBrowserToolbarView$app_debug$annotations", "getBrowserToolbarView$app_debug", "contextMenuIntegration", "Lcom/cookiejarapps/android/smartcookieweb/integration/ContextMenuIntegration;", "customTabSessionId", "", "getCustomTabSessionId", "()Ljava/lang/String;", "setCustomTabSessionId", "(Ljava/lang/String;)V", "downloadsFeature", "Lmozilla/components/feature/downloads/DownloadsFeature;", "findInPageIntegration", "Lcom/cookiejarapps/android/smartcookieweb/integration/FindInPageIntegration;", "fullScreenFeature", "Lmozilla/components/feature/session/FullScreenFeature;", "fullScreenMediaSessionFeature", "Lmozilla/components/feature/media/fullscreen/MediaSessionFullscreenFeature;", "homeViewModel", "Lorg/mozilla/fenix/home/HomeScreenViewModel;", "getHomeViewModel", "()Lorg/mozilla/fenix/home/HomeScreenViewModel;", "homeViewModel$delegate", "Lkotlin/Lazy;", "initUIJob", "Lkotlinx/coroutines/Job;", "pipFeature", "Lmozilla/components/feature/session/PictureInPictureFeature;", "promptsFeature", "Lmozilla/components/feature/prompts/PromptFeature;", "readerViewFeature", "Lcom/cookiejarapps/android/smartcookieweb/integration/ReaderModeIntegration;", "getReaderViewFeature", "()Lmozilla/components/support/base/feature/ViewBoundFeatureWrapper;", "searchFeature", "Lmozilla/components/feature/search/SearchFeature;", "secureWindowFeature", "Lmozilla/components/feature/privatemode/feature/SecureWindowFeature;", "sessionFeature", "Lmozilla/components/feature/session/SessionFeature;", "shareDownloadsFeature", "Lmozilla/components/feature/downloads/share/ShareDownloadFeature;", "sharedViewModel", "Lorg/mozilla/fenix/home/SharedViewModel;", "getSharedViewModel", "()Lorg/mozilla/fenix/home/SharedViewModel;", "sharedViewModel$delegate", "sitePermissionsFeature", "Lmozilla/components/feature/sitepermissions/SitePermissionsFeature;", "swipeRefreshFeature", "Lmozilla/components/feature/session/SwipeRefreshFeature;", "thumbnailsFeature", "Lmozilla/components/browser/thumbnails/BrowserThumbnails;", "getThumbnailsFeature", "toolbarIntegration", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarIntegration;", "webAppToolbarShouldBeVisible", "getWebAppToolbarShouldBeVisible", "setWebAppToolbarShouldBeVisible", "expandToolbarOnNavigation", "", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "expandToolbarOnNavigation$app_debug", "fullScreenChanged", "inFullScreen", "fullScreenChanged$app_debug", "getAppropriateLayoutGravity", "", "getCurrentTab", "Lmozilla/components/browser/state/state/SessionState;", "getCurrentTab$app_debug", "handleTabSelected", "selectedTab", "Lmozilla/components/browser/state/state/TabSessionState;", "initializeEngineView", "toolbarHeight", "initializeEngineView$app_debug", "initializeUI", "view", "Landroid/view/View;", "tab", "initializeUI$app_debug", "observeRestoreComplete", "navController", "Landroidx/navigation/NavController;", "observeRestoreComplete$app_debug", "observeTabSelection", "observeTabSelection$app_debug", "onAccessibilityStateChanged", "enabled", "onActivityResult", "requestCode", "data", "Landroid/content/Intent;", "resultCode", "onBackPressed", "onConfigurationChanged", "newConfig", "Landroid/content/res/Configuration;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDestroyView", "onHomePressed", "onPause", "onPictureInPictureModeChanged", "onRequestPermissionsResult", "permissions", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onResume", "onSaveInstanceState", "outState", "onStop", "onViewCreated", "onViewStateRestored", "pipModeChanged", "session", "removeSessionIfNeeded", "resumeDownloadDialogState", "sessionId", "context", "Landroid/content/Context;", "resumeDownloadDialogState$app_debug", "safeInvalidateBrowserToolbarView", "safeInvalidateBrowserToolbarView$app_debug", "saveDownloadDialogState", "downloadState", "Lmozilla/components/browser/state/state/content/DownloadState;", "downloadJobStatus", "Lmozilla/components/browser/state/state/content/DownloadState$Status;", "shouldPullToRefreshBeEnabled", "shouldPullToRefreshBeEnabled$app_debug", "updateThemeForSession", "updateThemeForSession$app_debug", "viewportFitChange", "layoutInDisplayCutoutMode", "Companion", "app_debug"})
public abstract class BaseBrowserFragment extends androidx.fragment.app.Fragment implements mozilla.components.support.base.feature.UserInteractionHandler, mozilla.components.support.base.feature.ActivityResultHandler, android.view.accessibility.AccessibilityManager.AccessibilityStateChangeListener {
    private com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserFragmentStore browserFragmentStore;
    private com.cookiejarapps.android.smartcookieweb.BrowserAnimator browserAnimator;
    private com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarViewInteractor _browserInteractor;
    @org.jetbrains.annotations.Nullable()
    private com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarView _browserToolbarView;
    @org.jetbrains.annotations.NotNull()
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.browser.thumbnails.BrowserThumbnails> thumbnailsFeature = null;
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.feature.session.SessionFeature> sessionFeature = null;
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<com.cookiejarapps.android.smartcookieweb.integration.ContextMenuIntegration> contextMenuIntegration = null;
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.feature.downloads.DownloadsFeature> downloadsFeature = null;
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.feature.downloads.share.ShareDownloadFeature> shareDownloadsFeature = null;
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.feature.app.links.AppLinksFeature> appLinksFeature = null;
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.feature.prompts.PromptFeature> promptsFeature = null;
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<com.cookiejarapps.android.smartcookieweb.integration.FindInPageIntegration> findInPageIntegration = null;
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarIntegration> toolbarIntegration = null;
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.feature.sitepermissions.SitePermissionsFeature> sitePermissionsFeature = null;
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.feature.session.FullScreenFeature> fullScreenFeature = null;
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.feature.session.SwipeRefreshFeature> swipeRefreshFeature = null;
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.feature.privatemode.feature.SecureWindowFeature> secureWindowFeature = null;
    private mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.feature.media.fullscreen.MediaSessionFullscreenFeature> fullScreenMediaSessionFeature;
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.feature.search.SearchFeature> searchFeature = null;
    private mozilla.components.feature.session.PictureInPictureFeature pipFeature;
    @org.jetbrains.annotations.NotNull()
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<com.cookiejarapps.android.smartcookieweb.integration.ReaderModeIntegration> readerViewFeature = null;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String customTabSessionId;
    private boolean browserInitialized = false;
    private kotlinx.coroutines.Job initUIJob;
    private boolean webAppToolbarShouldBeVisible = true;
    private final kotlin.Lazy sharedViewModel$delegate = null;
    private final kotlin.Lazy homeViewModel$delegate = null;
    private com.cookiejarapps.android.smartcookieweb.databinding.FragmentBrowserBinding _binding;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.BaseBrowserFragment.Companion Companion = null;
    private static final java.lang.String KEY_CUSTOM_TAB_SESSION_ID = "custom_tab_session_id";
    private static final int REQUEST_CODE_DOWNLOAD_PERMISSIONS = 1;
    private static final int REQUEST_CODE_PROMPT_PERMISSIONS = 2;
    private static final int REQUEST_CODE_APP_PERMISSIONS = 3;
    
    public BaseBrowserFragment() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarViewInteractor getBrowserInteractor() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarView get_browserToolbarView$app_debug() {
        return null;
    }
    
    @kotlin.Suppress(names = {"VariableNaming"})
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void get_browserToolbarView$app_debug$annotations() {
    }
    
    public final void set_browserToolbarView$app_debug(@org.jetbrains.annotations.Nullable()
    com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarView p0) {
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getBrowserToolbarView$app_debug$annotations() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarView getBrowserToolbarView$app_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.browser.thumbnails.BrowserThumbnails> getThumbnailsFeature() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<com.cookiejarapps.android.smartcookieweb.integration.ReaderModeIntegration> getReaderViewFeature() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCustomTabSessionId() {
        return null;
    }
    
    public final void setCustomTabSessionId(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public final boolean getBrowserInitialized$app_debug() {
        return false;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getBrowserInitialized$app_debug$annotations() {
    }
    
    public final void setBrowserInitialized$app_debug(boolean p0) {
    }
    
    protected final boolean getWebAppToolbarShouldBeVisible() {
        return false;
    }
    
    protected final void setWebAppToolbarShouldBeVisible(boolean p0) {
    }
    
    private final org.mozilla.fenix.home.SharedViewModel getSharedViewModel() {
        return null;
    }
    
    private final org.mozilla.fenix.home.HomeScreenViewModel getHomeViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final com.cookiejarapps.android.smartcookieweb.databinding.FragmentBrowserBinding getBinding() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.annotation.CallSuper()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public final void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initializeUI(android.view.View view) {
    }
    
    @androidx.annotation.CallSuper()
    @kotlin.Suppress(names = {"ComplexMethod", "LongMethod"})
    public void initializeUI$app_debug(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.state.SessionState tab) {
    }
    
    @androidx.annotation.VisibleForTesting()
    public final void expandToolbarOnNavigation$app_debug(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore store) {
    }
    
    /**
     * Preserves current state of the [DynamicDownloadDialog] to persist through tab changes and
     * other fragments navigation.
     */
    private final void saveDownloadDialogState(java.lang.String sessionId, mozilla.components.browser.state.state.content.DownloadState downloadState, mozilla.components.browser.state.state.content.DownloadState.Status downloadJobStatus) {
    }
    
    /**
     * Re-initializes [DynamicDownloadDialog] if the user hasn't dismissed the dialog
     * before navigating away from it's original tab.
     * onTryAgain it will use [ContentAction.UpdateDownloadAction] to re-enqueue the former failed
     * download, because [DownloadsFeature] clears any queued downloads onStop.
     */
    @androidx.annotation.VisibleForTesting()
    public final void resumeDownloadDialogState$app_debug(@org.jetbrains.annotations.Nullable()
    java.lang.String sessionId, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore store, @org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.NotNull()
    android.content.Context context, int toolbarHeight) {
    }
    
    @androidx.annotation.VisibleForTesting()
    public final boolean shouldPullToRefreshBeEnabled$app_debug(boolean inFullScreen) {
        return false;
    }
    
    @androidx.annotation.VisibleForTesting()
    public final void initializeEngineView$app_debug(int toolbarHeight) {
    }
    
    @androidx.annotation.VisibleForTesting()
    public final void observeRestoreComplete$app_debug(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore store, @org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController) {
    }
    
    @androidx.annotation.VisibleForTesting()
    public final void observeTabSelection$app_debug(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore store) {
    }
    
    private final void handleTabSelected(mozilla.components.browser.state.state.TabSessionState selectedTab) {
    }
    
    @androidx.annotation.CallSuper()
    @java.lang.Override()
    public void onResume() {
    }
    
    @androidx.annotation.CallSuper()
    @java.lang.Override()
    public void onPause() {
    }
    
    @androidx.annotation.CallSuper()
    @java.lang.Override()
    public void onStop() {
    }
    
    @androidx.annotation.CallSuper()
    @java.lang.Override()
    public boolean onBackPressed() {
        return false;
    }
    
    /**
     * Saves the external app session ID to be restored later in [onViewStateRestored].
     */
    @java.lang.Override()
    public final void onSaveInstanceState(@org.jetbrains.annotations.NotNull()
    android.os.Bundle outState) {
    }
    
    /**
     * Retrieves the external app session ID saved by [onSaveInstanceState].
     */
    @java.lang.Override()
    public final void onViewStateRestored(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    /**
     * Forwards permission grant results to one of the features.
     */
    @java.lang.Override()
    public final void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    /**
     * Forwards activity results to the [ActivityResultHandler] features.
     */
    @java.lang.Override()
    public boolean onActivityResult(int requestCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data, int resultCode) {
        return false;
    }
    
    /**
     * Removes the session if it was opened by an ACTION_VIEW intent
     * or if it has a parent session and no more history
     */
    protected boolean removeSessionIfNeeded() {
        return false;
    }
    
    /**
     * Returns the layout [android.view.Gravity] for the quick settings and ETP dialog.
     */
    protected final int getAppropriateLayoutGravity() {
        return 0;
    }
    
    /**
     * Set the activity normal/private theme to match the current session.
     */
    @androidx.annotation.VisibleForTesting()
    public final void updateThemeForSession$app_debug(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.state.SessionState session) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.annotation.VisibleForTesting()
    public final mozilla.components.browser.state.state.SessionState getCurrentTab$app_debug() {
        return null;
    }
    
    @java.lang.Override()
    public boolean onHomePressed() {
        return false;
    }
    
    /**
     * Exit fullscreen mode when exiting PIP mode
     */
    private final void pipModeChanged(mozilla.components.browser.state.state.SessionState session) {
    }
    
    @java.lang.Override()
    public final void onPictureInPictureModeChanged(boolean enabled) {
    }
    
    private final void viewportFitChange(int layoutInDisplayCutoutMode) {
    }
    
    @androidx.annotation.VisibleForTesting()
    public final void fullScreenChanged$app_debug(boolean inFullScreen) {
    }
    
    @java.lang.Override()
    public void onDestroyView() {
    }
    
    @java.lang.Override()
    public void onAccessibilityStateChanged(boolean enabled) {
    }
    
    @java.lang.Override()
    public void onConfigurationChanged(@org.jetbrains.annotations.NotNull()
    android.content.res.Configuration newConfig) {
    }
    
    public final void safeInvalidateBrowserToolbarView$app_debug() {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0006X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/BaseBrowserFragment$Companion;", "", "()V", "KEY_CUSTOM_TAB_SESSION_ID", "", "REQUEST_CODE_APP_PERMISSIONS", "", "REQUEST_CODE_DOWNLOAD_PERMISSIONS", "REQUEST_CODE_PROMPT_PERMISSIONS", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}