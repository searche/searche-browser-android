package com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u001f\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0002\u00a2\u0006\u0002\u0010\tB\u001f\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\fJ\"\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\b\u0010\n\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u000b\u001a\u00020\u0003H\u0014J$\u0010\u0011\u001a\u0004\u0018\u00010\u00022\b\u0010\b\u001a\u0004\u0018\u00010\u00022\u0006\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u0003H\u0014\u00a8\u0006\u0012"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/AddBookmarkSiteDialog;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/AddBookmarkDialog;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkSiteItem;", "", "context", "Landroid/content/Context;", "manager", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/repository/BookmarkManager;", "item", "(Landroid/content/Context;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/repository/BookmarkManager;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkSiteItem;)V", "title", "url", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V", "initView", "", "view", "Landroid/view/View;", "makeItem", "app_debug"})
public final class AddBookmarkSiteDialog extends com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.AddBookmarkDialog<com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkSiteItem, java.lang.String> {
    
    public AddBookmarkSiteDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository.BookmarkManager manager, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkSiteItem item) {
        super(null, null, null, null, null);
    }
    
    public AddBookmarkSiteDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String url) {
        super(null, null, null, null, null);
    }
    
    @java.lang.Override()
    protected void initView(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String url) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    protected com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkSiteItem makeItem(@org.jetbrains.annotations.Nullable()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkSiteItem item, @org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String url) {
        return null;
    }
}