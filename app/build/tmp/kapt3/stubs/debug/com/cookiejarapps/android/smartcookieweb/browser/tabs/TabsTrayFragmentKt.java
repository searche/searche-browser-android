package com.cookiejarapps.android.smartcookieweb.browser.tabs;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"filterFromConfig", "", "Lmozilla/components/browser/state/state/TabSessionState;", "configuration", "Lcom/cookiejarapps/android/smartcookieweb/browser/tabs/Configuration;", "app_debug"})
public final class TabsTrayFragmentKt {
    
    public static final boolean filterFromConfig(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.state.TabSessionState $this$filterFromConfig, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.tabs.Configuration configuration) {
        return false;
    }
}