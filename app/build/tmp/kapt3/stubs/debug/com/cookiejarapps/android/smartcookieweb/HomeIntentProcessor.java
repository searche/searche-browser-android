package com.cookiejarapps.android.smartcookieweb;

import java.lang.System;

/**
 * Processor for Android intents received in [com.cookiejarapps.android.smartcookieweb.BrowserActivity].
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0005H&\u00a8\u0006\t"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/HomeIntentProcessor;", "", "process", "", "intent", "Landroid/content/Intent;", "navController", "Landroidx/navigation/NavController;", "out", "app_debug"})
public abstract interface HomeIntentProcessor {
    
    public abstract boolean process(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent, @org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController, @org.jetbrains.annotations.NotNull()
    android.content.Intent out);
}