package com.cookiejarapps.android.smartcookieweb.ext;

import java.lang.System;

@kotlin.Suppress(names = {"TooManyFunctions"})
@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000.\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\r\u001a\f\u0010\u0004\u001a\u00020\u0001*\u00020\u0005H\u0001\u001a\n\u0010\u0006\u001a\u00020\u0007*\u00020\u0005\u001a\f\u0010\b\u001a\u0004\u0018\u00010\t*\u00020\u0005\u001a\f\u0010\n\u001a\u00020\u0007*\u00020\u0005H\u0001\u001a\u0014\u0010\u000b\u001a\u00020\f*\u00020\u00052\b\b\u0001\u0010\r\u001a\u00020\u0001\u001a\n\u0010\u000e\u001a\u00020\u000f*\u00020\u0005\u001a\n\u0010\u0010\u001a\u00020\f*\u00020\u0005\u001a\u0012\u0010\u0011\u001a\u00020\f*\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0005\u001a\u001a\u0010\u0013\u001a\u00020\f*\u00020\u00052\u0006\u0010\u0014\u001a\u00020\u00012\u0006\u0010\u0015\u001a\u00020\u0001\u001a6\u0010\u0016\u001a\u00020\f*\u00020\u00052\u0006\u0010\u0017\u001a\u00020\u00012\u0006\u0010\u0018\u001a\u00020\u00012\u0006\u0010\u0019\u001a\u00020\u000f2\b\b\u0002\u0010\u001a\u001a\u00020\u00012\b\b\u0002\u0010\u001b\u001a\u00020\u0001\"\u0016\u0010\u0000\u001a\u00020\u00018\u0000X\u0081T\u00a2\u0006\b\n\u0000\u0012\u0004\b\u0002\u0010\u0003\u00a8\u0006\u001c"}, d2 = {"MINIMUM_KEYBOARD_HEIGHT", "", "getMINIMUM_KEYBOARD_HEIGHT$annotations", "()V", "getKeyboardHeight", "Landroid/view/View;", "getRectWithScreenLocation", "Landroid/graphics/Rect;", "getWindowInsets", "Landroidx/core/view/WindowInsetsCompat;", "getWindowVisibleDisplayFrame", "increaseTapArea", "", "extraDps", "isKeyboardVisible", "", "removeTouchDelegate", "setNewAccessibilityParent", "newParent", "updateAccessibilityCollectionInfo", "rowCount", "columnCount", "updateAccessibilityCollectionItemInfo", "rowIndex", "columnIndex", "isSelected", "rowSpan", "columnSpan", "app_debug"})
public final class ViewKt {
    
    /**
     * The assumed minimum height of the keyboard.
     */
    @androidx.annotation.Dimension(unit = androidx.annotation.Dimension.DP)
    public static final int MINIMUM_KEYBOARD_HEIGHT = 100;
    
    public static final void increaseTapArea(@org.jetbrains.annotations.NotNull()
    android.view.View $this$increaseTapArea, @androidx.annotation.Dimension(unit = 0)
    int extraDps) {
    }
    
    public static final void removeTouchDelegate(@org.jetbrains.annotations.NotNull()
    android.view.View $this$removeTouchDelegate) {
    }
    
    /**
     * Sets the new a11y parent.
     */
    public static final void setNewAccessibilityParent(@org.jetbrains.annotations.NotNull()
    android.view.View $this$setNewAccessibilityParent, @org.jetbrains.annotations.NotNull()
    android.view.View newParent) {
    }
    
    /**
     * Updates the a11y collection item info for an item in a list.
     */
    public static final void updateAccessibilityCollectionItemInfo(@org.jetbrains.annotations.NotNull()
    android.view.View $this$updateAccessibilityCollectionItemInfo, int rowIndex, int columnIndex, boolean isSelected, int rowSpan, int columnSpan) {
    }
    
    /**
     * Updates the a11y collection info for a list.
     */
    public static final void updateAccessibilityCollectionInfo(@org.jetbrains.annotations.NotNull()
    android.view.View $this$updateAccessibilityCollectionInfo, int rowCount, int columnCount) {
    }
    
    /**
     * Fills a [Rect] with data about a view's location in the screen.
     *
     * @see View.getLocationOnScreen
     * @see View.getRectWithViewLocation for a version of this that is relative to a window
     */
    @org.jetbrains.annotations.NotNull()
    public static final android.graphics.Rect getRectWithScreenLocation(@org.jetbrains.annotations.NotNull()
    android.view.View $this$getRectWithScreenLocation) {
        return null;
    }
    
    /**
     * A safer version of [ViewCompat.getRootWindowInsets] that does not throw a NullPointerException
     * if the view is not attached.
     */
    @org.jetbrains.annotations.Nullable()
    public static final androidx.core.view.WindowInsetsCompat getWindowInsets(@org.jetbrains.annotations.NotNull()
    android.view.View $this$getWindowInsets) {
        return null;
    }
    
    /**
     * Checks if the keyboard is visible
     *
     * Inspired by https://stackoverflow.com/questions/2150078/how-to-check-visibility-of-software-keyboard-in-android
     * API 30 adds a native method for this. We should use it (and a compat method if one
     * is added) when it becomes available
     */
    public static final boolean isKeyboardVisible(@org.jetbrains.annotations.NotNull()
    android.view.View $this$isKeyboardVisible) {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.annotation.VisibleForTesting()
    public static final android.graphics.Rect getWindowVisibleDisplayFrame(@org.jetbrains.annotations.NotNull()
    android.view.View $this$getWindowVisibleDisplayFrame) {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    public static final int getKeyboardHeight(@org.jetbrains.annotations.NotNull()
    android.view.View $this$getKeyboardHeight) {
        return 0;
    }
    
    /**
     * The assumed minimum height of the keyboard.
     */
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getMINIMUM_KEYBOARD_HEIGHT$annotations() {
    }
}