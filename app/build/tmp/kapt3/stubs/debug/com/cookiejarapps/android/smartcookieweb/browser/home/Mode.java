package com.cookiejarapps.android.smartcookieweb.browser.home;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00032\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0002\u0006\u0007\u00a8\u0006\b"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/Mode;", "", "()V", "Companion", "Normal", "Private", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/Mode$Normal;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/Mode$Private;", "app_debug"})
public abstract class Mode {
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.browser.home.Mode.Companion Companion = null;
    
    private Mode() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/Mode$Normal;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/Mode;", "()V", "app_debug"})
    public static final class Normal extends com.cookiejarapps.android.smartcookieweb.browser.home.Mode {
        @org.jetbrains.annotations.NotNull()
        public static final com.cookiejarapps.android.smartcookieweb.browser.home.Mode.Normal INSTANCE = null;
        
        private Normal() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/Mode$Private;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/Mode;", "()V", "app_debug"})
    public static final class Private extends com.cookiejarapps.android.smartcookieweb.browser.home.Mode {
        @org.jetbrains.annotations.NotNull()
        public static final com.cookiejarapps.android.smartcookieweb.browser.home.Mode.Private INSTANCE = null;
        
        private Private() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/Mode$Companion;", "", "()V", "fromBrowsingMode", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/Mode;", "browsingMode", "Lcom/cookiejarapps/android/smartcookieweb/browser/BrowsingMode;", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.cookiejarapps.android.smartcookieweb.browser.home.Mode fromBrowsingMode(@org.jetbrains.annotations.NotNull()
        com.cookiejarapps.android.smartcookieweb.browser.BrowsingMode browsingMode) {
            return null;
        }
    }
}