package com.cookiejarapps.android.smartcookieweb.components;

import java.lang.System;

/**
 * ViewModel factory to create [StoreProvider] instances.
 *
 * @param createStore Callback to create a new [Store], used when the [ViewModel] is first created.
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000*\u0010\b\u0000\u0010\u0001*\n\u0012\u0002\b\u0003\u0012\u0002\b\u00030\u00022\u00020\u0003B\u0013\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\u00a2\u0006\u0002\u0010\u0006J\'\u0010\u0007\u001a\u0002H\b\"\n\b\u0001\u0010\b*\u0004\u0018\u00010\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016\u00a2\u0006\u0002\u0010\fR\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/StoreProviderFactory;", "T", "Lmozilla/components/lib/state/Store;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "createStore", "Lkotlin/Function0;", "(Lkotlin/jvm/functions/Function0;)V", "create", "VM", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "app_debug"})
public final class StoreProviderFactory<T extends mozilla.components.lib.state.Store<?, ?>> implements androidx.lifecycle.ViewModelProvider.Factory {
    private final kotlin.jvm.functions.Function0<T> createStore = null;
    
    public StoreProviderFactory(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<? extends T> createStore) {
        super();
    }
    
    @kotlin.Suppress(names = {"UNCHECKED_CAST"})
    @java.lang.Override()
    public <VM extends androidx.lifecycle.ViewModel>VM create(@org.jetbrains.annotations.NotNull()
    java.lang.Class<VM> modelClass) {
        return null;
    }
}