package com.cookiejarapps.android.smartcookieweb.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J#\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0004H\u0001\u00a2\u0006\u0002\b\bJR\u0010\t\u001a\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f2\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00042\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\n0\u000f2\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\n0\u000f2\b\b\u0002\u0010\u0011\u001a\u00020\u0012\u00a8\u0006\u0013"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/utils/ToolbarPopupWindow;", "", "()V", "getUrlForClipboard", "", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "customTabId", "getUrlForClipboard$app_debug", "show", "", "view", "Ljava/lang/ref/WeakReference;", "Landroid/view/View;", "handlePasteAndGo", "Lkotlin/Function1;", "handlePaste", "copyVisible", "", "app_debug"})
public final class ToolbarPopupWindow {
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.utils.ToolbarPopupWindow INSTANCE = null;
    
    private ToolbarPopupWindow() {
        super();
    }
    
    public final void show(@org.jetbrains.annotations.NotNull()
    java.lang.ref.WeakReference<android.view.View> view, @org.jetbrains.annotations.Nullable()
    java.lang.String customTabId, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> handlePasteAndGo, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> handlePaste, boolean copyVisible) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.annotation.VisibleForTesting()
    public final java.lang.String getUrlForClipboard$app_debug(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore store, @org.jetbrains.annotations.Nullable()
    java.lang.String customTabId) {
        return null;
    }
}