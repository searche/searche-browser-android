package com.cookiejarapps.android.smartcookieweb.components;

import java.lang.System;

/**
 * Generic ViewModel to wrap a State object for state restoration.
 *
 * @property store [Store] instance attached to [ViewModel].
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \t*\u0010\b\u0000\u0010\u0001*\n\u0012\u0002\b\u0003\u0012\u0002\b\u00030\u00022\u00020\u0003:\u0001\tB\r\u0012\u0006\u0010\u0004\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0005R\u0013\u0010\u0004\u001a\u00028\u0000\u00a2\u0006\n\n\u0002\u0010\b\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\n"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/StoreProvider;", "T", "Lmozilla/components/lib/state/Store;", "Landroidx/lifecycle/ViewModel;", "store", "(Lmozilla/components/lib/state/Store;)V", "getStore", "()Lmozilla/components/lib/state/Store;", "Lmozilla/components/lib/state/Store;", "Companion", "app_debug"})
public final class StoreProvider<T extends mozilla.components.lib.state.Store<?, ?>> extends androidx.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    private final T store = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.components.StoreProvider.Companion Companion = null;
    
    public StoreProvider(@org.jetbrains.annotations.NotNull()
    T store) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final T getStore() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J3\u0010\u0003\u001a\u0002H\u0004\"\u0010\b\u0001\u0010\u0004*\n\u0012\u0002\b\u0003\u0012\u0002\b\u00030\u00052\u0006\u0010\u0006\u001a\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00040\t\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/StoreProvider$Companion;", "", "()V", "get", "T", "Lmozilla/components/lib/state/Store;", "fragment", "Landroidx/fragment/app/Fragment;", "createStore", "Lkotlin/Function0;", "(Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function0;)Lmozilla/components/lib/state/Store;", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final <T extends mozilla.components.lib.state.Store<?, ?>>T get(@org.jetbrains.annotations.NotNull()
        androidx.fragment.app.Fragment fragment, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function0<? extends T> createStore) {
            return null;
        }
    }
}