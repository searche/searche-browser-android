package com.cookiejarapps.android.smartcookieweb.settings.fragment;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0002J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0002J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0002J\u0010\u0010\t\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0002J\u0010\u0010\n\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0002J\"\u0010\u000b\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016J\u001c\u0010\u0011\u001a\u00020\u00042\b\u0010\u0012\u001a\u0004\u0018\u00010\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J\b\u0010\u0016\u001a\u00020\u0004H\u0002J\b\u0010\u0017\u001a\u00020\u0004H\u0002J\b\u0010\u0018\u001a\u00020\u0004H\u0002J\b\u0010\u0019\u001a\u00020\u0004H\u0002\u00a8\u0006\u001b"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/settings/fragment/ImportExportSettingsFragment;", "Lcom/cookiejarapps/android/smartcookieweb/settings/fragment/BaseSettingsFragment;", "()V", "clearSettings", "", "exportBookmarks", "uri", "Landroid/net/Uri;", "exportSettings", "importBookmarks", "importSettings", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreatePreferences", "savedInstanceState", "Landroid/os/Bundle;", "s", "", "requestBookmarkExport", "requestBookmarkImport", "requestSettingsExport", "requestSettingsImport", "Companion", "app_debug"})
public final class ImportExportSettingsFragment extends com.cookiejarapps.android.smartcookieweb.settings.fragment.BaseSettingsFragment {
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.settings.fragment.ImportExportSettingsFragment.Companion Companion = null;
    public static final int EXPORT_BOOKMARKS = 0;
    public static final int IMPORT_BOOKMARKS = 1;
    public static final int EXPORT_SETTINGS = 2;
    public static final int IMPORT_SETTINGS = 3;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_URL = "url";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_TITLE = "title";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_FOLDER = "folder";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SCW_PREFERENCES = "scw_preferences";
    
    public ImportExportSettingsFragment() {
        super();
    }
    
    @java.lang.Override()
    public void onCreatePreferences(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState, @org.jetbrains.annotations.Nullable()
    java.lang.String s) {
    }
    
    private final void requestBookmarkImport() {
    }
    
    private final void requestBookmarkExport() {
    }
    
    private final void requestSettingsImport() {
    }
    
    private final void requestSettingsExport() {
    }
    
    private final void importBookmarks(android.net.Uri uri) {
    }
    
    private final void exportBookmarks(android.net.Uri uri) {
    }
    
    private final void clearSettings() {
    }
    
    private final void exportSettings(android.net.Uri uri) {
    }
    
    private final void importSettings(android.net.Uri uri) {
    }
    
    @java.lang.Override()
    public void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/settings/fragment/ImportExportSettingsFragment$Companion;", "", "()V", "EXPORT_BOOKMARKS", "", "EXPORT_SETTINGS", "IMPORT_BOOKMARKS", "IMPORT_SETTINGS", "KEY_FOLDER", "", "KEY_TITLE", "KEY_URL", "SCW_PREFERENCES", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}