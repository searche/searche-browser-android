package com.cookiejarapps.android.smartcookieweb.settings.fragment;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b&\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J<\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00062\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000bH\u0004J6\u0010\u000e\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00062\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\r0\u000fH\u0004J<\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00062\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\r0\u000bH\u0004JD\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0016\u001a\u00020\b2\b\b\u0002\u0010\u0007\u001a\u00020\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00062\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\r0\u000bH\u0004\u00a8\u0006\u0018"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/settings/fragment/BaseSettingsFragment;", "Landroidx/preference/PreferenceFragmentCompat;", "()V", "clickableDynamicPreference", "Landroidx/preference/Preference;", "preference", "", "isEnabled", "", "summary", "onClick", "Lkotlin/Function1;", "Lcom/cookiejarapps/android/smartcookieweb/settings/fragment/SummaryUpdater;", "", "clickablePreference", "Lkotlin/Function0;", "seekbarPreference", "Landroidx/preference/SeekBarPreference;", "onStateChanged", "", "switchPreference", "Landroidx/preference/SwitchPreferenceCompat;", "isChecked", "onCheckChange", "app_debug"})
public abstract class BaseSettingsFragment extends androidx.preference.PreferenceFragmentCompat {
    
    public BaseSettingsFragment() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final androidx.preference.SwitchPreferenceCompat switchPreference(@org.jetbrains.annotations.NotNull()
    java.lang.String preference, boolean isChecked, boolean isEnabled, @org.jetbrains.annotations.Nullable()
    java.lang.String summary, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> onCheckChange) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final androidx.preference.Preference clickablePreference(@org.jetbrains.annotations.NotNull()
    java.lang.String preference, boolean isEnabled, @org.jetbrains.annotations.Nullable()
    java.lang.String summary, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> onClick) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final androidx.preference.Preference clickableDynamicPreference(@org.jetbrains.annotations.NotNull()
    java.lang.String preference, boolean isEnabled, @org.jetbrains.annotations.Nullable()
    java.lang.String summary, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.cookiejarapps.android.smartcookieweb.settings.fragment.SummaryUpdater, kotlin.Unit> onClick) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final androidx.preference.SeekBarPreference seekbarPreference(@org.jetbrains.annotations.NotNull()
    java.lang.String preference, boolean isEnabled, @org.jetbrains.annotations.Nullable()
    java.lang.String summary, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> onStateChanged) {
        return null;
    }
}