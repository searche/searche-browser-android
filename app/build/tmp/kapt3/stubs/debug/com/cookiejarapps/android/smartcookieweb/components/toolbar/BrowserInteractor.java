package com.cookiejarapps.android.smartcookieweb.components.toolbar;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0016\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0016J\u0010\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\f\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u0010\u001a\u00020\b2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\b\u0010\u0013\u001a\u00020\bH\u0016J\u0010\u0010\u0014\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u0015H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserInteractor;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarViewInteractor;", "browserToolbarController", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarController;", "menuController", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarMenuController;", "(Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarController;Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarMenuController;)V", "onBrowserToolbarClicked", "", "onBrowserToolbarMenuItemTapped", "item", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "onBrowserToolbarPaste", "text", "", "onBrowserToolbarPasteAndGo", "onScrolled", "offset", "", "onTabCounterClicked", "onTabCounterMenuItemTapped", "Lmozilla/components/ui/tabcounter/TabCounterMenu$Item;", "app_debug"})
public class BrowserInteractor implements com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarViewInteractor {
    private final com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarController browserToolbarController = null;
    private final com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarMenuController menuController = null;
    
    public BrowserInteractor(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarController browserToolbarController, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarMenuController menuController) {
        super();
    }
    
    @java.lang.Override()
    public void onTabCounterClicked() {
    }
    
    @java.lang.Override()
    public void onTabCounterMenuItemTapped(@org.jetbrains.annotations.NotNull()
    mozilla.components.ui.tabcounter.TabCounterMenu.Item item) {
    }
    
    @java.lang.Override()
    public void onBrowserToolbarPaste(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    @java.lang.Override()
    public void onBrowserToolbarPasteAndGo(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    @java.lang.Override()
    public void onBrowserToolbarClicked() {
    }
    
    @java.lang.Override()
    public void onBrowserToolbarMenuItemTapped(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item item) {
    }
    
    @java.lang.Override()
    public void onScrolled(int offset) {
    }
}