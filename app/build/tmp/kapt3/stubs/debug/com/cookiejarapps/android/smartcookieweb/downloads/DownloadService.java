package com.cookiejarapps.android.smartcookieweb.downloads;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001b\u0010\u0003\u001a\u00020\u00048TX\u0094\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006R\u001b\u0010\t\u001a\u00020\n8TX\u0094\u0084\u0002\u00a2\u0006\f\n\u0004\b\r\u0010\b\u001a\u0004\b\u000b\u0010\fR\u001b\u0010\u000e\u001a\u00020\u000f8TX\u0094\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0012\u0010\b\u001a\u0004\b\u0010\u0010\u0011\u00a8\u0006\u0013"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/downloads/DownloadService;", "Lmozilla/components/feature/downloads/AbstractFetchDownloadService;", "()V", "httpClient", "Lmozilla/components/concept/fetch/Client;", "getHttpClient", "()Lmozilla/components/concept/fetch/Client;", "httpClient$delegate", "Lkotlin/Lazy;", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "getStore", "()Lmozilla/components/browser/state/store/BrowserStore;", "store$delegate", "style", "Lmozilla/components/feature/downloads/AbstractFetchDownloadService$Style;", "getStyle", "()Lmozilla/components/feature/downloads/AbstractFetchDownloadService$Style;", "style$delegate", "app_debug"})
public final class DownloadService extends mozilla.components.feature.downloads.AbstractFetchDownloadService {
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy httpClient$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy store$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy style$delegate = null;
    
    public DownloadService() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    protected mozilla.components.concept.fetch.Client getHttpClient() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    protected mozilla.components.browser.state.store.BrowserStore getStore() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    protected mozilla.components.feature.downloads.AbstractFetchDownloadService.Style getStyle() {
        return null;
    }
}