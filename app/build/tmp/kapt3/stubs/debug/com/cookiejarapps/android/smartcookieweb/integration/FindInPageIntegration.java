package com.cookiejarapps.android.smartcookieweb.integration;

import java.lang.System;

@kotlin.Suppress(names = {"UndocumentedPublicClass"})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u001dB1\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\fJ\r\u0010\r\u001a\u00020\u000eH\u0000\u00a2\u0006\u0002\b\u000fJ\r\u0010\u0010\u001a\u00020\u0011H\u0000\u00a2\u0006\u0002\b\u0012J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0015\u001a\u00020\u000eH\u0016J\r\u0010\u0019\u001a\u00020\u0014H\u0000\u00a2\u0006\u0002\b\u001aJ\r\u0010\u001b\u001a\u00020\u0014H\u0000\u00a2\u0006\u0002\b\u001cR\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/integration/FindInPageIntegration;", "Lcom/cookiejarapps/android/smartcookieweb/integration/InflationAwareFeature;", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "sessionId", "", "stub", "Landroid/view/ViewStub;", "engineView", "Lmozilla/components/concept/engine/EngineView;", "toolbarInfo", "Lcom/cookiejarapps/android/smartcookieweb/integration/FindInPageIntegration$ToolbarInfo;", "(Lmozilla/components/browser/state/store/BrowserStore;Ljava/lang/String;Landroid/view/ViewStub;Lmozilla/components/concept/engine/EngineView;Lcom/cookiejarapps/android/smartcookieweb/integration/FindInPageIntegration$ToolbarInfo;)V", "getEngineViewParent", "Landroid/view/View;", "getEngineViewParent$app_debug", "getEngineViewsParentLayoutParams", "Landroid/view/ViewGroup$MarginLayoutParams;", "getEngineViewsParentLayoutParams$app_debug", "onLaunch", "", "view", "feature", "Lmozilla/components/support/base/feature/LifecycleAwareFeature;", "onViewInflated", "prepareLayoutForFindBar", "prepareLayoutForFindBar$app_debug", "restorePreviousLayout", "restorePreviousLayout$app_debug", "ToolbarInfo", "app_debug"})
public final class FindInPageIntegration extends com.cookiejarapps.android.smartcookieweb.integration.InflationAwareFeature {
    private final mozilla.components.browser.state.store.BrowserStore store = null;
    private final java.lang.String sessionId = null;
    private final mozilla.components.concept.engine.EngineView engineView = null;
    private final com.cookiejarapps.android.smartcookieweb.integration.FindInPageIntegration.ToolbarInfo toolbarInfo = null;
    
    public FindInPageIntegration(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore store, @org.jetbrains.annotations.Nullable()
    java.lang.String sessionId, @org.jetbrains.annotations.NotNull()
    android.view.ViewStub stub, @org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.EngineView engineView, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.integration.FindInPageIntegration.ToolbarInfo toolbarInfo) {
        super(null);
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public mozilla.components.support.base.feature.LifecycleAwareFeature onViewInflated(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
        return null;
    }
    
    @java.lang.Override()
    public void onLaunch(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.NotNull()
    mozilla.components.support.base.feature.LifecycleAwareFeature feature) {
    }
    
    public final void restorePreviousLayout$app_debug() {
    }
    
    public final void prepareLayoutForFindBar$app_debug() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getEngineViewParent$app_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.ViewGroup.MarginLayoutParams getEngineViewsParentLayoutParams$app_debug() {
        return null;
    }
    
    /**
     * Holder of all details needed about the Toolbar.
     * Used to modify the layout of BrowserToolbar while the find in page bar is shown.
     */
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00052\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\bR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/integration/FindInPageIntegration$ToolbarInfo;", "", "toolbar", "Lmozilla/components/browser/toolbar/BrowserToolbar;", "isToolbarDynamic", "", "isToolbarPlacedAtTop", "(Lmozilla/components/browser/toolbar/BrowserToolbar;ZZ)V", "()Z", "getToolbar", "()Lmozilla/components/browser/toolbar/BrowserToolbar;", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "toString", "", "app_debug"})
    public static final class ToolbarInfo {
        @org.jetbrains.annotations.NotNull()
        private final mozilla.components.browser.toolbar.BrowserToolbar toolbar = null;
        private final boolean isToolbarDynamic = false;
        private final boolean isToolbarPlacedAtTop = false;
        
        /**
         * Holder of all details needed about the Toolbar.
         * Used to modify the layout of BrowserToolbar while the find in page bar is shown.
         */
        @org.jetbrains.annotations.NotNull()
        public final com.cookiejarapps.android.smartcookieweb.integration.FindInPageIntegration.ToolbarInfo copy(@org.jetbrains.annotations.NotNull()
        mozilla.components.browser.toolbar.BrowserToolbar toolbar, boolean isToolbarDynamic, boolean isToolbarPlacedAtTop) {
            return null;
        }
        
        /**
         * Holder of all details needed about the Toolbar.
         * Used to modify the layout of BrowserToolbar while the find in page bar is shown.
         */
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        /**
         * Holder of all details needed about the Toolbar.
         * Used to modify the layout of BrowserToolbar while the find in page bar is shown.
         */
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        /**
         * Holder of all details needed about the Toolbar.
         * Used to modify the layout of BrowserToolbar while the find in page bar is shown.
         */
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public ToolbarInfo(@org.jetbrains.annotations.NotNull()
        mozilla.components.browser.toolbar.BrowserToolbar toolbar, boolean isToolbarDynamic, boolean isToolbarPlacedAtTop) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final mozilla.components.browser.toolbar.BrowserToolbar component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final mozilla.components.browser.toolbar.BrowserToolbar getToolbar() {
            return null;
        }
        
        public final boolean component2() {
            return false;
        }
        
        public final boolean isToolbarDynamic() {
            return false;
        }
        
        public final boolean component3() {
            return false;
        }
        
        public final boolean isToolbarPlacedAtTop() {
            return false;
        }
    }
}