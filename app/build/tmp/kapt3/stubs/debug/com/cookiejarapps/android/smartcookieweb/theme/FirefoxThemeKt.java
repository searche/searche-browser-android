package com.cookiejarapps.android.smartcookieweb.theme;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000(\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a%\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\u0011\u0010\t\u001a\r\u0012\u0004\u0012\u00020\u00060\n\u00a2\u0006\u0002\b\u000bH\u0007\u001a#\u0010\f\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u00012\u0011\u0010\t\u001a\r\u0012\u0004\u0012\u00020\u00060\n\u00a2\u0006\u0002\b\u000bH\u0007\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00010\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"darkColorPalette", "Lcom/cookiejarapps/android/smartcookieweb/theme/FirefoxColors;", "lightColorPalette", "localFirefoxColors", "Landroidx/compose/runtime/ProvidableCompositionLocal;", "FirefoxTheme", "", "darkTheme", "", "content", "Lkotlin/Function0;", "Landroidx/compose/runtime/Composable;", "ProvideFirefoxColors", "colors", "app_debug"})
public final class FirefoxThemeKt {
    private static final com.cookiejarapps.android.smartcookieweb.theme.FirefoxColors darkColorPalette = null;
    private static final com.cookiejarapps.android.smartcookieweb.theme.FirefoxColors lightColorPalette = null;
    private static final androidx.compose.runtime.ProvidableCompositionLocal<com.cookiejarapps.android.smartcookieweb.theme.FirefoxColors> localFirefoxColors = null;
    
    /**
     * The theme for Mozilla Firefox for Android (Fenix).
     */
    @androidx.compose.runtime.Composable()
    public static final void FirefoxTheme(boolean darkTheme, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> content) {
    }
    
    @androidx.compose.runtime.Composable()
    public static final void ProvideFirefoxColors(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.theme.FirefoxColors colors, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> content) {
    }
}