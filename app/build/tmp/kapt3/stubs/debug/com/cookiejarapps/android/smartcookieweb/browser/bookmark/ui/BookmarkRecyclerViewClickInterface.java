package com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0018\u0010\b\u001a\u00020\t2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\n"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/BookmarkRecyclerViewClickInterface;", "", "onRecyclerItemClicked", "", "v", "Landroid/view/View;", "position", "", "onRecyclerItemLongClicked", "", "app_debug"})
public abstract interface BookmarkRecyclerViewClickInterface {
    
    public abstract void onRecyclerItemClicked(@org.jetbrains.annotations.NotNull()
    android.view.View v, int position);
    
    public abstract boolean onRecyclerItemLongClicked(@org.jetbrains.annotations.NotNull()
    android.view.View v, int position);
}