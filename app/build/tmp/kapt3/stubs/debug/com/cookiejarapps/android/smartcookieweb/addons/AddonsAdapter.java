package com.cookiejarapps.android.smartcookieweb.addons;

import java.lang.System;

/**
 * An adapter for displaying add-on items. This will display information related to the state of
 * an add-on such as recommended, unsupported or installed. In addition, it will perform actions
 * such as installing an add-on. Compatible with AddonsManagerAdapter.
 *
 * Unlike AddonsManagerAdapter in Mozilla Android Components, this correctly recognises sideloaded
 * add-ons as their own category, instead of add-ons left over from a previous Fennec install.
 *
 * @property addonCollectionProvider Provider of AMO collection API.
 * @property addonsManagerDelegate Delegate that will provides method for handling the add-on items.
 * @param addons The list of add-on based on the AMO store.
 * @property style Indicates how items should look like.
 * @property excludedAddonIDs The list of add-on IDs to be excluded from the recommended section.
 */
@kotlin.Suppress(names = {"LargeClass"})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0096\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0003FGHBG\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f\u0012\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\t\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\u001d\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\nH\u0000\u00a2\u0006\u0002\b!J%\u0010\"\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'H\u0000\u00a2\u0006\u0002\b(J\u0010\u0010)\u001a\u00020\u001f2\u0006\u0010*\u001a\u00020+H\u0002J!\u0010,\u001a\b\u0012\u0004\u0012\u00020\u00020\t2\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0000\u00a2\u0006\u0002\b-J\u0010\u0010.\u001a\u00020\u00032\u0006\u0010*\u001a\u00020+H\u0002J\'\u0010/\u001a\u0002002\u0006\u0010 \u001a\u00020\n2\u0006\u00101\u001a\u0002022\b\b\u0002\u0010\u001a\u001a\u00020\u001bH\u0000\u00a2\u0006\u0002\b3J\u0010\u00104\u001a\u00020\'2\u0006\u0010&\u001a\u00020\'H\u0016J\u0018\u00105\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u00032\u0006\u0010&\u001a\u00020\'H\u0016J\u0018\u00106\u001a\u00020\u00032\u0006\u0010*\u001a\u00020+2\u0006\u00107\u001a\u00020\'H\u0016J\u0006\u00108\u001a\u00020\u001dJ\'\u00109\u001a\u00020\u001d2\u0006\u0010:\u001a\u0002022\u0006\u0010;\u001a\u00020<2\b\b\u0002\u0010=\u001a\u00020\'H\u0000\u00a2\u0006\u0002\b>J(\u0010?\u001a\u00020\u001d2\u0016\u0010@\u001a\u0012\u0012\u0004\u0012\u00020\n0Aj\b\u0012\u0004\u0012\u00020\n`B2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u001e\u0010C\u001a\u00020\u001d2\u0016\u0010@\u001a\u0012\u0012\u0004\u0012\u00020\n0Aj\b\u0012\u0004\u0012\u00020\n`BJ\u000e\u0010D\u001a\u00020\u001d2\u0006\u0010 \u001a\u00020\nJ\u0014\u0010E\u001a\u00020\u001d2\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R0\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\n0\u00138\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006I"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/addons/AddonsAdapter;", "Landroidx/recyclerview/widget/ListAdapter;", "", "Lmozilla/components/feature/addons/ui/CustomViewHolder;", "addonCollectionProvider", "Lmozilla/components/feature/addons/amo/AddonCollectionProvider;", "addonsManagerDelegate", "Lmozilla/components/feature/addons/ui/AddonsManagerAdapterDelegate;", "addons", "", "Lmozilla/components/feature/addons/Addon;", "style", "Lcom/cookiejarapps/android/smartcookieweb/addons/AddonsAdapter$Style;", "excludedAddonIDs", "", "userPreferences", "Lcom/cookiejarapps/android/smartcookieweb/preferences/UserPreferences;", "(Lmozilla/components/feature/addons/amo/AddonCollectionProvider;Lmozilla/components/feature/addons/ui/AddonsManagerAdapterDelegate;Ljava/util/List;Lcom/cookiejarapps/android/smartcookieweb/addons/AddonsAdapter$Style;Ljava/util/List;Lcom/cookiejarapps/android/smartcookieweb/preferences/UserPreferences;)V", "addonsMap", "", "getAddonsMap$app_debug$annotations", "()V", "getAddonsMap$app_debug", "()Ljava/util/Map;", "setAddonsMap$app_debug", "(Ljava/util/Map;)V", "scope", "Lkotlinx/coroutines/CoroutineScope;", "bindAddon", "", "holder", "Lmozilla/components/feature/addons/ui/CustomViewHolder$AddonViewHolder;", "addon", "bindAddon$app_debug", "bindSection", "Lmozilla/components/feature/addons/ui/CustomViewHolder$SectionViewHolder;", "section", "Lcom/cookiejarapps/android/smartcookieweb/addons/AddonsAdapter$Section;", "position", "", "bindSection$app_debug", "createAddonViewHolder", "parent", "Landroid/view/ViewGroup;", "createListWithSections", "createListWithSections$app_debug", "createSectionViewHolder", "fetchIcon", "Lkotlinx/coroutines/Job;", "iconView", "Landroid/widget/ImageView;", "fetchIcon$app_debug", "getItemViewType", "onBindViewHolder", "onCreateViewHolder", "viewType", "reSort", "setWithAnimation", "image", "bitmap", "Landroid/graphics/Bitmap;", "duration", "setWithAnimation$app_debug", "sort", "array", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "sortByAZ", "updateAddon", "updateAddons", "DifferCallback", "Section", "Style", "app_debug"})
public final class AddonsAdapter extends androidx.recyclerview.widget.ListAdapter<java.lang.Object, mozilla.components.feature.addons.ui.CustomViewHolder> {
    private final mozilla.components.feature.addons.amo.AddonCollectionProvider addonCollectionProvider = null;
    private final mozilla.components.feature.addons.ui.AddonsManagerAdapterDelegate addonsManagerDelegate = null;
    private final java.util.List<mozilla.components.feature.addons.Addon> addons = null;
    private final com.cookiejarapps.android.smartcookieweb.addons.AddonsAdapter.Style style = null;
    private final java.util.List<java.lang.String> excludedAddonIDs = null;
    private final com.cookiejarapps.android.smartcookieweb.preferences.UserPreferences userPreferences = null;
    private final kotlinx.coroutines.CoroutineScope scope = null;
    @org.jetbrains.annotations.NotNull()
    private java.util.Map<java.lang.String, mozilla.components.feature.addons.Addon> addonsMap;
    
    public AddonsAdapter(@org.jetbrains.annotations.NotNull()
    mozilla.components.feature.addons.amo.AddonCollectionProvider addonCollectionProvider, @org.jetbrains.annotations.NotNull()
    mozilla.components.feature.addons.ui.AddonsManagerAdapterDelegate addonsManagerDelegate, @org.jetbrains.annotations.NotNull()
    java.util.List<mozilla.components.feature.addons.Addon> addons, @org.jetbrains.annotations.Nullable()
    com.cookiejarapps.android.smartcookieweb.addons.AddonsAdapter.Style style, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> excludedAddonIDs, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.preferences.UserPreferences userPreferences) {
        super(null);
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.String, mozilla.components.feature.addons.Addon> getAddonsMap$app_debug() {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getAddonsMap$app_debug$annotations() {
    }
    
    public final void setAddonsMap$app_debug(@org.jetbrains.annotations.NotNull()
    java.util.Map<java.lang.String, mozilla.components.feature.addons.Addon> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public mozilla.components.feature.addons.ui.CustomViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    private final mozilla.components.feature.addons.ui.CustomViewHolder createSectionViewHolder(android.view.ViewGroup parent) {
        return null;
    }
    
    private final mozilla.components.feature.addons.ui.CustomViewHolder.AddonViewHolder createAddonViewHolder(android.view.ViewGroup parent) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemViewType(int position) {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    mozilla.components.feature.addons.ui.CustomViewHolder holder, int position) {
    }
    
    public final void bindSection$app_debug(@org.jetbrains.annotations.NotNull()
    mozilla.components.feature.addons.ui.CustomViewHolder.SectionViewHolder holder, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.addons.AddonsAdapter.Section section, int position) {
    }
    
    public final void bindAddon$app_debug(@org.jetbrains.annotations.NotNull()
    mozilla.components.feature.addons.ui.CustomViewHolder.AddonViewHolder holder, @org.jetbrains.annotations.NotNull()
    mozilla.components.feature.addons.Addon addon) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job fetchIcon$app_debug(@org.jetbrains.annotations.NotNull()
    mozilla.components.feature.addons.Addon addon, @org.jetbrains.annotations.NotNull()
    android.widget.ImageView iconView, @org.jetbrains.annotations.NotNull()
    kotlinx.coroutines.CoroutineScope scope) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.Object> createListWithSections$app_debug(@org.jetbrains.annotations.NotNull()
    java.util.List<mozilla.components.feature.addons.Addon> addons) {
        return null;
    }
    
    private final void sort(java.util.ArrayList<mozilla.components.feature.addons.Addon> array, com.cookiejarapps.android.smartcookieweb.preferences.UserPreferences userPreferences) {
    }
    
    public final void sortByAZ(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<mozilla.components.feature.addons.Addon> array) {
    }
    
    public final void reSort() {
    }
    
    public final void updateAddon(@org.jetbrains.annotations.NotNull()
    mozilla.components.feature.addons.Addon addon) {
    }
    
    public final void updateAddons(@org.jetbrains.annotations.NotNull()
    java.util.List<mozilla.components.feature.addons.Addon> addons) {
    }
    
    public final void setWithAnimation$app_debug(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView image, @org.jetbrains.annotations.NotNull()
    android.graphics.Bitmap bitmap, int duration) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0010\u000e\n\u0000\b\u0080\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u00052\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0013"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/addons/AddonsAdapter$Section;", "", "title", "", "visibleDivider", "", "(IZ)V", "getTitle", "()I", "getVisibleDivider", "()Z", "component1", "component2", "copy", "equals", "other", "hashCode", "toString", "", "app_debug"})
    public static final class Section {
        private final int title = 0;
        private final boolean visibleDivider = false;
        
        @org.jetbrains.annotations.NotNull()
        public final com.cookiejarapps.android.smartcookieweb.addons.AddonsAdapter.Section copy(@androidx.annotation.StringRes()
        int title, boolean visibleDivider) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public Section(@androidx.annotation.StringRes()
        int title, boolean visibleDivider) {
            super();
        }
        
        public final int component1() {
            return 0;
        }
        
        public final int getTitle() {
            return 0;
        }
        
        public final boolean component2() {
            return false;
        }
        
        public final boolean getVisibleDivider() {
            return false;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u001d\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001Bc\u0012\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\n\b\u0003\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0003\u0010\f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000fJ\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000fJ\t\u0010\u001f\u001a\u00020\nH\u00c6\u0003J\u0010\u0010 \u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010!\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000fJl\u0010\"\u001a\u00020\u00002\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\t\u001a\u00020\n2\n\b\u0003\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\f\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001\u00a2\u0006\u0002\u0010#J\u0013\u0010$\u001a\u00020\n2\b\u0010%\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010&\u001a\u00020\u0003H\u00d6\u0001J\u0015\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020*H\u0000\u00a2\u0006\u0002\b+J\u0015\u0010,\u001a\u00020(2\u0006\u0010)\u001a\u00020*H\u0000\u00a2\u0006\u0002\b-J\u0015\u0010.\u001a\u00020(2\u0006\u0010/\u001a\u000200H\u0000\u00a2\u0006\u0002\b1J\u0015\u00102\u001a\u00020(2\u0006\u00103\u001a\u000204H\u0000\u00a2\u0006\u0002\b5J\u0015\u00106\u001a\u00020(2\u0006\u0010)\u001a\u00020*H\u0000\u00a2\u0006\u0002\b7J\u0015\u00108\u001a\u00020(2\u0006\u0010)\u001a\u00020*H\u0000\u00a2\u0006\u0002\b9J\t\u0010:\u001a\u00020;H\u00d6\u0001R\u0015\u0010\b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u000e\u0010\u000fR\u0015\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u0011\u0010\u000fR\u0015\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u0012\u0010\u000fR\u0015\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u0013\u0010\u000fR\u0015\u0010\f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u0014\u0010\u000fR\u0015\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u0015\u0010\u000fR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019\u00a8\u0006<"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/addons/AddonsAdapter$Style;", "", "sectionsTextColor", "", "addonNameTextColor", "addonSummaryTextColor", "sectionsTypeFace", "Landroid/graphics/Typeface;", "addonAllowPrivateBrowsingLabelDrawableRes", "visibleDividers", "", "dividerColor", "dividerHeight", "(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/graphics/Typeface;Ljava/lang/Integer;ZLjava/lang/Integer;Ljava/lang/Integer;)V", "getAddonAllowPrivateBrowsingLabelDrawableRes", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getAddonNameTextColor", "getAddonSummaryTextColor", "getDividerColor", "getDividerHeight", "getSectionsTextColor", "getSectionsTypeFace", "()Landroid/graphics/Typeface;", "getVisibleDividers", "()Z", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/graphics/Typeface;Ljava/lang/Integer;ZLjava/lang/Integer;Ljava/lang/Integer;)Lcom/cookiejarapps/android/smartcookieweb/addons/AddonsAdapter$Style;", "equals", "other", "hashCode", "maybeSetAddonNameTextColor", "", "textView", "Landroid/widget/TextView;", "maybeSetAddonNameTextColor$app_debug", "maybeSetAddonSummaryTextColor", "maybeSetAddonSummaryTextColor$app_debug", "maybeSetPrivateBrowsingLabelDrawale", "imageView", "Landroid/widget/ImageView;", "maybeSetPrivateBrowsingLabelDrawale$app_debug", "maybeSetSectionsDividerStyle", "divider", "Landroid/view/View;", "maybeSetSectionsDividerStyle$app_debug", "maybeSetSectionsTextColor", "maybeSetSectionsTextColor$app_debug", "maybeSetSectionsTypeFace", "maybeSetSectionsTypeFace$app_debug", "toString", "", "app_debug"})
    public static final class Style {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.Integer sectionsTextColor = null;
        @org.jetbrains.annotations.Nullable()
        private final java.lang.Integer addonNameTextColor = null;
        @org.jetbrains.annotations.Nullable()
        private final java.lang.Integer addonSummaryTextColor = null;
        @org.jetbrains.annotations.Nullable()
        private final android.graphics.Typeface sectionsTypeFace = null;
        @org.jetbrains.annotations.Nullable()
        private final java.lang.Integer addonAllowPrivateBrowsingLabelDrawableRes = null;
        private final boolean visibleDividers = false;
        @org.jetbrains.annotations.Nullable()
        private final java.lang.Integer dividerColor = null;
        @org.jetbrains.annotations.Nullable()
        private final java.lang.Integer dividerHeight = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.cookiejarapps.android.smartcookieweb.addons.AddonsAdapter.Style copy(@org.jetbrains.annotations.Nullable()
        @androidx.annotation.ColorRes()
        java.lang.Integer sectionsTextColor, @org.jetbrains.annotations.Nullable()
        @androidx.annotation.ColorRes()
        java.lang.Integer addonNameTextColor, @org.jetbrains.annotations.Nullable()
        @androidx.annotation.ColorRes()
        java.lang.Integer addonSummaryTextColor, @org.jetbrains.annotations.Nullable()
        android.graphics.Typeface sectionsTypeFace, @org.jetbrains.annotations.Nullable()
        @androidx.annotation.DrawableRes()
        java.lang.Integer addonAllowPrivateBrowsingLabelDrawableRes, boolean visibleDividers, @org.jetbrains.annotations.Nullable()
        @androidx.annotation.ColorRes()
        java.lang.Integer dividerColor, @org.jetbrains.annotations.Nullable()
        @androidx.annotation.DimenRes()
        java.lang.Integer dividerHeight) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public Style() {
            super();
        }
        
        public Style(@org.jetbrains.annotations.Nullable()
        @androidx.annotation.ColorRes()
        java.lang.Integer sectionsTextColor, @org.jetbrains.annotations.Nullable()
        @androidx.annotation.ColorRes()
        java.lang.Integer addonNameTextColor, @org.jetbrains.annotations.Nullable()
        @androidx.annotation.ColorRes()
        java.lang.Integer addonSummaryTextColor, @org.jetbrains.annotations.Nullable()
        android.graphics.Typeface sectionsTypeFace, @org.jetbrains.annotations.Nullable()
        @androidx.annotation.DrawableRes()
        java.lang.Integer addonAllowPrivateBrowsingLabelDrawableRes, boolean visibleDividers, @org.jetbrains.annotations.Nullable()
        @androidx.annotation.ColorRes()
        java.lang.Integer dividerColor, @org.jetbrains.annotations.Nullable()
        @androidx.annotation.DimenRes()
        java.lang.Integer dividerHeight) {
            super();
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer component1() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getSectionsTextColor() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer component2() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getAddonNameTextColor() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer component3() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getAddonSummaryTextColor() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final android.graphics.Typeface component4() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final android.graphics.Typeface getSectionsTypeFace() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer component5() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getAddonAllowPrivateBrowsingLabelDrawableRes() {
            return null;
        }
        
        public final boolean component6() {
            return false;
        }
        
        public final boolean getVisibleDividers() {
            return false;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer component7() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getDividerColor() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer component8() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getDividerHeight() {
            return null;
        }
        
        public final void maybeSetSectionsTextColor$app_debug(@org.jetbrains.annotations.NotNull()
        android.widget.TextView textView) {
        }
        
        public final void maybeSetSectionsTypeFace$app_debug(@org.jetbrains.annotations.NotNull()
        android.widget.TextView textView) {
        }
        
        public final void maybeSetAddonNameTextColor$app_debug(@org.jetbrains.annotations.NotNull()
        android.widget.TextView textView) {
        }
        
        public final void maybeSetAddonSummaryTextColor$app_debug(@org.jetbrains.annotations.NotNull()
        android.widget.TextView textView) {
        }
        
        public final void maybeSetPrivateBrowsingLabelDrawale$app_debug(@org.jetbrains.annotations.NotNull()
        android.widget.ImageView imageView) {
        }
        
        public final void maybeSetSectionsDividerStyle$app_debug(@org.jetbrains.annotations.NotNull()
        android.view.View divider) {
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u00c0\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0017J\u0018\u0010\b\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016\u00a8\u0006\t"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/addons/AddonsAdapter$DifferCallback;", "Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "", "()V", "areContentsTheSame", "", "oldItem", "newItem", "areItemsTheSame", "app_debug"})
    public static final class DifferCallback extends androidx.recyclerview.widget.DiffUtil.ItemCallback<java.lang.Object> {
        @org.jetbrains.annotations.NotNull()
        public static final com.cookiejarapps.android.smartcookieweb.addons.AddonsAdapter.DifferCallback INSTANCE = null;
        
        private DifferCallback() {
            super();
        }
        
        @java.lang.Override()
        public boolean areItemsTheSame(@org.jetbrains.annotations.NotNull()
        java.lang.Object oldItem, @org.jetbrains.annotations.NotNull()
        java.lang.Object newItem) {
            return false;
        }
        
        @android.annotation.SuppressLint(value = {"DiffUtilEquals"})
        @java.lang.Override()
        public boolean areContentsTheSame(@org.jetbrains.annotations.NotNull()
        java.lang.Object oldItem, @org.jetbrains.annotations.NotNull()
        java.lang.Object newItem) {
            return false;
        }
    }
}