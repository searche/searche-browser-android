package com.cookiejarapps.android.smartcookieweb.search.awesomebar;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B8\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012!\u0010\u0006\u001a\u001d\u0012\u0013\u0012\u00110\b\u00a2\u0006\f\b\t\u0012\b\b\n\u0012\u0004\b\b(\u000b\u0012\u0004\u0012\u00020\f0\u0007\u00a2\u0006\u0002\u0010\rJ\u001f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u00132\u0006\u0010\u0015\u001a\u00020\u000fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u00020\u000fX\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R)\u0010\u0006\u001a\u001d\u0012\u0013\u0012\u00110\b\u00a2\u0006\f\b\t\u0012\b\b\n\u0012\u0004\b\b(\u000b\u0012\u0004\u0012\u00020\f0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0017"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/awesomebar/ShortcutsSuggestionProvider;", "Lmozilla/components/concept/awesomebar/AwesomeBar$SuggestionProvider;", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "context", "Landroid/content/Context;", "selectShortcutEngine", "Lkotlin/Function1;", "Lmozilla/components/browser/state/search/SearchEngine;", "Lkotlin/ParameterName;", "name", "engine", "", "(Lmozilla/components/browser/state/store/BrowserStore;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)V", "id", "", "getId", "()Ljava/lang/String;", "onInputChanged", "", "Lmozilla/components/concept/awesomebar/AwesomeBar$Suggestion;", "text", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class ShortcutsSuggestionProvider implements mozilla.components.concept.awesomebar.AwesomeBar.SuggestionProvider {
    private final mozilla.components.browser.state.store.BrowserStore store = null;
    private final android.content.Context context = null;
    private final kotlin.jvm.functions.Function1<mozilla.components.browser.state.search.SearchEngine, kotlin.Unit> selectShortcutEngine = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String id = null;
    
    public ShortcutsSuggestionProvider(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore store, @org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super mozilla.components.browser.state.search.SearchEngine, kotlin.Unit> selectShortcutEngine) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getId() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object onInputChanged(@org.jetbrains.annotations.NotNull()
    java.lang.String text, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<mozilla.components.concept.awesomebar.AwesomeBar.Suggestion>> continuation) {
        return null;
    }
    
    public void onInputCancelled() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public java.util.List<mozilla.components.concept.awesomebar.AwesomeBar.Suggestion> onInputStarted() {
        return null;
    }
}