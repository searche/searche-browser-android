package com.cookiejarapps.android.smartcookieweb.history;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0016\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u001cB\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0002\u0010\u0006J\n\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016J\u000e\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0013J\b\u0010\u0014\u001a\u00020\u0013H\u0016J\u001c\u0010\u0015\u001a\u00020\u00162\n\u0010\u0017\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u001c\u0010\u0018\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0013H\u0016R \u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\u0006R \u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\n\"\u0004\b\u000e\u0010\u0006R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/history/HistoryItemRecyclerViewAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/cookiejarapps/android/smartcookieweb/history/HistoryItemRecyclerViewAdapter$ViewHolder;", "values", "", "Lmozilla/components/concept/storage/VisitInfo;", "(Ljava/util/List;)V", "filtered", "", "getFiltered", "()Ljava/util/List;", "setFiltered", "oldList", "getOldList", "setOldList", "getFilter", "Landroid/widget/Filter;", "getItem", "position", "", "getItemCount", "onBindViewHolder", "", "holder", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "ViewHolder", "app_debug"})
public class HistoryItemRecyclerViewAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.cookiejarapps.android.smartcookieweb.history.HistoryItemRecyclerViewAdapter.ViewHolder> {
    private java.util.List<mozilla.components.concept.storage.VisitInfo> values;
    public java.util.List<mozilla.components.concept.storage.VisitInfo> filtered;
    public java.util.List<mozilla.components.concept.storage.VisitInfo> oldList;
    
    public HistoryItemRecyclerViewAdapter(@org.jetbrains.annotations.NotNull()
    java.util.List<mozilla.components.concept.storage.VisitInfo> values) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<mozilla.components.concept.storage.VisitInfo> getFiltered() {
        return null;
    }
    
    public final void setFiltered(@org.jetbrains.annotations.NotNull()
    java.util.List<mozilla.components.concept.storage.VisitInfo> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<mozilla.components.concept.storage.VisitInfo> getOldList() {
        return null;
    }
    
    public final void setOldList(@org.jetbrains.annotations.NotNull()
    java.util.List<mozilla.components.concept.storage.VisitInfo> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public android.widget.Filter getFilter() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.concept.storage.VisitInfo getItem(int position) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.cookiejarapps.android.smartcookieweb.history.HistoryItemRecyclerViewAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.history.HistoryItemRecyclerViewAdapter.ViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\bR\u0011\u0010\u000b\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/history/HistoryItemRecyclerViewAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Lcom/cookiejarapps/android/smartcookieweb/history/HistoryItemRecyclerViewAdapter;Landroid/view/View;)V", "timeView", "Landroid/widget/TextView;", "getTimeView", "()Landroid/widget/TextView;", "titleView", "getTitleView", "urlView", "getUrlView", "app_debug"})
    public final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView titleView = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView urlView = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView timeView = null;
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View view) {
            super(null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getTitleView() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getUrlView() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getTimeView() {
            return null;
        }
    }
}