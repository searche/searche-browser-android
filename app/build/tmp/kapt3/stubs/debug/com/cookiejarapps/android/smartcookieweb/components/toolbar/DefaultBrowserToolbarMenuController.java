package com.cookiejarapps.android.smartcookieweb.components.toolbar;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u0014\u001a\u00020\n2\u0006\u0010\u0015\u001a\u00020\u0016H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\u0004\u0018\u00010\u00118BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/DefaultBrowserToolbarMenuController;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarMenuController;", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "activity", "Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;", "navController", "Landroidx/navigation/NavController;", "findInPageLauncher", "Lkotlin/Function0;", "", "browserAnimator", "Lcom/cookiejarapps/android/smartcookieweb/BrowserAnimator;", "customTabSessionId", "", "(Lmozilla/components/browser/state/store/BrowserStore;Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;Landroidx/navigation/NavController;Lkotlin/jvm/functions/Function0;Lcom/cookiejarapps/android/smartcookieweb/BrowserAnimator;Ljava/lang/String;)V", "currentSession", "Lmozilla/components/browser/state/state/SessionState;", "getCurrentSession", "()Lmozilla/components/browser/state/state/SessionState;", "handleToolbarItemInteraction", "item", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "app_debug"})
public final class DefaultBrowserToolbarMenuController implements com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarMenuController {
    private final mozilla.components.browser.state.store.BrowserStore store = null;
    private final com.cookiejarapps.android.smartcookieweb.BrowserActivity activity = null;
    private final androidx.navigation.NavController navController = null;
    private final kotlin.jvm.functions.Function0<kotlin.Unit> findInPageLauncher = null;
    private final com.cookiejarapps.android.smartcookieweb.BrowserAnimator browserAnimator = null;
    private final java.lang.String customTabSessionId = null;
    
    public DefaultBrowserToolbarMenuController(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore store, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.BrowserActivity activity, @org.jetbrains.annotations.NotNull()
    androidx.navigation.NavController navController, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> findInPageLauncher, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.BrowserAnimator browserAnimator, @org.jetbrains.annotations.Nullable()
    java.lang.String customTabSessionId) {
        super();
    }
    
    private final mozilla.components.browser.state.state.SessionState getCurrentSession() {
        return null;
    }
    
    @kotlin.Suppress(names = {"ComplexMethod", "LongMethod"})
    @java.lang.Override()
    public void handleToolbarItemInteraction(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item item) {
    }
}