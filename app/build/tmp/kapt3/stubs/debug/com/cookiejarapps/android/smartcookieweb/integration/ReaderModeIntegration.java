package com.cookiejarapps.android.smartcookieweb.integration;

import java.lang.System;

@kotlin.Suppress(names = {"UndocumentedPublicClass"})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B5\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\b\u0010\u0016\u001a\u00020\u0015H\u0016J\b\u0010\u0017\u001a\u00020\u0018H\u0016J\b\u0010\u0019\u001a\u00020\u0018H\u0016R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/integration/ReaderModeIntegration;", "Lmozilla/components/support/base/feature/LifecycleAwareFeature;", "Lmozilla/components/support/base/feature/UserInteractionHandler;", "context", "Landroid/content/Context;", "engine", "Lmozilla/components/concept/engine/Engine;", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "toolbar", "Lmozilla/components/browser/toolbar/BrowserToolbar;", "view", "Lmozilla/components/feature/readerview/view/ReaderViewControlsView;", "readerViewAppearanceButton", "Lcom/google/android/material/floatingactionbutton/FloatingActionButton;", "(Landroid/content/Context;Lmozilla/components/concept/engine/Engine;Lmozilla/components/browser/state/store/BrowserStore;Lmozilla/components/browser/toolbar/BrowserToolbar;Lmozilla/components/feature/readerview/view/ReaderViewControlsView;Lcom/google/android/material/floatingactionbutton/FloatingActionButton;)V", "feature", "Lmozilla/components/feature/readerview/ReaderViewFeature;", "readerViewButton", "Lmozilla/components/browser/toolbar/BrowserToolbar$ToggleButton;", "readerViewButtonVisible", "", "onBackPressed", "start", "", "stop", "app_debug"})
public final class ReaderModeIntegration implements mozilla.components.support.base.feature.LifecycleAwareFeature, mozilla.components.support.base.feature.UserInteractionHandler {
    private boolean readerViewButtonVisible = false;
    private final mozilla.components.browser.toolbar.BrowserToolbar.ToggleButton readerViewButton = null;
    private final mozilla.components.feature.readerview.ReaderViewFeature feature = null;
    
    public ReaderModeIntegration(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.Engine engine, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore store, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.toolbar.BrowserToolbar toolbar, @org.jetbrains.annotations.NotNull()
    mozilla.components.feature.readerview.view.ReaderViewControlsView view, @org.jetbrains.annotations.NotNull()
    com.google.android.material.floatingactionbutton.FloatingActionButton readerViewAppearanceButton) {
        super();
    }
    
    @java.lang.Override()
    public void start() {
    }
    
    @java.lang.Override()
    public void stop() {
    }
    
    @java.lang.Override()
    public boolean onBackPressed() {
        return false;
    }
    
    public boolean onHomePressed() {
        return false;
    }
}