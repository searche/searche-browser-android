package com.cookiejarapps.android.smartcookieweb.browser.bookmark;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J3\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\b2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0096@\u00f8\u0001\u0000\u00f8\u0001\u0000\u00f8\u0001\u0001\u00a2\u0006\u0004\b\r\u0010\u000eJ;\u0010\u000f\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u0010\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\b2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0096@\u00f8\u0001\u0000\u00f8\u0001\u0000\u00f8\u0001\u0001\u00a2\u0006\u0004\b\u0011\u0010\u0012J+\u0010\u0013\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\b2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0096@\u00f8\u0001\u0000\u00f8\u0001\u0000\u00f8\u0001\u0001\u00a2\u0006\u0004\b\u0014\u0010\u0015J\b\u0010\u0016\u001a\u00020\u0017H\u0016J\u0019\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\bH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001bJ\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\u0006\u0010\u001a\u001a\u00020\bH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001bJ\u001f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001f2\u0006\u0010\u0010\u001a\u00020\bH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001bJ1\u0010 \u001a\b\u0012\u0004\u0012\u00020\u001d0\u001f2\u0006\u0010!\u001a\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010$2\u0006\u0010%\u001a\u00020$H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010&J#\u0010\'\u001a\u0004\u0018\u00010\u001d2\u0006\u0010\u001a\u001a\u00020\b2\u0006\u0010(\u001a\u00020\u0019H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010)J\u0011\u0010*\u001a\u00020\u0017H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010+J\'\u0010,\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001f2\u0006\u0010-\u001a\u00020\b2\u0006\u0010!\u001a\u00020\"H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010.J!\u0010/\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\b2\u0006\u00100\u001a\u000201H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00102J\u0011\u00103\u001a\u00020\u0017H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010+R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u000b\n\u0002\b\u0019\n\u0005\b\u00a1\u001e0\u0001\u00a8\u00064"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/CustomBookmarksStorage;", "Lmozilla/components/concept/storage/BookmarksStorage;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "manager", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/repository/BookmarkManager;", "addFolder", "", "parentGuid", "title", "position", "Lkotlin/UInt;", "addFolder-HqaIMu8", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/UInt;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addItem", "url", "addItem-iC4mN9g", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/UInt;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addSeparator", "addSeparator-SLwFa_Y", "(Ljava/lang/String;Lkotlin/UInt;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "cleanup", "", "deleteNode", "", "guid", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getBookmark", "Lmozilla/components/concept/storage/BookmarkNode;", "getBookmarksWithUrl", "", "getRecentBookmarks", "limit", "", "maxAge", "", "currentTime", "(ILjava/lang/Long;JLkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getTree", "recursive", "(Ljava/lang/String;ZLkotlin/coroutines/Continuation;)Ljava/lang/Object;", "runMaintenance", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "searchBookmarks", "query", "(Ljava/lang/String;ILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "updateNode", "info", "Lmozilla/components/concept/storage/BookmarkInfo;", "(Ljava/lang/String;Lmozilla/components/concept/storage/BookmarkInfo;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "warmUp", "app_debug"})
public final class CustomBookmarksStorage implements mozilla.components.concept.storage.BookmarksStorage {
    private final com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository.BookmarkManager manager = null;
    
    public CustomBookmarksStorage(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
    
    @java.lang.Override()
    public void cleanup() {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object deleteNode(@org.jetbrains.annotations.NotNull()
    java.lang.String guid, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getBookmark(@org.jetbrains.annotations.NotNull()
    java.lang.String guid, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super mozilla.components.concept.storage.BookmarkNode> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getBookmarksWithUrl(@org.jetbrains.annotations.NotNull()
    java.lang.String url, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<mozilla.components.concept.storage.BookmarkNode>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getRecentBookmarks(int limit, @org.jetbrains.annotations.Nullable()
    java.lang.Long maxAge, long currentTime, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<mozilla.components.concept.storage.BookmarkNode>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getTree(@org.jetbrains.annotations.NotNull()
    java.lang.String guid, boolean recursive, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super mozilla.components.concept.storage.BookmarkNode> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object runMaintenance(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object searchBookmarks(@org.jetbrains.annotations.NotNull()
    java.lang.String query, int limit, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<mozilla.components.concept.storage.BookmarkNode>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object updateNode(@org.jetbrains.annotations.NotNull()
    java.lang.String guid, @org.jetbrains.annotations.NotNull()
    mozilla.components.concept.storage.BookmarkInfo info, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object warmUp(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
}