package com.cookiejarapps.android.smartcookieweb.browser.bookmark.items;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b&\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB\u0017\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001a\u0010\u0011\u001a\u0004\u0018\u00010\u00002\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0004J\u001a\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0012\u001a\u00020\u0013H$J\u000e\u0010\u0019\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u001bJ\u0010\u0010\u001c\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u001bH$R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u0012\u0010\r\u001a\u00020\u000eX\u00a4\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001e"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkItem;", "Ljava/io/Serializable;", "title", "", "id", "", "(Ljava/lang/String;J)V", "getId", "()J", "getTitle", "()Ljava/lang/String;", "setTitle", "(Ljava/lang/String;)V", "type", "", "getType", "()I", "read", "reader", "Lcom/squareup/moshi/JsonReader;", "parent", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;", "readMain", "", "name", "write", "writer", "Lcom/squareup/moshi/JsonWriter;", "writeMain", "Companion", "app_debug"})
public abstract class BookmarkItem implements java.io.Serializable {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String title;
    private final long id = 0L;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    protected static final java.lang.String COLUMN_NAME_TYPE = "0";
    @org.jetbrains.annotations.NotNull()
    protected static final java.lang.String COLUMN_NAME_TITLE = "1";
    @org.jetbrains.annotations.NotNull()
    protected static final java.lang.String COLUMN_NAME_ID = "3";
    
    public BookmarkItem(@org.jetbrains.annotations.Nullable()
    java.lang.String title, long id) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTitle() {
        return null;
    }
    
    public final void setTitle(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public final long getId() {
        return 0L;
    }
    
    protected abstract int getType();
    
    @kotlin.jvm.Throws(exceptionClasses = {java.io.IOException.class})
    public final boolean write(@org.jetbrains.annotations.NotNull()
    com.squareup.moshi.JsonWriter writer) throws java.io.IOException {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    @kotlin.jvm.Throws(exceptionClasses = {java.io.IOException.class})
    protected final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem read(@org.jetbrains.annotations.NotNull()
    com.squareup.moshi.JsonReader reader, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem parent) throws java.io.IOException {
        return null;
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.io.IOException.class})
    protected abstract boolean writeMain(@org.jetbrains.annotations.NotNull()
    com.squareup.moshi.JsonWriter writer) throws java.io.IOException;
    
    @kotlin.jvm.Throws(exceptionClasses = {java.io.IOException.class})
    protected abstract boolean readMain(@org.jetbrains.annotations.Nullable()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    com.squareup.moshi.JsonReader reader) throws java.io.IOException;
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0084T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0084T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0084T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkItem$Companion;", "", "()V", "COLUMN_NAME_ID", "", "COLUMN_NAME_TITLE", "COLUMN_NAME_TYPE", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}