package com.cookiejarapps.android.smartcookieweb.search;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\u001a\"\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u001a\u0018\u0010\u0007\u001a\u00020\u00012\u0006\u0010\b\u001a\u00020\u00012\u0006\u0010\t\u001a\u00020\nH\u0002\u00a8\u0006\u000b"}, d2 = {"createInitialSearchFragmentState", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentState;", "components", "Lcom/cookiejarapps/android/smartcookieweb/components/Components;", "tabId", "", "pastedText", "searchStateReducer", "state", "action", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentAction;", "app_debug"})
public final class SearchFragmentStoreKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.search.SearchFragmentState createInitialSearchFragmentState(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.components.Components components, @org.jetbrains.annotations.Nullable()
    java.lang.String tabId, @org.jetbrains.annotations.Nullable()
    java.lang.String pastedText) {
        return null;
    }
    
    /**
     * The SearchState Reducer.
     */
    private static final com.cookiejarapps.android.smartcookieweb.search.SearchFragmentState searchStateReducer(com.cookiejarapps.android.smartcookieweb.search.SearchFragmentState state, com.cookiejarapps.android.smartcookieweb.search.SearchFragmentAction action) {
        return null;
    }
}