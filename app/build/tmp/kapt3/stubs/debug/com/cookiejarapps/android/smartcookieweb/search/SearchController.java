package com.cookiejarapps.android.smartcookieweb.search;

import java.lang.System;

/**
 * An interface that handles the view manipulation of the Search, triggered by the Interactor
 */
@kotlin.Suppress(names = {"TooManyFunctions"})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\nH&J\b\u0010\u000b\u001a\u00020\u0003H&J\u0010\u0010\f\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u0007H&J\u0010\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u0007H&J\u0010\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0007H&J\u0010\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0007H&\u00a8\u0006\u0013"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/SearchController;", "", "handleClickSearchEngineSettings", "", "handleEditingCancelled", "handleExistingSessionSelected", "tabId", "", "handleSearchShortcutEngineSelected", "searchEngine", "Lmozilla/components/browser/state/search/SearchEngine;", "handleSearchShortcutsButtonClicked", "handleSearchTermsTapped", "searchTerms", "handleTextChanged", "text", "handleUrlCommitted", "url", "handleUrlTapped", "app_debug"})
public abstract interface SearchController {
    
    public abstract void handleUrlCommitted(@org.jetbrains.annotations.NotNull()
    java.lang.String url);
    
    public abstract void handleEditingCancelled();
    
    public abstract void handleTextChanged(@org.jetbrains.annotations.NotNull()
    java.lang.String text);
    
    public abstract void handleUrlTapped(@org.jetbrains.annotations.NotNull()
    java.lang.String url);
    
    public abstract void handleSearchTermsTapped(@org.jetbrains.annotations.NotNull()
    java.lang.String searchTerms);
    
    public abstract void handleSearchShortcutEngineSelected(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.search.SearchEngine searchEngine);
    
    public abstract void handleClickSearchEngineSettings();
    
    public abstract void handleExistingSessionSelected(@org.jetbrains.annotations.NotNull()
    java.lang.String tabId);
    
    public abstract void handleSearchShortcutsButtonClicked();
}