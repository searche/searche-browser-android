package com.cookiejarapps.android.smartcookieweb.browser.home;

import java.lang.System;

@kotlinx.coroutines.ExperimentalCoroutinesApi()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u009c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u0000 K2\u00020\u0001:\u0001KB\u0005\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'2\f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u001a0)H\u0002J\u0010\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-H\u0002J\u0018\u0010.\u001a\u00020+2\u0006\u0010/\u001a\u0002002\u0006\u00101\u001a\u000202H\u0002J\b\u00103\u001a\u00020+H\u0002J\b\u00104\u001a\u00020+H\u0002J\u0010\u00105\u001a\u00020+2\u0006\u00106\u001a\u000207H\u0016J\u0012\u00108\u001a\u00020+2\b\u00109\u001a\u0004\u0018\u00010\u0015H\u0016J&\u0010:\u001a\u0004\u0018\u00010-2\u0006\u0010;\u001a\u00020<2\b\u0010=\u001a\u0004\u0018\u00010>2\b\u00109\u001a\u0004\u0018\u00010\u0015H\u0016J\b\u0010?\u001a\u00020+H\u0016J\b\u0010@\u001a\u00020+H\u0016J\u001a\u0010A\u001a\u00020+2\u0006\u0010,\u001a\u00020-2\b\u00109\u001a\u0004\u0018\u00010\u0015H\u0016J\b\u0010B\u001a\u00020+H\u0002J\u0010\u0010C\u001a\u00020+2\u0006\u00101\u001a\u000202H\u0002J\u0018\u0010D\u001a\u00020+2\u0006\u0010E\u001a\u00020F2\u0006\u00101\u001a\u000202H\u0002J\u0010\u0010G\u001a\u00020+2\u0006\u0010,\u001a\u00020-H\u0002J\u0010\u0010H\u001a\u00020+2\u0006\u0010I\u001a\u00020JH\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0007\u001a\u00020\b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\nR\u0014\u0010\r\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\u00020\u00118BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R,\u0010\u0018\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001a0\u00198\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\b\u001b\u0010\u0002\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u0014\u0010 \u001a\u00020!8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\"\u0010#\u00a8\u0006L"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/cookiejarapps/android/smartcookieweb/databinding/FragmentHomeBinding;", "appBarLayout", "Lcom/google/android/material/appbar/AppBarLayout;", "args", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeFragmentArgs;", "getArgs", "()Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeFragmentArgs;", "args$delegate", "Landroidx/navigation/NavArgsLazy;", "binding", "getBinding", "()Lcom/cookiejarapps/android/smartcookieweb/databinding/FragmentHomeBinding;", "browsingModeManager", "Lcom/cookiejarapps/android/smartcookieweb/browser/BrowsingModeManager;", "getBrowsingModeManager", "()Lcom/cookiejarapps/android/smartcookieweb/browser/BrowsingModeManager;", "bundleArgs", "Landroid/os/Bundle;", "database", "Lcom/cookiejarapps/android/smartcookieweb/browser/shortcuts/ShortcutDatabase;", "getMenuButton", "Lkotlin/Function0;", "Lmozilla/components/browser/menu/view/MenuButton;", "getGetMenuButton$app_debug$annotations", "getGetMenuButton$app_debug", "()Lkotlin/jvm/functions/Function0;", "setGetMenuButton$app_debug", "(Lkotlin/jvm/functions/Function0;)V", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "getStore", "()Lmozilla/components/browser/state/store/BrowserStore;", "createHomeMenu", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu;", "context", "Landroid/content/Context;", "menuButtonView", "Ljava/lang/ref/WeakReference;", "createTabCounterMenu", "", "view", "Landroid/view/View;", "deleteShortcut", "shortcutEntity", "Lcom/cookiejarapps/android/smartcookieweb/browser/shortcuts/ShortcutEntity;", "adapter", "Lcom/cookiejarapps/android/smartcookieweb/browser/shortcuts/ShortcutGridAdapter;", "navigateToSearch", "observeSearchEngineChanges", "onConfigurationChanged", "newConfig", "Landroid/content/res/Configuration;", "onCreate", "savedInstanceState", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDestroyView", "onResume", "onViewCreated", "openTabDrawer", "showCreateShortcutDialog", "showEditShortcutDialog", "position", "", "updateLayout", "updateTabCounter", "browserState", "Lmozilla/components/browser/state/state/BrowserState;", "Companion", "app_debug"})
public final class HomeFragment extends androidx.fragment.app.Fragment {
    private com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutDatabase database;
    private final androidx.navigation.NavArgsLazy args$delegate = null;
    private android.os.Bundle bundleArgs;
    private com.google.android.material.appbar.AppBarLayout appBarLayout;
    private com.cookiejarapps.android.smartcookieweb.databinding.FragmentHomeBinding _binding;
    @org.jetbrains.annotations.NotNull()
    private kotlin.jvm.functions.Function0<mozilla.components.browser.menu.view.MenuButton> getMenuButton;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.browser.home.HomeFragment.Companion Companion = null;
    private static final java.lang.String FOCUS_ON_ADDRESS_BAR = "focusOnAddressBar";
    
    public HomeFragment() {
        super();
    }
    
    private final com.cookiejarapps.android.smartcookieweb.browser.home.HomeFragmentArgs getArgs() {
        return null;
    }
    
    private final com.cookiejarapps.android.smartcookieweb.browser.BrowsingModeManager getBrowsingModeManager() {
        return null;
    }
    
    private final mozilla.components.browser.state.store.BrowserStore getStore() {
        return null;
    }
    
    private final com.cookiejarapps.android.smartcookieweb.databinding.FragmentHomeBinding getBinding() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.jvm.functions.Function0<mozilla.components.browser.menu.view.MenuButton> getGetMenuButton$app_debug() {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getGetMenuButton$app_debug$annotations() {
    }
    
    public final void setGetMenuButton$app_debug(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<mozilla.components.browser.menu.view.MenuButton> p0) {
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @kotlin.Suppress(names = {"LongMethod"})
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onConfigurationChanged(@org.jetbrains.annotations.NotNull()
    android.content.res.Configuration newConfig) {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    private final void showEditShortcutDialog(int position, com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutGridAdapter adapter) {
    }
    
    private final void showCreateShortcutDialog(com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutGridAdapter adapter) {
    }
    
    private final void deleteShortcut(com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutEntity shortcutEntity, com.cookiejarapps.android.smartcookieweb.browser.shortcuts.ShortcutGridAdapter adapter) {
    }
    
    private final void updateLayout(android.view.View view) {
    }
    
    @kotlin.Suppress(names = {"LongMethod", "ComplexMethod"})
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void observeSearchEngineChanges() {
    }
    
    private final void createTabCounterMenu(android.view.View view) {
    }
    
    @java.lang.Override()
    public void onDestroyView() {
    }
    
    private final void navigateToSearch() {
    }
    
    @java.lang.SuppressWarnings(value = {"ComplexMethod", "LongMethod"})
    private final com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu createHomeMenu(android.content.Context context, java.lang.ref.WeakReference<mozilla.components.browser.menu.view.MenuButton> menuButtonView) {
        return null;
    }
    
    private final void openTabDrawer() {
    }
    
    private final void updateTabCounter(mozilla.components.browser.state.state.BrowserState browserState) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeFragment$Companion;", "", "()V", "FOCUS_ON_ADDRESS_BAR", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}