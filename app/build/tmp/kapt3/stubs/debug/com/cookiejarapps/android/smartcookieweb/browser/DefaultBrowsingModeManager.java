package com.cookiejarapps.android.smartcookieweb.browser;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\b0\u0007\u00a2\u0006\u0002\u0010\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010\u000b\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u00038V@VX\u0096\u000e\u00a2\u0006\f\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\b0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/DefaultBrowsingModeManager;", "Lcom/cookiejarapps/android/smartcookieweb/browser/BrowsingModeManager;", "_mode", "Lcom/cookiejarapps/android/smartcookieweb/browser/BrowsingMode;", "userPreferences", "Lcom/cookiejarapps/android/smartcookieweb/preferences/UserPreferences;", "modeDidChange", "Lkotlin/Function1;", "", "(Lcom/cookiejarapps/android/smartcookieweb/browser/BrowsingMode;Lcom/cookiejarapps/android/smartcookieweb/preferences/UserPreferences;Lkotlin/jvm/functions/Function1;)V", "value", "mode", "getMode", "()Lcom/cookiejarapps/android/smartcookieweb/browser/BrowsingMode;", "setMode", "(Lcom/cookiejarapps/android/smartcookieweb/browser/BrowsingMode;)V", "app_debug"})
public final class DefaultBrowsingModeManager implements com.cookiejarapps.android.smartcookieweb.browser.BrowsingModeManager {
    private com.cookiejarapps.android.smartcookieweb.browser.BrowsingMode _mode;
    private final com.cookiejarapps.android.smartcookieweb.preferences.UserPreferences userPreferences = null;
    private final kotlin.jvm.functions.Function1<com.cookiejarapps.android.smartcookieweb.browser.BrowsingMode, kotlin.Unit> modeDidChange = null;
    
    public DefaultBrowsingModeManager(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.BrowsingMode _mode, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.preferences.UserPreferences userPreferences, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.cookiejarapps.android.smartcookieweb.browser.BrowsingMode, kotlin.Unit> modeDidChange) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.cookiejarapps.android.smartcookieweb.browser.BrowsingMode getMode() {
        return null;
    }
    
    @java.lang.Override()
    public void setMode(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.BrowsingMode value) {
    }
}