package com.cookiejarapps.android.smartcookieweb.addons;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0088\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 @2\u00020\u00012\u00020\u00022\u00020\u0003:\u0001@B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u001d\u001a\u00020\u001eH\u0002J\u0014\u0010\u001f\u001a\u00020\u001e2\n\b\u0002\u0010 \u001a\u0004\u0018\u00010\rH\u0004J\b\u0010!\u001a\u00020\u000bH\u0016J&\u0010\"\u001a\u0004\u0018\u00010#2\u0006\u0010$\u001a\u00020%2\b\u0010&\u001a\u0004\u0018\u00010\'2\b\u0010(\u001a\u0004\u0018\u00010)H\u0016J\b\u0010*\u001a\u00020\u001eH\u0016J!\u0010+\u001a\u00020\u001e2\b\u0010\n\u001a\u0004\u0018\u00010\u000b2\b\u0010,\u001a\u0004\u0018\u00010\u000bH\u0016\u00a2\u0006\u0002\u0010-J\u0010\u0010.\u001a\u00020\u001e2\u0006\u0010/\u001a\u000200H\u0016J+\u00101\u001a\u00020\u001e2\u0006\u00102\u001a\u0002032\f\u00104\u001a\b\u0012\u0004\u0012\u00020\u001c052\u0006\u00106\u001a\u000207H\u0016\u00a2\u0006\u0002\u00108J\b\u00109\u001a\u00020\u001eH\u0016J\b\u0010:\u001a\u00020\u001eH\u0016J\u001a\u0010;\u001a\u00020\u001e2\u0006\u0010<\u001a\u00020#2\b\u0010(\u001a\u0004\u0018\u00010)H\u0016J\u0010\u0010=\u001a\u00020\u001e2\u0006\u0010>\u001a\u00020?H\u0016R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\u00068BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\f\u001a\u0004\u0018\u00010\rX\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006A"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/addons/WebExtensionPopupFragment;", "Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;", "Lmozilla/components/support/base/feature/UserInteractionHandler;", "Lmozilla/components/concept/engine/EngineSession$Observer;", "()V", "_binding", "Lcom/cookiejarapps/android/smartcookieweb/databinding/FragmentExtensionPopupBinding;", "binding", "getBinding", "()Lcom/cookiejarapps/android/smartcookieweb/databinding/FragmentExtensionPopupBinding;", "canGoBack", "", "engineSession", "Lmozilla/components/concept/engine/EngineSession;", "getEngineSession", "()Lmozilla/components/concept/engine/EngineSession;", "setEngineSession", "(Lmozilla/components/concept/engine/EngineSession;)V", "promptsFeature", "Lmozilla/components/support/base/feature/ViewBoundFeatureWrapper;", "Lmozilla/components/feature/prompts/PromptFeature;", "session", "Lmozilla/components/browser/state/state/SessionState;", "getSession", "()Lmozilla/components/browser/state/state/SessionState;", "setSession", "(Lmozilla/components/browser/state/state/SessionState;)V", "webExtensionId", "", "consumePopupSession", "", "initializeSession", "fromEngineSession", "onBackPressed", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDestroyView", "onNavigationStateChange", "canGoForward", "(Ljava/lang/Boolean;Ljava/lang/Boolean;)V", "onPromptRequest", "promptRequest", "Lmozilla/components/concept/engine/prompt/PromptRequest;", "onRequestPermissionsResult", "requestCode", "", "permissions", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onStart", "onStop", "onViewCreated", "view", "onWindowRequest", "windowRequest", "Lmozilla/components/concept/engine/window/WindowRequest;", "Companion", "app_debug"})
public final class WebExtensionPopupFragment extends com.google.android.material.bottomsheet.BottomSheetDialogFragment implements mozilla.components.support.base.feature.UserInteractionHandler, mozilla.components.concept.engine.EngineSession.Observer {
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.feature.prompts.PromptFeature> promptsFeature = null;
    @org.jetbrains.annotations.Nullable()
    private mozilla.components.browser.state.state.SessionState session;
    @org.jetbrains.annotations.Nullable()
    private mozilla.components.concept.engine.EngineSession engineSession;
    private boolean canGoBack = false;
    private java.lang.String webExtensionId;
    private com.cookiejarapps.android.smartcookieweb.databinding.FragmentExtensionPopupBinding _binding;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.addons.WebExtensionPopupFragment.Companion Companion = null;
    private static final int REQUEST_CODE_PROMPT_PERMISSIONS = 1;
    
    public WebExtensionPopupFragment() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final mozilla.components.browser.state.state.SessionState getSession() {
        return null;
    }
    
    protected final void setSession(@org.jetbrains.annotations.Nullable()
    mozilla.components.browser.state.state.SessionState p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final mozilla.components.concept.engine.EngineSession getEngineSession() {
        return null;
    }
    
    protected final void setEngineSession(@org.jetbrains.annotations.Nullable()
    mozilla.components.concept.engine.EngineSession p0) {
    }
    
    private final com.cookiejarapps.android.smartcookieweb.databinding.FragmentExtensionPopupBinding getBinding() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onDestroyView() {
    }
    
    @java.lang.Override()
    public void onStart() {
    }
    
    @java.lang.Override()
    public void onStop() {
    }
    
    protected final void initializeSession(@org.jetbrains.annotations.Nullable()
    mozilla.components.concept.engine.EngineSession fromEngineSession) {
    }
    
    @java.lang.Override()
    public void onWindowRequest(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.window.WindowRequest windowRequest) {
    }
    
    private final void consumePopupSession() {
    }
    
    @java.lang.Override()
    public boolean onBackPressed() {
        return false;
    }
    
    @java.lang.Override()
    public void onPromptRequest(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.prompt.PromptRequest promptRequest) {
    }
    
    @java.lang.Override()
    public void onNavigationStateChange(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean canGoBack, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean canGoForward) {
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    public void onAppPermissionRequest(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.permission.PermissionRequest permissionRequest) {
    }
    
    public void onBeforeUnloadPromptDenied() {
    }
    
    public void onCancelContentPermissionRequest(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.permission.PermissionRequest permissionRequest) {
    }
    
    public void onContentPermissionRequest(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.permission.PermissionRequest permissionRequest) {
    }
    
    public void onCrash() {
    }
    
    public void onDesktopModeChange(boolean enabled) {
    }
    
    public void onExcludedOnTrackingProtectionChange(boolean excluded) {
    }
    
    public void onExternalResource(@org.jetbrains.annotations.NotNull()
    java.lang.String url, @org.jetbrains.annotations.Nullable()
    java.lang.String fileName, @org.jetbrains.annotations.Nullable()
    java.lang.Long contentLength, @org.jetbrains.annotations.Nullable()
    java.lang.String contentType, @org.jetbrains.annotations.Nullable()
    java.lang.String cookie, @org.jetbrains.annotations.Nullable()
    java.lang.String userAgent, boolean isPrivate, @org.jetbrains.annotations.Nullable()
    mozilla.components.concept.fetch.Response response) {
    }
    
    public void onFind(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    public void onFindResult(int activeMatchOrdinal, int numberOfMatches, boolean isDoneCounting) {
    }
    
    public void onFirstContentfulPaint() {
    }
    
    public void onFullScreenChange(boolean enabled) {
    }
    
    public void onHistoryStateChanged(@org.jetbrains.annotations.NotNull()
    java.util.List<mozilla.components.concept.engine.history.HistoryItem> historyList, int currentIndex) {
    }
    
    public boolean onHomePressed() {
        return false;
    }
    
    public void onLaunchIntentRequest(@org.jetbrains.annotations.NotNull()
    java.lang.String url, @org.jetbrains.annotations.Nullable()
    android.content.Intent appIntent) {
    }
    
    public void onLoadRequest(@org.jetbrains.annotations.NotNull()
    java.lang.String url, boolean triggeredByRedirect, boolean triggeredByWebContent) {
    }
    
    public void onLoadingStateChange(boolean loading) {
    }
    
    public void onLocationChange(@org.jetbrains.annotations.NotNull()
    java.lang.String url) {
    }
    
    public void onLongPress(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.HitResult hitResult) {
    }
    
    public void onMediaActivated(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.mediasession.MediaSession.Controller mediaSessionController) {
    }
    
    public void onMediaDeactivated() {
    }
    
    public void onMediaFeatureChanged(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.mediasession.MediaSession.Feature features) {
    }
    
    public void onMediaFullscreenChanged(boolean fullscreen, @org.jetbrains.annotations.Nullable()
    mozilla.components.concept.engine.mediasession.MediaSession.ElementMetadata elementMetadata) {
    }
    
    public void onMediaMetadataChanged(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.mediasession.MediaSession.Metadata metadata) {
    }
    
    public void onMediaMuteChanged(boolean muted) {
    }
    
    public void onMediaPlaybackStateChanged(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.mediasession.MediaSession.PlaybackState playbackState) {
    }
    
    public void onMediaPositionStateChanged(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.mediasession.MediaSession.PositionState positionState) {
    }
    
    public void onMetaViewportFitChanged(int layoutInDisplayCutoutMode) {
    }
    
    public void onNavigateBack() {
    }
    
    public void onPaintStatusReset() {
    }
    
    public void onPreviewImageChange(@org.jetbrains.annotations.NotNull()
    java.lang.String previewImageUrl) {
    }
    
    public void onProcessKilled() {
    }
    
    public void onProgress(int progress) {
    }
    
    public void onPromptDismissed(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.prompt.PromptRequest promptRequest) {
    }
    
    public void onRecordingStateChanged(@org.jetbrains.annotations.NotNull()
    java.util.List<mozilla.components.concept.engine.media.RecordingDevice> devices) {
    }
    
    public void onRepostPromptCancelled() {
    }
    
    public void onSecurityChange(boolean secure, @org.jetbrains.annotations.Nullable()
    java.lang.String host, @org.jetbrains.annotations.Nullable()
    java.lang.String issuer) {
    }
    
    public void onShowDynamicToolbar() {
    }
    
    public void onStateUpdated(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.EngineSessionState state) {
    }
    
    public void onThumbnailChange(@org.jetbrains.annotations.Nullable()
    android.graphics.Bitmap bitmap) {
    }
    
    public void onTitleChange(@org.jetbrains.annotations.NotNull()
    java.lang.String title) {
    }
    
    public void onTrackerBlocked(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.content.blocking.Tracker tracker) {
    }
    
    public void onTrackerBlockingEnabledChange(boolean enabled) {
    }
    
    public void onTrackerLoaded(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.content.blocking.Tracker tracker) {
    }
    
    public void onWebAppManifestLoaded(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.manifest.WebAppManifest manifest) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/addons/WebExtensionPopupFragment$Companion;", "", "()V", "REQUEST_CODE_PROMPT_PERMISSIONS", "", "create", "Lcom/cookiejarapps/android/smartcookieweb/addons/WebExtensionPopupFragment;", "webExtensionId", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.cookiejarapps.android.smartcookieweb.addons.WebExtensionPopupFragment create(@org.jetbrains.annotations.NotNull()
        java.lang.String webExtensionId) {
            return null;
        }
    }
}