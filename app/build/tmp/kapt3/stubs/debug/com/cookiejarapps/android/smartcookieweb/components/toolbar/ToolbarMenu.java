package com.cookiejarapps.android.smartcookieweb.components.toolbar;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001:\u0001\nR\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\t\u00a8\u0006\u000b"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu;", "", "menuBuilder", "Lmozilla/components/browser/menu/BrowserMenuBuilder;", "getMenuBuilder", "()Lmozilla/components/browser/menu/BrowserMenuBuilder;", "menuToolbar", "Lmozilla/components/browser/menu/item/BrowserMenuItemToolbar;", "getMenuToolbar", "()Lmozilla/components/browser/menu/item/BrowserMenuItemToolbar;", "Item", "app_debug"})
public abstract interface ToolbarMenu {
    
    @org.jetbrains.annotations.NotNull()
    public abstract mozilla.components.browser.menu.BrowserMenuBuilder getMenuBuilder();
    
    @org.jetbrains.annotations.NotNull()
    public abstract mozilla.components.browser.menu.item.BrowserMenuItemToolbar getMenuToolbar();
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u000f\u0003\u0004\u0005\u0006\u0007\b\t\n\u000b\f\r\u000e\u000f\u0010\u0011B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u000f\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f \u00a8\u0006!"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "", "()V", "AddToHomeScreen", "Back", "Bookmarks", "FindInPage", "Forward", "History", "NewPrivateTab", "NewTab", "OpenInApp", "Print", "Reload", "RequestDesktop", "Settings", "Share", "Stop", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Settings;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$RequestDesktop;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$FindInPage;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Share;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Back;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Forward;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Reload;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Stop;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$AddToHomeScreen;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$NewTab;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$NewPrivateTab;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$OpenInApp;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Bookmarks;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$History;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Print;", "app_debug"})
    public static abstract class Item {
        
        private Item() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Settings;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "()V", "app_debug"})
        public static final class Settings extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item.Settings INSTANCE = null;
            
            private Settings() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0006\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0007\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\b\u001a\u00020\u00032\b\u0010\t\u001a\u0004\u0018\u00010\nH\u00d6\u0003J\t\u0010\u000b\u001a\u00020\fH\u00d6\u0001J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0005\u00a8\u0006\u000f"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$RequestDesktop;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "isChecked", "", "(Z)V", "()Z", "component1", "copy", "equals", "other", "", "hashCode", "", "toString", "", "app_debug"})
        public static final class RequestDesktop extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item {
            private final boolean isChecked = false;
            
            @org.jetbrains.annotations.NotNull()
            public final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item.RequestDesktop copy(boolean isChecked) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public RequestDesktop(boolean isChecked) {
                super();
            }
            
            public final boolean component1() {
                return false;
            }
            
            public final boolean isChecked() {
                return false;
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$FindInPage;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "()V", "app_debug"})
        public static final class FindInPage extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item.FindInPage INSTANCE = null;
            
            private FindInPage() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Share;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "()V", "app_debug"})
        public static final class Share extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item.Share INSTANCE = null;
            
            private Share() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\u00032\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00d6\u0003J\t\u0010\f\u001a\u00020\rH\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Back;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "viewHistory", "", "(Z)V", "getViewHistory", "()Z", "component1", "copy", "equals", "other", "", "hashCode", "", "toString", "", "app_debug"})
        public static final class Back extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item {
            private final boolean viewHistory = false;
            
            @org.jetbrains.annotations.NotNull()
            public final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item.Back copy(boolean viewHistory) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Back(boolean viewHistory) {
                super();
            }
            
            public final boolean component1() {
                return false;
            }
            
            public final boolean getViewHistory() {
                return false;
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\u00032\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00d6\u0003J\t\u0010\f\u001a\u00020\rH\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Forward;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "viewHistory", "", "(Z)V", "getViewHistory", "()Z", "component1", "copy", "equals", "other", "", "hashCode", "", "toString", "", "app_debug"})
        public static final class Forward extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item {
            private final boolean viewHistory = false;
            
            @org.jetbrains.annotations.NotNull()
            public final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item.Forward copy(boolean viewHistory) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Forward(boolean viewHistory) {
                super();
            }
            
            public final boolean component1() {
                return false;
            }
            
            public final boolean getViewHistory() {
                return false;
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\u00032\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00d6\u0003J\t\u0010\f\u001a\u00020\rH\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Reload;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "bypassCache", "", "(Z)V", "getBypassCache", "()Z", "component1", "copy", "equals", "other", "", "hashCode", "", "toString", "", "app_debug"})
        public static final class Reload extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item {
            private final boolean bypassCache = false;
            
            @org.jetbrains.annotations.NotNull()
            public final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item.Reload copy(boolean bypassCache) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Reload(boolean bypassCache) {
                super();
            }
            
            public final boolean component1() {
                return false;
            }
            
            public final boolean getBypassCache() {
                return false;
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Stop;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "()V", "app_debug"})
        public static final class Stop extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item.Stop INSTANCE = null;
            
            private Stop() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$AddToHomeScreen;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "()V", "app_debug"})
        public static final class AddToHomeScreen extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item.AddToHomeScreen INSTANCE = null;
            
            private AddToHomeScreen() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$NewTab;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "()V", "app_debug"})
        public static final class NewTab extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item.NewTab INSTANCE = null;
            
            private NewTab() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$NewPrivateTab;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "()V", "app_debug"})
        public static final class NewPrivateTab extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item.NewPrivateTab INSTANCE = null;
            
            private NewPrivateTab() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$OpenInApp;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "()V", "app_debug"})
        public static final class OpenInApp extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item.OpenInApp INSTANCE = null;
            
            private OpenInApp() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Bookmarks;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "()V", "app_debug"})
        public static final class Bookmarks extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item.Bookmarks INSTANCE = null;
            
            private Bookmarks() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$History;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "()V", "app_debug"})
        public static final class History extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item.History INSTANCE = null;
            
            private History() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item$Print;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "()V", "app_debug"})
        public static final class Print extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item.Print INSTANCE = null;
            
            private Print() {
                super();
            }
        }
    }
}