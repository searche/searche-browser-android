package com.cookiejarapps.android.smartcookieweb.browser.tabs;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0016\u0018\u0000 !2\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u0004:\u0002!\"B=\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0018\b\u0002\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00030\bj\u0002`\n\u0012\b\b\u0002\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J&\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u00162\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00190\u0018H\u0016J\u0018\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\t2\u0006\u0010\u001c\u001a\u00020\u0016H\u0016J*\u0010\u001d\u001a\u00020\u00132\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00020\u00182\b\u0010\u001f\u001a\u0004\u0018\u00010 2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00030\bj\u0002`\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/tabs/TabListAdapter;", "Landroidx/recyclerview/widget/ListAdapter;", "Lmozilla/components/browser/state/state/TabSessionState;", "Lmozilla/components/browser/tabstray/TabViewHolder;", "Lmozilla/components/browser/tabstray/TabsTray;", "thumbnailLoader", "Lmozilla/components/concept/base/images/ImageLoader;", "viewHolderProvider", "Lkotlin/Function1;", "Landroid/view/ViewGroup;", "Lcom/cookiejarapps/android/smartcookieweb/browser/tabs/ViewHolderProvider;", "styling", "Lmozilla/components/browser/tabstray/TabsTrayStyling;", "delegate", "Lmozilla/components/browser/tabstray/TabsTray$Delegate;", "(Lmozilla/components/concept/base/images/ImageLoader;Lkotlin/jvm/functions/Function1;Lmozilla/components/browser/tabstray/TabsTrayStyling;Lmozilla/components/browser/tabstray/TabsTray$Delegate;)V", "selectedTabId", "", "onBindViewHolder", "", "holder", "position", "", "payloads", "", "", "onCreateViewHolder", "parent", "viewType", "updateTabs", "tabs", "tabPartition", "Lmozilla/components/browser/state/state/TabPartition;", "Companion", "DiffCallback", "app_debug"})
public class TabListAdapter extends androidx.recyclerview.widget.ListAdapter<mozilla.components.browser.state.state.TabSessionState, mozilla.components.browser.tabstray.TabViewHolder> implements mozilla.components.browser.tabstray.TabsTray {
    private final kotlin.jvm.functions.Function1<android.view.ViewGroup, mozilla.components.browser.tabstray.TabViewHolder> viewHolderProvider = null;
    private final mozilla.components.browser.tabstray.TabsTrayStyling styling = null;
    private final mozilla.components.browser.tabstray.TabsTray.Delegate delegate = null;
    private java.lang.String selectedTabId;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.browser.tabs.TabListAdapter.Companion Companion = null;
    
    /**
     * Payload used in onBindViewHolder for a partial update of the current view.
     *
     * Signals that the currently selected tab should be highlighted. This is the default behavior.
     */
    private static final int PAYLOAD_HIGHLIGHT_SELECTED_ITEM = com.cookiejarapps.android.smartcookieweb.R.id.payload_highlight_selected_item;
    
    /**
     * Payload used in onBindViewHolder for a partial update of the current view.
     *
     * Signals that the currently selected tab should NOT be highlighted. No tabs would appear as highlighted.
     */
    private static final int PAYLOAD_DONT_HIGHLIGHT_SELECTED_ITEM = com.cookiejarapps.android.smartcookieweb.R.id.payload_dont_highlight_selected_item;
    
    public TabListAdapter(@org.jetbrains.annotations.Nullable()
    mozilla.components.concept.base.images.ImageLoader thumbnailLoader, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super android.view.ViewGroup, ? extends mozilla.components.browser.tabstray.TabViewHolder> viewHolderProvider, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.tabstray.TabsTrayStyling styling, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.tabstray.TabsTray.Delegate delegate) {
        super(null);
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public mozilla.components.browser.tabstray.TabViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.tabstray.TabViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.tabstray.TabViewHolder holder, int position, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends java.lang.Object> payloads) {
    }
    
    @java.lang.Override()
    public void updateTabs(@org.jetbrains.annotations.NotNull()
    java.util.List<mozilla.components.browser.state.state.TabSessionState> tabs, @org.jetbrains.annotations.Nullable()
    mozilla.components.browser.state.state.TabPartition tabPartition, @org.jetbrains.annotations.Nullable()
    java.lang.String selectedTabId) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u00c2\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016J\u0018\u0010\b\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016\u00a8\u0006\t"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/tabs/TabListAdapter$DiffCallback;", "Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "Lmozilla/components/browser/state/state/TabSessionState;", "()V", "areContentsTheSame", "", "oldItem", "newItem", "areItemsTheSame", "app_debug"})
    static final class DiffCallback extends androidx.recyclerview.widget.DiffUtil.ItemCallback<mozilla.components.browser.state.state.TabSessionState> {
        @org.jetbrains.annotations.NotNull()
        public static final com.cookiejarapps.android.smartcookieweb.browser.tabs.TabListAdapter.DiffCallback INSTANCE = null;
        
        private DiffCallback() {
            super();
        }
        
        @java.lang.Override()
        public boolean areItemsTheSame(@org.jetbrains.annotations.NotNull()
        mozilla.components.browser.state.state.TabSessionState oldItem, @org.jetbrains.annotations.NotNull()
        mozilla.components.browser.state.state.TabSessionState newItem) {
            return false;
        }
        
        @java.lang.Override()
        public boolean areContentsTheSame(@org.jetbrains.annotations.NotNull()
        mozilla.components.browser.state.state.TabSessionState oldItem, @org.jetbrains.annotations.NotNull()
        mozilla.components.browser.state.state.TabSessionState newItem) {
            return false;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006\u00a8\u0006\t"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/tabs/TabListAdapter$Companion;", "", "()V", "PAYLOAD_DONT_HIGHLIGHT_SELECTED_ITEM", "", "getPAYLOAD_DONT_HIGHLIGHT_SELECTED_ITEM", "()I", "PAYLOAD_HIGHLIGHT_SELECTED_ITEM", "getPAYLOAD_HIGHLIGHT_SELECTED_ITEM", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        public final int getPAYLOAD_HIGHLIGHT_SELECTED_ITEM() {
            return 0;
        }
        
        public final int getPAYLOAD_DONT_HIGHLIGHT_SELECTED_ITEM() {
            return 0;
        }
    }
}