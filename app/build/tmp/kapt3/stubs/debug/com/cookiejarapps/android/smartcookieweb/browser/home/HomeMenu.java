package com.cookiejarapps.android.smartcookieweb.browser.home;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0018BW\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0014\b\u0002\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007\u0012\u0014\b\u0002\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\t0\u0007\u0012\u0014\b\u0002\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\t0\u0007\u00a2\u0006\u0002\u0010\u000eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R!\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u00108BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0012\u0010\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\t0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\t0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu;", "", "lifecycleOwner", "Landroidx/lifecycle/LifecycleOwner;", "context", "Landroid/content/Context;", "onItemTapped", "Lkotlin/Function1;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item;", "", "onMenuBuilderChanged", "Lmozilla/components/browser/menu/BrowserMenuBuilder;", "onHighlightPresent", "Lmozilla/components/browser/menu/BrowserMenuHighlight;", "(Landroidx/lifecycle/LifecycleOwner;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "coreMenuItems", "", "Lmozilla/components/browser/menu/BrowserMenuItem;", "getCoreMenuItems", "()Ljava/util/List;", "coreMenuItems$delegate", "Lkotlin/Lazy;", "shouldUseBottomToolbar", "", "Item", "app_debug"})
public final class HomeMenu {
    private final androidx.lifecycle.LifecycleOwner lifecycleOwner = null;
    private final android.content.Context context = null;
    private final kotlin.jvm.functions.Function1<com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item, kotlin.Unit> onItemTapped = null;
    private final kotlin.jvm.functions.Function1<mozilla.components.browser.menu.BrowserMenuBuilder, kotlin.Unit> onMenuBuilderChanged = null;
    private final kotlin.jvm.functions.Function1<mozilla.components.browser.menu.BrowserMenuHighlight, kotlin.Unit> onHighlightPresent = null;
    private final boolean shouldUseBottomToolbar = false;
    private final kotlin.Lazy coreMenuItems$delegate = null;
    
    public HomeMenu(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LifecycleOwner lifecycleOwner, @org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item, kotlin.Unit> onItemTapped, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super mozilla.components.browser.menu.BrowserMenuBuilder, kotlin.Unit> onMenuBuilderChanged, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super mozilla.components.browser.menu.BrowserMenuHighlight, kotlin.Unit> onHighlightPresent) {
        super();
    }
    
    private final java.util.List<mozilla.components.browser.menu.BrowserMenuItem> getCoreMenuItems() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\t\u0003\u0004\u0005\u0006\u0007\b\t\n\u000bB\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\t\f\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u00a8\u0006\u0015"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item;", "", "()V", "AddonsManager", "Bookmarks", "Downloads", "Help", "History", "Quit", "Settings", "SyncedTabs", "WhatsNew", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$WhatsNew;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$Help;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$AddonsManager;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$Settings;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$SyncedTabs;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$History;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$Bookmarks;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$Downloads;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$Quit;", "app_debug"})
    public static abstract class Item {
        
        private Item() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$WhatsNew;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item;", "()V", "app_debug"})
        public static final class WhatsNew extends com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item.WhatsNew INSTANCE = null;
            
            private WhatsNew() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$Help;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item;", "()V", "app_debug"})
        public static final class Help extends com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item.Help INSTANCE = null;
            
            private Help() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$AddonsManager;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item;", "()V", "app_debug"})
        public static final class AddonsManager extends com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item.AddonsManager INSTANCE = null;
            
            private AddonsManager() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$Settings;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item;", "()V", "app_debug"})
        public static final class Settings extends com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item.Settings INSTANCE = null;
            
            private Settings() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$SyncedTabs;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item;", "()V", "app_debug"})
        public static final class SyncedTabs extends com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item.SyncedTabs INSTANCE = null;
            
            private SyncedTabs() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$History;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item;", "()V", "app_debug"})
        public static final class History extends com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item.History INSTANCE = null;
            
            private History() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$Bookmarks;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item;", "()V", "app_debug"})
        public static final class Bookmarks extends com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item.Bookmarks INSTANCE = null;
            
            private Bookmarks() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$Downloads;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item;", "()V", "app_debug"})
        public static final class Downloads extends com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item.Downloads INSTANCE = null;
            
            private Downloads() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item$Quit;", "Lcom/cookiejarapps/android/smartcookieweb/browser/home/HomeMenu$Item;", "()V", "app_debug"})
        public static final class Quit extends com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item {
            @org.jetbrains.annotations.NotNull()
            public static final com.cookiejarapps.android.smartcookieweb.browser.home.HomeMenu.Item.Quit INSTANCE = null;
            
            private Quit() {
                super();
            }
        }
    }
}