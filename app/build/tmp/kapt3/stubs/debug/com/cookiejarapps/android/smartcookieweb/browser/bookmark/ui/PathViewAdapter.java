package com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\b\u0012\u0004\u0012\u00020\u00040\u0003:\u0001)B\u0015\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\u0013\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u001eJ\b\u0010\u001f\u001a\u00020\u0016H\u0016J\u0018\u0010 \u001a\u00020\u001c2\u0006\u0010!\u001a\u00020\u00042\u0006\u0010\"\u001a\u00020\u0016H\u0016J\u0018\u0010#\u001a\u00020\u00042\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\u0016H\u0016J\u000e\u0010\'\u001a\u00020\u001c2\u0006\u0010(\u001a\u00020\u0016R\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00028\u00000\u000b8F\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\rR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00000\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0015\u001a\u00020\u0016@BX\u0086\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u000e\u0010\u001a\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathViewAdapter;", "T", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathView$Path;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathViewAdapter$BreadcrumbViewHolder;", "context", "Landroid/content/Context;", "pathView", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathView;", "(Landroid/content/Context;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathView;)V", "crumbs", "", "getCrumbs", "()Ljava/util/List;", "endPadding", "", "inflater", "Landroid/view/LayoutInflater;", "items", "", "leftOffset", "<set-?>", "", "selectedItem", "getSelectedItem", "()I", "standardPadding", "addItem", "", "item", "(Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathView$Path;)V", "getItemCount", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "select", "index", "BreadcrumbViewHolder", "app_debug"})
public final class PathViewAdapter<T extends com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.PathView.Path> extends androidx.recyclerview.widget.RecyclerView.Adapter<com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.PathViewAdapter.BreadcrumbViewHolder> {
    private final com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.PathView pathView = null;
    private final android.view.LayoutInflater inflater = null;
    private final java.util.List<T> items = null;
    private final float leftOffset = 0.0F;
    private final float endPadding = 0.0F;
    private final float standardPadding = 0.0F;
    private int selectedItem = -1;
    
    public PathViewAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.PathView pathView) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<T> getCrumbs() {
        return null;
    }
    
    public final int getSelectedItem() {
        return 0;
    }
    
    public final void addItem(@org.jetbrains.annotations.NotNull()
    T item) {
    }
    
    public final void select(int index) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.PathViewAdapter.BreadcrumbViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.PathViewAdapter.BreadcrumbViewHolder holder, int position) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0016\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\t"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathViewAdapter$BreadcrumbViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Landroid/view/View;)V", "title", "Landroid/widget/TextView;", "getTitle", "()Landroid/widget/TextView;", "app_debug"})
    public static class BreadcrumbViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView title = null;
        
        public BreadcrumbViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View view) {
            super(null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getTitle() {
            return null;
        }
    }
}