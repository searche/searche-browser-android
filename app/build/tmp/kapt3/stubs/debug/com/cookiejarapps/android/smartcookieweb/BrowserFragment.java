package com.cookiejarapps.android.smartcookieweb;

import java.lang.System;

/**
 * Fragment used for browsing the web within the main app.
 */
@kotlin.Suppress(names = {"TooManyFunctions", "LargeClass"})
@kotlinx.coroutines.ExperimentalCoroutinesApi()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u001d\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0010\u00a2\u0006\u0002\b\u000fR\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/BrowserFragment;", "Lcom/cookiejarapps/android/smartcookieweb/BaseBrowserFragment;", "Lmozilla/components/support/base/feature/UserInteractionHandler;", "()V", "webExtToolbarFeature", "Lmozilla/components/support/base/feature/ViewBoundFeatureWrapper;", "Lmozilla/components/feature/toolbar/WebExtensionToolbarFeature;", "windowFeature", "Lmozilla/components/feature/tabs/WindowFeature;", "initializeUI", "", "view", "Landroid/view/View;", "tab", "Lmozilla/components/browser/state/state/SessionState;", "initializeUI$app_debug", "app_debug"})
public final class BrowserFragment extends com.cookiejarapps.android.smartcookieweb.BaseBrowserFragment implements mozilla.components.support.base.feature.UserInteractionHandler {
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.feature.tabs.WindowFeature> windowFeature = null;
    private final mozilla.components.support.base.feature.ViewBoundFeatureWrapper<mozilla.components.feature.toolbar.WebExtensionToolbarFeature> webExtToolbarFeature = null;
    
    public BrowserFragment() {
        super();
    }
    
    @kotlin.Suppress(names = {"LongMethod"})
    @java.lang.Override()
    public void initializeUI$app_debug(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.state.SessionState tab) {
    }
}