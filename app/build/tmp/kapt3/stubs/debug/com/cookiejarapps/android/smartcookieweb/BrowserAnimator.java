package com.cookiejarapps.android.smartcookieweb;

import java.lang.System;

/**
 * Handles properly animating the browser engine based on `SHOULD_ANIMATE_FLAG` passed in through
 * nav arguments.
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B=\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0003\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0003\u00a2\u0006\u0002\u0010\u000bJ\u0006\u0010\u0012\u001a\u00020\u0013J\u0014\u0010\u0014\u001a\u00020\u00132\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00130\u0016J\u0012\u0010\u0017\u001a\u00020\u0018*\b\u0012\u0004\u0012\u00020\u00040\u0003H\u0002R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\u0004\u0018\u00010\u00068BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u0004\u0018\u00010\b8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/BrowserAnimator;", "", "fragment", "Ljava/lang/ref/WeakReference;", "Landroidx/fragment/app/Fragment;", "engineView", "Lmozilla/components/concept/engine/EngineView;", "swipeRefresh", "Landroid/view/View;", "viewLifecycleScope", "Landroidx/lifecycle/LifecycleCoroutineScope;", "(Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V", "unwrappedEngineView", "getUnwrappedEngineView", "()Lmozilla/components/concept/engine/EngineView;", "unwrappedSwipeRefresh", "getUnwrappedSwipeRefresh", "()Landroid/view/View;", "beginAnimateInIfNecessary", "", "captureEngineViewAndDrawStatically", "onComplete", "Lkotlin/Function0;", "isAdded", "", "Companion", "app_debug"})
public final class BrowserAnimator {
    private final java.lang.ref.WeakReference<androidx.fragment.app.Fragment> fragment = null;
    private final java.lang.ref.WeakReference<mozilla.components.concept.engine.EngineView> engineView = null;
    private final java.lang.ref.WeakReference<android.view.View> swipeRefresh = null;
    private final java.lang.ref.WeakReference<androidx.lifecycle.LifecycleCoroutineScope> viewLifecycleScope = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.BrowserAnimator.Companion Companion = null;
    
    public BrowserAnimator(@org.jetbrains.annotations.NotNull()
    java.lang.ref.WeakReference<androidx.fragment.app.Fragment> fragment, @org.jetbrains.annotations.NotNull()
    java.lang.ref.WeakReference<mozilla.components.concept.engine.EngineView> engineView, @org.jetbrains.annotations.NotNull()
    java.lang.ref.WeakReference<android.view.View> swipeRefresh, @org.jetbrains.annotations.NotNull()
    java.lang.ref.WeakReference<androidx.lifecycle.LifecycleCoroutineScope> viewLifecycleScope) {
        super();
    }
    
    private final mozilla.components.concept.engine.EngineView getUnwrappedEngineView() {
        return null;
    }
    
    private final android.view.View getUnwrappedSwipeRefresh() {
        return null;
    }
    
    public final void beginAnimateInIfNecessary() {
    }
    
    /**
     * Makes the swipeRefresh background a screenshot of the engineView in its current state.
     * This allows us to "animate" the engineView.
     */
    public final void captureEngineViewAndDrawStatically(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> onComplete) {
    }
    
    private final boolean isAdded(java.lang.ref.WeakReference<androidx.fragment.app.Fragment> $this$isAdded) {
        return false;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/BrowserAnimator$Companion;", "", "()V", "getToolbarNavOptions", "Landroidx/navigation/NavOptions;", "context", "Landroid/content/Context;", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.navigation.NavOptions getToolbarNavOptions(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
    }
}