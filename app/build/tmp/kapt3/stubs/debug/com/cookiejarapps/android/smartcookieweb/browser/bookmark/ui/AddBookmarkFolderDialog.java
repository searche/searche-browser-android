package com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bB9\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\rJ\u000e\u0010\u0016\u001a\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u0013J\u0006\u0010\u0018\u001a\u00020\u0019R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/AddBookmarkFolderDialog;", "", "context", "Landroid/content/Context;", "manager", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/repository/BookmarkManager;", "item", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;", "(Landroid/content/Context;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/repository/BookmarkManager;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;)V", "mManager", "title", "", "mParent", "(Landroid/content/Context;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/repository/BookmarkManager;Ljava/lang/String;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkFolderItem;)V", "addToTopCheckBox", "Landroid/widget/CheckBox;", "mDialog", "Landroidx/appcompat/app/AlertDialog;", "mOnClickListener", "Landroid/content/DialogInterface$OnClickListener;", "titleEditText", "Landroid/widget/EditText;", "setOnClickListener", "l", "show", "", "app_debug"})
public final class AddBookmarkFolderDialog {
    private com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository.BookmarkManager mManager;
    private com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem mParent;
    private final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem item = null;
    private final androidx.appcompat.app.AlertDialog mDialog = null;
    private final android.widget.EditText titleEditText = null;
    private final android.widget.CheckBox addToTopCheckBox = null;
    private android.content.DialogInterface.OnClickListener mOnClickListener;
    
    @kotlin.jvm.JvmOverloads()
    public AddBookmarkFolderDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository.BookmarkManager mManager, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem mParent) {
        super();
    }
    
    @kotlin.jvm.JvmOverloads()
    public AddBookmarkFolderDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository.BookmarkManager mManager, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem mParent, @org.jetbrains.annotations.Nullable()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem item) {
        super();
    }
    
    public AddBookmarkFolderDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.repository.BookmarkManager manager, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkFolderItem item) {
        super();
    }
    
    public final void show() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.AddBookmarkFolderDialog setOnClickListener(@org.jetbrains.annotations.NotNull()
    android.content.DialogInterface.OnClickListener l) {
        return null;
    }
}