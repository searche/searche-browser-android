package com.cookiejarapps.android.smartcookieweb.components.toolbar;

import java.lang.System;

@kotlinx.coroutines.ExperimentalCoroutinesApi()
@kotlin.Suppress(names = {"LargeClass", "LongParameterList"})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001BC\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0014\b\u0002\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u000fJ\b\u00109\u001a\u00020\u0007H\u0002J\r\u0010:\u001a\u00020;H\u0001\u00a2\u0006\u0002\b<R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0012\u001a\u00020\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0017\u001a\u00020\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0014R\u000e\u0010\u0019\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u001a\u001a\u00020\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0014R\u0011\u0010\u000e\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u001cR\u000e\u0010\f\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u001d\u001a\u00020\u001e8VX\u0096\u0084\u0002\u00a2\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b\u001f\u0010 R!\u0010#\u001a\b\u0012\u0004\u0012\u00020%0$8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b(\u0010\"\u001a\u0004\b&\u0010\'R\u001b\u0010)\u001a\u00020*8VX\u0096\u0084\u0002\u00a2\u0006\f\n\u0004\b-\u0010\"\u001a\u0004\b+\u0010,R\u0011\u0010.\u001a\u00020\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b/\u0010\u0014R\u0011\u00100\u001a\u00020\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b1\u0010\u0014R\u001a\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u00102\u001a\u00020\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b3\u0010\u0014R\u0016\u00104\u001a\u0004\u0018\u0001058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b6\u00107R\u000e\u00108\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006="}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserMenu;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu;", "context", "Landroid/content/Context;", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "shouldReverseItems", "", "onItemTapped", "Lkotlin/Function1;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu$Item;", "", "lifecycleOwner", "Landroidx/lifecycle/LifecycleOwner;", "isPinningSupported", "(Landroid/content/Context;Lmozilla/components/browser/state/store/BrowserStore;ZLkotlin/jvm/functions/Function1;Landroidx/lifecycle/LifecycleOwner;Z)V", "addToHomescreen", "Lmozilla/components/browser/menu/item/BrowserMenuImageText;", "bookmarksItem", "getBookmarksItem", "()Lmozilla/components/browser/menu/item/BrowserMenuImageText;", "desktopMode", "Lmozilla/components/browser/menu/item/BrowserMenuImageSwitch;", "externalAppItem", "getExternalAppItem", "findInPage", "historyItem", "getHistoryItem", "()Z", "menuBuilder", "Lmozilla/components/browser/menu/WebExtensionBrowserMenuBuilder;", "getMenuBuilder", "()Lmozilla/components/browser/menu/WebExtensionBrowserMenuBuilder;", "menuBuilder$delegate", "Lkotlin/Lazy;", "menuItems", "", "Lmozilla/components/browser/menu/BrowserMenuItem;", "getMenuItems", "()Ljava/util/List;", "menuItems$delegate", "menuToolbar", "Lmozilla/components/browser/menu/item/BrowserMenuItemToolbar;", "getMenuToolbar", "()Lmozilla/components/browser/menu/item/BrowserMenuItemToolbar;", "menuToolbar$delegate", "newPrivateTabItem", "getNewPrivateTabItem", "newTabItem", "getNewTabItem", "printItem", "getPrintItem", "selectedSession", "Lmozilla/components/browser/state/state/TabSessionState;", "getSelectedSession", "()Lmozilla/components/browser/state/state/TabSessionState;", "settings", "canAddToHomescreen", "primaryTextColor", "", "primaryTextColor$app_debug", "app_debug"})
public final class BrowserMenu implements com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu {
    private final android.content.Context context = null;
    private final mozilla.components.browser.state.store.BrowserStore store = null;
    private final kotlin.jvm.functions.Function1<com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item, kotlin.Unit> onItemTapped = null;
    private final androidx.lifecycle.LifecycleOwner lifecycleOwner = null;
    private final boolean isPinningSupported = false;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy menuBuilder$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy menuToolbar$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final mozilla.components.browser.menu.item.BrowserMenuImageText externalAppItem = null;
    private final kotlin.Lazy menuItems$delegate = null;
    private final mozilla.components.browser.menu.item.BrowserMenuImageText settings = null;
    private final mozilla.components.browser.menu.item.BrowserMenuImageSwitch desktopMode = null;
    private final mozilla.components.browser.menu.item.BrowserMenuImageText addToHomescreen = null;
    private final mozilla.components.browser.menu.item.BrowserMenuImageText findInPage = null;
    @org.jetbrains.annotations.NotNull()
    private final mozilla.components.browser.menu.item.BrowserMenuImageText historyItem = null;
    @org.jetbrains.annotations.NotNull()
    private final mozilla.components.browser.menu.item.BrowserMenuImageText printItem = null;
    @org.jetbrains.annotations.NotNull()
    private final mozilla.components.browser.menu.item.BrowserMenuImageText newTabItem = null;
    @org.jetbrains.annotations.NotNull()
    private final mozilla.components.browser.menu.item.BrowserMenuImageText newPrivateTabItem = null;
    @org.jetbrains.annotations.NotNull()
    private final mozilla.components.browser.menu.item.BrowserMenuImageText bookmarksItem = null;
    
    public BrowserMenu(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.store.BrowserStore store, boolean shouldReverseItems, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu.Item, kotlin.Unit> onItemTapped, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LifecycleOwner lifecycleOwner, boolean isPinningSupported) {
        super();
    }
    
    public final boolean isPinningSupported() {
        return false;
    }
    
    private final mozilla.components.browser.state.state.TabSessionState getSelectedSession() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public mozilla.components.browser.menu.WebExtensionBrowserMenuBuilder getMenuBuilder() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public mozilla.components.browser.menu.item.BrowserMenuItemToolbar getMenuToolbar() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.browser.menu.item.BrowserMenuImageText getExternalAppItem() {
        return null;
    }
    
    private final boolean canAddToHomescreen() {
        return false;
    }
    
    private final java.util.List<mozilla.components.browser.menu.BrowserMenuItem> getMenuItems() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.browser.menu.item.BrowserMenuImageText getHistoryItem() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.browser.menu.item.BrowserMenuImageText getPrintItem() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.browser.menu.item.BrowserMenuImageText getNewTabItem() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.browser.menu.item.BrowserMenuImageText getNewPrivateTabItem() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.browser.menu.item.BrowserMenuImageText getBookmarksItem() {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    @androidx.annotation.ColorRes()
    public final int primaryTextColor$app_debug() {
        return 0;
    }
}