package com.cookiejarapps.android.smartcookieweb.browser.bookmark.items;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u001a2\u00020\u00012\u00020\u0002:\u0001\u001aB\u0017\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\b\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\tJ\u001a\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0015\u001a\u00020\u0016H\u0014J\u0010\u0010\u0017\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0019H\u0014R\u0014\u0010\n\u001a\u00020\u000b8TX\u0094\u0004\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\rR\u001a\u0010\b\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011\u00a8\u0006\u001b"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkSiteItem;", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkItem;", "Ljava/io/Serializable;", "title", "", "id", "", "(Ljava/lang/String;J)V", "url", "(Ljava/lang/String;Ljava/lang/String;J)V", "type", "", "getType", "()I", "getUrl", "()Ljava/lang/String;", "setUrl", "(Ljava/lang/String;)V", "readMain", "", "name", "reader", "Lcom/squareup/moshi/JsonReader;", "writeMain", "writer", "Lcom/squareup/moshi/JsonWriter;", "Companion", "app_debug"})
public final class BookmarkSiteItem extends com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkItem implements java.io.Serializable {
    public java.lang.String url;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.browser.bookmark.items.BookmarkSiteItem.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    protected static final java.lang.String COLUMN_NAME_URL = "2";
    public static final int BOOKMARK_ITEM_ID = 2;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUrl() {
        return null;
    }
    
    public final void setUrl(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @java.lang.Override()
    protected int getType() {
        return 0;
    }
    
    public BookmarkSiteItem(@org.jetbrains.annotations.NotNull()
    java.lang.String title, long id) {
        super(null, 0L);
    }
    
    public BookmarkSiteItem(@org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String url, long id) {
        super(null, 0L);
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.io.IOException.class})
    @java.lang.Override()
    protected boolean writeMain(@org.jetbrains.annotations.NotNull()
    com.squareup.moshi.JsonWriter writer) throws java.io.IOException {
        return false;
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.io.IOException.class})
    @java.lang.Override()
    protected boolean readMain(@org.jetbrains.annotations.Nullable()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    com.squareup.moshi.JsonReader reader) throws java.io.IOException {
        return false;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0084T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/items/BookmarkSiteItem$Companion;", "", "()V", "BOOKMARK_ITEM_ID", "", "COLUMN_NAME_URL", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}