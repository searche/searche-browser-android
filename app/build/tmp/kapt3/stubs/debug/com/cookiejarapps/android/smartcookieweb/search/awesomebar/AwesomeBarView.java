package com.cookiejarapps.android.smartcookieweb.search.awesomebar;

import java.lang.System;

/**
 * View that contains and configures the BrowserAwesomeBar
 * TODO: suggestions based on bookmarks
 */
@kotlin.Suppress(names = {"LargeClass"})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0088\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u001e\u0010\'\u001a\b\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+H\u0002J\u001e\u0010,\u001a\b\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+H\u0002J\u001e\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00170\u001b2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+H\u0002J\u0016\u0010.\u001a\b\u0012\u0004\u0012\u00020\u00170\u001b2\u0006\u0010/\u001a\u00020\u001aH\u0002J\b\u00100\u001a\u000201H\u0002J$\u00102\u001a\u0002012\f\u00103\u001a\b\u0012\u0004\u0012\u00020\u00170\u00162\f\u00104\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016H\u0002J\u0016\u00105\u001a\u0002012\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+J\u0018\u00106\u001a\u0002012\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u0018\u001a\u0014\u0012\u0004\u0012\u00020\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00170\u001b0\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010&\u00a8\u00067"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/awesomebar/AwesomeBarView;", "", "activity", "Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;", "interactor", "Lcom/cookiejarapps/android/smartcookieweb/search/awesomebar/AwesomeBarInteractor;", "view", "Lcom/cookiejarapps/android/smartcookieweb/search/awesomebar/AwesomeBarWrapper;", "(Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;Lcom/cookiejarapps/android/smartcookieweb/search/awesomebar/AwesomeBarInteractor;Lcom/cookiejarapps/android/smartcookieweb/search/awesomebar/AwesomeBarWrapper;)V", "bookmarksStorageSuggestionProvider", "Lmozilla/components/feature/awesomebar/provider/BookmarksStorageSuggestionProvider;", "defaultSearchActionProvider", "Lmozilla/components/feature/awesomebar/provider/SearchActionProvider;", "defaultSearchSuggestionProvider", "Lmozilla/components/feature/awesomebar/provider/SearchSuggestionProvider;", "historyStorageProvider", "Lmozilla/components/feature/awesomebar/provider/HistoryStorageSuggestionProvider;", "getInteractor", "()Lcom/cookiejarapps/android/smartcookieweb/search/awesomebar/AwesomeBarInteractor;", "loadUrlUseCase", "Lmozilla/components/feature/session/SessionUseCases$LoadUrlUseCase;", "providersInUse", "", "Lmozilla/components/concept/awesomebar/AwesomeBar$SuggestionProvider;", "searchSuggestionProviderMap", "", "Lmozilla/components/browser/state/search/SearchEngine;", "", "searchUseCase", "Lmozilla/components/feature/search/SearchUseCases$SearchUseCase;", "selectTabUseCase", "Lmozilla/components/feature/tabs/TabsUseCases$SelectTabUseCase;", "sessionProvider", "Lmozilla/components/feature/awesomebar/provider/SessionSuggestionProvider;", "shortcutSearchUseCase", "shortcutsEnginePickerProvider", "Lcom/cookiejarapps/android/smartcookieweb/search/awesomebar/ShortcutsSuggestionProvider;", "getView", "()Lcom/cookiejarapps/android/smartcookieweb/search/awesomebar/AwesomeBarWrapper;", "getProvidersToAdd", "context", "Landroid/content/Context;", "state", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentState;", "getProvidersToRemove", "getSelectedSearchSuggestionProvider", "getSuggestionProviderForEngine", "engine", "handleDisplayShortcutsProviders", "", "performProviderListChanges", "providersToAdd", "providersToRemove", "update", "updateSuggestionProvidersVisibility", "app_debug"})
public final class AwesomeBarView {
    private final com.cookiejarapps.android.smartcookieweb.BrowserActivity activity = null;
    @org.jetbrains.annotations.NotNull()
    private final com.cookiejarapps.android.smartcookieweb.search.awesomebar.AwesomeBarInteractor interactor = null;
    @org.jetbrains.annotations.NotNull()
    private final com.cookiejarapps.android.smartcookieweb.search.awesomebar.AwesomeBarWrapper view = null;
    private final mozilla.components.feature.awesomebar.provider.SessionSuggestionProvider sessionProvider = null;
    private final mozilla.components.feature.awesomebar.provider.HistoryStorageSuggestionProvider historyStorageProvider = null;
    private final mozilla.components.feature.awesomebar.provider.BookmarksStorageSuggestionProvider bookmarksStorageSuggestionProvider = null;
    private final com.cookiejarapps.android.smartcookieweb.search.awesomebar.ShortcutsSuggestionProvider shortcutsEnginePickerProvider = null;
    private final mozilla.components.feature.awesomebar.provider.SearchSuggestionProvider defaultSearchSuggestionProvider = null;
    private final mozilla.components.feature.awesomebar.provider.SearchActionProvider defaultSearchActionProvider = null;
    private final java.util.Map<mozilla.components.browser.state.search.SearchEngine, java.util.List<mozilla.components.concept.awesomebar.AwesomeBar.SuggestionProvider>> searchSuggestionProviderMap = null;
    private java.util.Set<mozilla.components.concept.awesomebar.AwesomeBar.SuggestionProvider> providersInUse;
    private final mozilla.components.feature.session.SessionUseCases.LoadUrlUseCase loadUrlUseCase = null;
    private final mozilla.components.feature.search.SearchUseCases.SearchUseCase searchUseCase = null;
    private final mozilla.components.feature.search.SearchUseCases.SearchUseCase shortcutSearchUseCase = null;
    private final mozilla.components.feature.tabs.TabsUseCases.SelectTabUseCase selectTabUseCase = null;
    
    public AwesomeBarView(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.BrowserActivity activity, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.search.awesomebar.AwesomeBarInteractor interactor, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.search.awesomebar.AwesomeBarWrapper view) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.search.awesomebar.AwesomeBarInteractor getInteractor() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.search.awesomebar.AwesomeBarWrapper getView() {
        return null;
    }
    
    public final void update(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.search.SearchFragmentState state) {
    }
    
    private final void updateSuggestionProvidersVisibility(android.content.Context context, com.cookiejarapps.android.smartcookieweb.search.SearchFragmentState state) {
    }
    
    private final void performProviderListChanges(java.util.Set<mozilla.components.concept.awesomebar.AwesomeBar.SuggestionProvider> providersToAdd, java.util.Set<mozilla.components.concept.awesomebar.AwesomeBar.SuggestionProvider> providersToRemove) {
    }
    
    @kotlin.Suppress(names = {"ComplexMethod"})
    private final java.util.Set<mozilla.components.concept.awesomebar.AwesomeBar.SuggestionProvider> getProvidersToAdd(android.content.Context context, com.cookiejarapps.android.smartcookieweb.search.SearchFragmentState state) {
        return null;
    }
    
    private final java.util.Set<mozilla.components.concept.awesomebar.AwesomeBar.SuggestionProvider> getProvidersToRemove(android.content.Context context, com.cookiejarapps.android.smartcookieweb.search.SearchFragmentState state) {
        return null;
    }
    
    private final java.util.List<mozilla.components.concept.awesomebar.AwesomeBar.SuggestionProvider> getSelectedSearchSuggestionProvider(android.content.Context context, com.cookiejarapps.android.smartcookieweb.search.SearchFragmentState state) {
        return null;
    }
    
    private final void handleDisplayShortcutsProviders() {
    }
    
    private final java.util.List<mozilla.components.concept.awesomebar.AwesomeBar.SuggestionProvider> getSuggestionProviderForEngine(mozilla.components.browser.state.search.SearchEngine engine) {
        return null;
    }
}