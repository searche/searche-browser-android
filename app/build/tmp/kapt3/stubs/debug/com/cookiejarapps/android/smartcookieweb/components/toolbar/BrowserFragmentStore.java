package com.cookiejarapps.android.smartcookieweb.components.toolbar;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0002\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0006"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserFragmentStore;", "Lmozilla/components/lib/state/Store;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserFragmentState;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserFragmentAction;", "initialState", "(Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserFragmentState;)V", "app_debug"})
public final class BrowserFragmentStore extends mozilla.components.lib.state.Store<com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserFragmentState, com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserFragmentAction> {
    
    public BrowserFragmentStore(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserFragmentState initialState) {
        super(null, null, null, null);
    }
}