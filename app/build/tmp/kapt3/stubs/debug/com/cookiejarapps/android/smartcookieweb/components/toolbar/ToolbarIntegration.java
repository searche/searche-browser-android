package com.cookiejarapps.android.smartcookieweb.components.toolbar;

import java.lang.System;

@kotlinx.coroutines.ExperimentalCoroutinesApi()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b\'\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0006\u0010\u0019\u001a\u00020\u001aJ\b\u0010\u001b\u001a\u00020\u001aH\u0016J\b\u0010\u001c\u001a\u00020\u001aH\u0016R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarIntegration;", "Lmozilla/components/support/base/feature/LifecycleAwareFeature;", "context", "Landroid/content/Context;", "toolbar", "Lmozilla/components/browser/toolbar/BrowserToolbar;", "toolbarMenu", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu;", "sessionId", "", "isPrivate", "", "renderStyle", "Lmozilla/components/feature/toolbar/ToolbarFeature$RenderStyle;", "(Landroid/content/Context;Lmozilla/components/browser/toolbar/BrowserToolbar;Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu;Ljava/lang/String;ZLmozilla/components/feature/toolbar/ToolbarFeature$RenderStyle;)V", "menuPresenter", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/MenuPresenter;", "store", "Lmozilla/components/browser/state/store/BrowserStore;", "getStore", "()Lmozilla/components/browser/state/store/BrowserStore;", "toolbarController", "Lmozilla/components/feature/toolbar/ToolbarBehaviorController;", "toolbarPresenter", "Lmozilla/components/feature/toolbar/ToolbarPresenter;", "invalidateMenu", "", "start", "stop", "app_debug"})
public abstract class ToolbarIntegration implements mozilla.components.support.base.feature.LifecycleAwareFeature {
    @org.jetbrains.annotations.NotNull()
    private final mozilla.components.browser.state.store.BrowserStore store = null;
    private final mozilla.components.feature.toolbar.ToolbarPresenter toolbarPresenter = null;
    private final mozilla.components.feature.toolbar.ToolbarBehaviorController toolbarController = null;
    private final com.cookiejarapps.android.smartcookieweb.components.toolbar.MenuPresenter menuPresenter = null;
    
    public ToolbarIntegration(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.toolbar.BrowserToolbar toolbar, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu toolbarMenu, @org.jetbrains.annotations.Nullable()
    java.lang.String sessionId, boolean isPrivate, @org.jetbrains.annotations.NotNull()
    mozilla.components.feature.toolbar.ToolbarFeature.RenderStyle renderStyle) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final mozilla.components.browser.state.store.BrowserStore getStore() {
        return null;
    }
    
    @java.lang.Override()
    public void start() {
    }
    
    @java.lang.Override()
    public void stop() {
    }
    
    public final void invalidateMenu() {
    }
}