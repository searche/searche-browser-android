package com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001:\u0002\u0019\u001aB%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0003\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bR\u000e\u0010\t\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u00020\u0007X\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0007X\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\f\u00a8\u0006\u001b"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathView;", "Landroidx/recyclerview/widget/RecyclerView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyle", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "arrowColor", "highlightedTextColor", "getHighlightedTextColor$app_debug", "()I", "linearLayoutManager", "Landroidx/recyclerview/widget/LinearLayoutManager;", "getLinearLayoutManager", "()Landroidx/recyclerview/widget/LinearLayoutManager;", "listener", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathView$OnPathViewClickListener;", "getListener", "()Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathView$OnPathViewClickListener;", "setListener", "(Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathView$OnPathViewClickListener;)V", "textColor", "getTextColor$app_debug", "OnPathViewClickListener", "Path", "app_debug"})
public final class PathView extends androidx.recyclerview.widget.RecyclerView {
    private final int highlightedTextColor = 0;
    private final int textColor = 0;
    private final int arrowColor = 0;
    @org.jetbrains.annotations.NotNull()
    private final androidx.recyclerview.widget.LinearLayoutManager linearLayoutManager = null;
    @org.jetbrains.annotations.Nullable()
    private com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.PathView.OnPathViewClickListener listener;
    
    @kotlin.jvm.JvmOverloads()
    public PathView(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super(null);
    }
    
    @kotlin.jvm.JvmOverloads()
    public PathView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.util.AttributeSet attrs) {
        super(null);
    }
    
    @kotlin.jvm.JvmOverloads()
    public PathView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.util.AttributeSet attrs, @androidx.annotation.StyleRes()
    int defStyle) {
        super(null);
    }
    
    public final int getHighlightedTextColor$app_debug() {
        return 0;
    }
    
    public final int getTextColor$app_debug() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.recyclerview.widget.LinearLayoutManager getLinearLayoutManager() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.PathView.OnPathViewClickListener getListener() {
        return null;
    }
    
    public final void setListener(@org.jetbrains.annotations.Nullable()
    com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.PathView.OnPathViewClickListener p0) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005\u00a8\u0006\u0006"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathView$Path;", "", "title", "", "getTitle", "()Ljava/lang/CharSequence;", "app_debug"})
    public static abstract interface Path {
        
        @org.jetbrains.annotations.NotNull()
        public abstract java.lang.CharSequence getTitle();
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/PathView$OnPathViewClickListener;", "", "onPathItemClick", "", "position", "", "app_debug"})
    public static abstract interface OnPathViewClickListener {
        
        public abstract void onPathItemClick(int position);
    }
}