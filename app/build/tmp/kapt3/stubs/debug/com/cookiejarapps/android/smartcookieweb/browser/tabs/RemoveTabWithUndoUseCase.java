package com.cookiejarapps.android.smartcookieweb.browser.tabs;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\u0011\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0096\u0002J\b\u0010\u000e\u001a\u00020\u000bH\u0002R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/tabs/RemoveTabWithUndoUseCase;", "Lmozilla/components/feature/tabs/TabsUseCases$RemoveTabUseCase;", "actual", "view", "Landroid/view/View;", "undo", "Lmozilla/components/feature/tabs/TabsUseCases$UndoTabRemovalUseCase;", "activity", "Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;", "(Lmozilla/components/feature/tabs/TabsUseCases$RemoveTabUseCase;Landroid/view/View;Lmozilla/components/feature/tabs/TabsUseCases$UndoTabRemovalUseCase;Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;)V", "invoke", "", "sessionId", "", "showSnackbar", "app_debug"})
final class RemoveTabWithUndoUseCase implements mozilla.components.feature.tabs.TabsUseCases.RemoveTabUseCase {
    private final mozilla.components.feature.tabs.TabsUseCases.RemoveTabUseCase actual = null;
    private final android.view.View view = null;
    private final mozilla.components.feature.tabs.TabsUseCases.UndoTabRemovalUseCase undo = null;
    private final com.cookiejarapps.android.smartcookieweb.BrowserActivity activity = null;
    
    public RemoveTabWithUndoUseCase(@org.jetbrains.annotations.NotNull()
    mozilla.components.feature.tabs.TabsUseCases.RemoveTabUseCase actual, @org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.NotNull()
    mozilla.components.feature.tabs.TabsUseCases.UndoTabRemovalUseCase undo, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.BrowserActivity activity) {
        super();
    }
    
    @java.lang.Override()
    public void invoke(@org.jetbrains.annotations.NotNull()
    java.lang.String sessionId) {
    }
    
    private final void showSnackbar() {
    }
    
    public void invoke(@org.jetbrains.annotations.NotNull()
    java.lang.String tabId, boolean selectParentIfExists) {
    }
}