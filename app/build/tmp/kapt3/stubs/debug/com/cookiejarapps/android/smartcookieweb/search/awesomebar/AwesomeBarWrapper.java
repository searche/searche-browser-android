package com.cookiejarapps.android.smartcookieweb.search.awesomebar;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\u0018\u00002\u00020\u00012\u00020\u0002B%\b\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\b\u0010\u0015\u001a\u00020\rH\u0017J!\u0010\u0016\u001a\u00020\r2\u0012\u0010\u0010\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00130\u0017\"\u00020\u0013H\u0016\u00a2\u0006\u0002\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0013H\u0016J\u0010\u0010\u001c\u001a\u00020\r2\u0006\u0010\u0014\u001a\u00020\fH\u0016J\b\u0010\u001d\u001a\u00020\rH\u0016J!\u0010\u001e\u001a\u00020\r2\u0012\u0010\u0010\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00130\u0017\"\u00020\u0013H\u0016\u00a2\u0006\u0002\u0010\u0018J\u001c\u0010\u001f\u001a\u00020\r2\u0012\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000bH\u0016J\u0016\u0010!\u001a\u00020\r2\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\r0\u000fH\u0016R\u001c\u0010\n\u001a\u0010\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00120\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\f0\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/awesomebar/AwesomeBarWrapper;", "Landroidx/compose/ui/platform/AbstractComposeView;", "Lmozilla/components/concept/awesomebar/AwesomeBar;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "onEditSuggestionListener", "Lkotlin/Function1;", "", "", "onStopListener", "Lkotlin/Function0;", "providers", "Landroidx/compose/runtime/MutableState;", "", "Lmozilla/components/concept/awesomebar/AwesomeBar$SuggestionProvider;", "text", "Content", "addProviders", "", "([Lmozilla/components/concept/awesomebar/AwesomeBar$SuggestionProvider;)V", "containsProvider", "", "provider", "onInputChanged", "removeAllProviders", "removeProviders", "setOnEditSuggestionListener", "listener", "setOnStopListener", "app_debug"})
public final class AwesomeBarWrapper extends androidx.compose.ui.platform.AbstractComposeView implements mozilla.components.concept.awesomebar.AwesomeBar {
    private final androidx.compose.runtime.MutableState<java.util.List<mozilla.components.concept.awesomebar.AwesomeBar.SuggestionProvider>> providers = null;
    private final androidx.compose.runtime.MutableState<java.lang.String> text = null;
    private kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> onEditSuggestionListener;
    private kotlin.jvm.functions.Function0<kotlin.Unit> onStopListener;
    
    @kotlin.jvm.JvmOverloads()
    public AwesomeBarWrapper(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super(null, null, 0);
    }
    
    @kotlin.jvm.JvmOverloads()
    public AwesomeBarWrapper(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.util.AttributeSet attrs) {
        super(null, null, 0);
    }
    
    @kotlin.jvm.JvmOverloads()
    public AwesomeBarWrapper(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.util.AttributeSet attrs, int defStyleAttr) {
        super(null, null, 0);
    }
    
    @androidx.compose.runtime.Composable()
    @java.lang.Override()
    public void Content() {
    }
    
    @java.lang.Override()
    public void addProviders(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.awesomebar.AwesomeBar.SuggestionProvider... providers) {
    }
    
    @java.lang.Override()
    public boolean containsProvider(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.awesomebar.AwesomeBar.SuggestionProvider provider) {
        return false;
    }
    
    @java.lang.Override()
    public void onInputChanged(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    @java.lang.Override()
    public void removeAllProviders() {
    }
    
    @java.lang.Override()
    public void removeProviders(@org.jetbrains.annotations.NotNull()
    mozilla.components.concept.awesomebar.AwesomeBar.SuggestionProvider... providers) {
    }
    
    @java.lang.Override()
    public void setOnEditSuggestionListener(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> listener) {
    }
    
    @java.lang.Override()
    public void setOnStopListener(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<kotlin.Unit> listener) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public android.view.View asView() {
        return null;
    }
    
    public void onInputCancelled() {
    }
    
    public void onInputStarted() {
    }
}