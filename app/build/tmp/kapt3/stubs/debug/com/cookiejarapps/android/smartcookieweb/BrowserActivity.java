package com.cookiejarapps.android.smartcookieweb;

import java.lang.System;

/**
 * Activity that holds the [BrowserFragment].
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u00ca\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0015\n\u0002\b\u0002\b\u0016\u0018\u0000 b2\u00020\u00012\u00020\u00022\u00020\u0003:\u0001bB\u0005\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010(\u001a\u00020\f2\u0006\u0010)\u001a\u00020*H\u0014J\u0012\u0010+\u001a\u0004\u0018\u00010,2\u0006\u0010-\u001a\u00020.H\u0014J\u001c\u0010/\u001a\u0004\u0018\u0001002\u0006\u00101\u001a\u0002022\b\u00103\u001a\u0004\u0018\u00010,H\u0014J\b\u00104\u001a\u000205H\u0016J\u0010\u00106\u001a\u0002072\u0006\u0010-\u001a\u000208H\u0016J\b\u00109\u001a\u000207H\u0002J\u001f\u0010:\u001a\u00020\u00192\u0006\u0010;\u001a\u0002082\b\u0010<\u001a\u0004\u0018\u00010=H\u0001\u00a2\u0006\u0002\b>J4\u0010?\u001a\u0002072\u0006\u0010@\u001a\u00020,2\u0006\u0010A\u001a\u00020\u00192\b\u0010B\u001a\u0004\u0018\u00010C2\u0006\u0010D\u001a\u00020\u00192\b\b\u0002\u0010E\u001a\u00020FH\u0002J\b\u0010G\u001a\u000207H\u0016J\"\u0010H\u001a\u0002072\u0006\u0010I\u001a\u00020J2\u0006\u0010K\u001a\u00020J2\b\u0010L\u001a\u0004\u0018\u000108H\u0004J\u0006\u0010M\u001a\u000207J\u0012\u0010N\u001a\u0002072\b\u0010O\u001a\u0004\u0018\u00010=H\u0014J,\u0010P\u001a\u0004\u0018\u00010Q2\b\u0010R\u001a\u0004\u0018\u00010Q2\u0006\u0010S\u001a\u00020,2\u0006\u0010T\u001a\u00020U2\u0006\u0010V\u001a\u00020WH\u0016J\u0012\u0010X\u001a\u0002072\b\u0010-\u001a\u0004\u0018\u000108H\u0004J\u0010\u0010Y\u001a\u0002072\u0006\u0010Z\u001a\u00020[H\u0002J\u001a\u0010\\\u001a\u0002072\u0006\u00101\u001a\u0002022\n\b\u0002\u00103\u001a\u0004\u0018\u00010,JJ\u0010]\u001a\u0002072\u0006\u0010@\u001a\u00020,2\u0006\u0010A\u001a\u00020\u00192\u0006\u00101\u001a\u0002022\n\b\u0002\u00103\u001a\u0004\u0018\u00010,2\n\b\u0002\u0010B\u001a\u0004\u0018\u00010C2\b\b\u0002\u0010D\u001a\u00020\u00192\b\b\u0002\u0010E\u001a\u00020FJ\u0006\u0010^\u001a\u000207J\u0012\u0010_\u001a\u0002072\n\u0010`\u001a\u00020a\"\u00020JR\u001a\u0010\u0005\u001a\u00020\u0006X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\fX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R!\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u00128BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u001a\u001a\u00020\u001b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001e\u0010\u0017\u001a\u0004\b\u001c\u0010\u001dR\u000e\u0010\u001f\u001a\u00020 X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010!\u001a\u0004\u0018\u00010\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010#\u001a\u00020$8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\'\u0010\u0017\u001a\u0004\b%\u0010&\u00a8\u0006c"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Landroid/content/ComponentCallbacks2;", "Lcom/cookiejarapps/android/smartcookieweb/NavHostActivity;", "()V", "binding", "Lcom/cookiejarapps/android/smartcookieweb/databinding/ActivityMainBinding;", "getBinding", "()Lcom/cookiejarapps/android/smartcookieweb/databinding/ActivityMainBinding;", "setBinding", "(Lcom/cookiejarapps/android/smartcookieweb/databinding/ActivityMainBinding;)V", "browsingModeManager", "Lcom/cookiejarapps/android/smartcookieweb/browser/BrowsingModeManager;", "getBrowsingModeManager", "()Lcom/cookiejarapps/android/smartcookieweb/browser/BrowsingModeManager;", "setBrowsingModeManager", "(Lcom/cookiejarapps/android/smartcookieweb/browser/BrowsingModeManager;)V", "externalSourceIntentProcessors", "", "Lcom/cookiejarapps/android/smartcookieweb/HomeIntentProcessor;", "getExternalSourceIntentProcessors", "()Ljava/util/List;", "externalSourceIntentProcessors$delegate", "Lkotlin/Lazy;", "isToolbarInflated", "", "navHost", "Landroidx/navigation/fragment/NavHostFragment;", "getNavHost", "()Landroidx/navigation/fragment/NavHostFragment;", "navHost$delegate", "navigationToolbar", "Landroidx/appcompat/widget/Toolbar;", "printExtension", "Lmozilla/components/concept/engine/webextension/WebExtension;", "webExtensionPopupFeature", "Lmozilla/components/support/webextensions/WebExtensionPopupFeature;", "getWebExtensionPopupFeature", "()Lmozilla/components/support/webextensions/WebExtensionPopupFeature;", "webExtensionPopupFeature$delegate", "createBrowsingModeManager", "initialMode", "Lcom/cookiejarapps/android/smartcookieweb/browser/BrowsingMode;", "getIntentSessionId", "", "intent", "Lmozilla/components/support/utils/SafeIntent;", "getNavDirections", "Landroidx/navigation/NavDirections;", "from", "Lcom/cookiejarapps/android/smartcookieweb/BrowserDirection;", "customTabSessionId", "getSupportActionBarAndInflateIfNecessary", "Landroidx/appcompat/app/ActionBar;", "handleNewIntent", "", "Landroid/content/Intent;", "installPrintExtension", "isActivityColdStarted", "startingIntent", "activityIcicle", "Landroid/os/Bundle;", "isActivityColdStarted$app_debug", "load", "searchTermOrURL", "newTab", "engine", "Lmozilla/components/browser/state/search/SearchEngine;", "forceSearch", "flags", "Lmozilla/components/concept/engine/EngineSession$LoadUrlFlags;", "navigateToBrowserOnColdStart", "onActivityResult", "requestCode", "", "resultCode", "data", "onBackPressed", "onCreate", "savedInstanceState", "onCreateView", "Landroid/view/View;", "parent", "name", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "onNewIntent", "openPopup", "webExtensionState", "Lmozilla/components/browser/state/state/WebExtensionState;", "openToBrowser", "openToBrowserAndLoad", "printPage", "setupNavigationToolbar", "topLevelDestinationIds", "", "Companion", "app_debug"})
public class BrowserActivity extends androidx.appcompat.app.AppCompatActivity implements android.content.ComponentCallbacks2, com.cookiejarapps.android.smartcookieweb.NavHostActivity {
    public com.cookiejarapps.android.smartcookieweb.databinding.ActivityMainBinding binding;
    public com.cookiejarapps.android.smartcookieweb.browser.BrowsingModeManager browsingModeManager;
    private boolean isToolbarInflated = false;
    private androidx.appcompat.widget.Toolbar navigationToolbar;
    private final kotlin.Lazy navHost$delegate = null;
    private final kotlin.Lazy externalSourceIntentProcessors$delegate = null;
    private final kotlin.Lazy webExtensionPopupFeature$delegate = null;
    private mozilla.components.concept.engine.webextension.WebExtension printExtension;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.BrowserActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String OPEN_TO_BROWSER = "open_to_browser";
    
    public BrowserActivity() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.databinding.ActivityMainBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.databinding.ActivityMainBinding p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.browser.BrowsingModeManager getBrowsingModeManager() {
        return null;
    }
    
    public final void setBrowsingModeManager(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.BrowsingModeManager p0) {
    }
    
    private final androidx.navigation.fragment.NavHostFragment getNavHost() {
        return null;
    }
    
    private final java.util.List<com.cookiejarapps.android.smartcookieweb.HomeIntentProcessor> getExternalSourceIntentProcessors() {
        return null;
    }
    
    private final mozilla.components.support.webextensions.WebExtensionPopupFeature getWebExtensionPopupFeature() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    protected java.lang.String getIntentSessionId(@org.jetbrains.annotations.NotNull()
    mozilla.components.support.utils.SafeIntent intent) {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    public final boolean isActivityColdStarted$app_debug(@org.jetbrains.annotations.NotNull()
    android.content.Intent startingIntent, @org.jetbrains.annotations.Nullable()
    android.os.Bundle activityIcicle) {
        return false;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected final void onNewIntent(@org.jetbrains.annotations.Nullable()
    android.content.Intent intent) {
    }
    
    public void handleNewIntent(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent) {
    }
    
    public void navigateToBrowserOnColdStart() {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected com.cookiejarapps.android.smartcookieweb.browser.BrowsingModeManager createBrowsingModeManager(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.browser.BrowsingMode initialMode) {
        return null;
    }
    
    @java.lang.Override()
    public final void onBackPressed() {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.Nullable()
    android.view.View parent, @org.jetbrains.annotations.NotNull()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.util.AttributeSet attrs) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.appcompat.app.ActionBar getSupportActionBarAndInflateIfNecessary() {
        return null;
    }
    
    @java.lang.Override()
    protected final void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @kotlin.Suppress(names = {"SpreadOperator"})
    public final void setupNavigationToolbar(@org.jetbrains.annotations.NotNull()
    int... topLevelDestinationIds) {
    }
    
    private final void openPopup(mozilla.components.browser.state.state.WebExtensionState webExtensionState) {
    }
    
    @kotlin.Suppress(names = {"LongParameterList"})
    public final void openToBrowserAndLoad(@org.jetbrains.annotations.NotNull()
    java.lang.String searchTermOrURL, boolean newTab, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.BrowserDirection from, @org.jetbrains.annotations.Nullable()
    java.lang.String customTabSessionId, @org.jetbrains.annotations.Nullable()
    mozilla.components.browser.state.search.SearchEngine engine, boolean forceSearch, @org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.EngineSession.LoadUrlFlags flags) {
    }
    
    public final void openToBrowser(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.BrowserDirection from, @org.jetbrains.annotations.Nullable()
    java.lang.String customTabSessionId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected androidx.navigation.NavDirections getNavDirections(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.BrowserDirection from, @org.jetbrains.annotations.Nullable()
    java.lang.String customTabSessionId) {
        return null;
    }
    
    private final void load(java.lang.String searchTermOrURL, boolean newTab, mozilla.components.browser.state.search.SearchEngine engine, boolean forceSearch, mozilla.components.concept.engine.EngineSession.LoadUrlFlags flags) {
    }
    
    private final void installPrintExtension() {
    }
    
    public final void printPage() {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity$Companion;", "", "()V", "OPEN_TO_BROWSER", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}