package com.cookiejarapps.android.smartcookieweb.browser;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005H&J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH&J\u0018\u0010\f\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\u0005H&\u00a8\u0006\u000f"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/SwipeGestureListener;", "", "onSwipeFinished", "", "velocityX", "", "velocityY", "onSwipeStarted", "", "start", "Landroid/graphics/PointF;", "next", "onSwipeUpdate", "distanceX", "distanceY", "app_debug"})
public abstract interface SwipeGestureListener {
    
    public abstract boolean onSwipeStarted(@org.jetbrains.annotations.NotNull()
    android.graphics.PointF start, @org.jetbrains.annotations.NotNull()
    android.graphics.PointF next);
    
    public abstract void onSwipeUpdate(float distanceX, float distanceY);
    
    public abstract void onSwipeFinished(float velocityX, float velocityY);
}