package com.cookiejarapps.android.smartcookieweb.browser.tabs;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0011\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0096\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/tabs/SelectTabWithHomepageUseCase;", "Lmozilla/components/feature/tabs/TabsUseCases$SelectTabUseCase;", "actual", "activity", "Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;", "(Lmozilla/components/feature/tabs/TabsUseCases$SelectTabUseCase;Lcom/cookiejarapps/android/smartcookieweb/BrowserActivity;)V", "invoke", "", "tabId", "", "app_debug"})
final class SelectTabWithHomepageUseCase implements mozilla.components.feature.tabs.TabsUseCases.SelectTabUseCase {
    private final mozilla.components.feature.tabs.TabsUseCases.SelectTabUseCase actual = null;
    private final com.cookiejarapps.android.smartcookieweb.BrowserActivity activity = null;
    
    public SelectTabWithHomepageUseCase(@org.jetbrains.annotations.NotNull()
    mozilla.components.feature.tabs.TabsUseCases.SelectTabUseCase actual, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.BrowserActivity activity) {
        super();
    }
    
    @java.lang.Override()
    public void invoke(@org.jetbrains.annotations.NotNull()
    java.lang.String tabId) {
    }
}