package com.cookiejarapps.android.smartcookieweb.addons;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0016B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0014J,\u0010\r\u001a\u0004\u0018\u00010\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u0017"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/addons/AddonSettingsActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "binding", "Lcom/cookiejarapps/android/smartcookieweb/databinding/ActivityAddOnSettingsBinding;", "getBinding", "()Lcom/cookiejarapps/android/smartcookieweb/databinding/ActivityAddOnSettingsBinding;", "setBinding", "(Lcom/cookiejarapps/android/smartcookieweb/databinding/ActivityAddOnSettingsBinding;)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "parent", "name", "", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "AddonSettingsFragment", "app_debug"})
public final class AddonSettingsActivity extends androidx.appcompat.app.AppCompatActivity {
    public com.cookiejarapps.android.smartcookieweb.databinding.ActivityAddOnSettingsBinding binding;
    
    public AddonSettingsActivity() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.cookiejarapps.android.smartcookieweb.databinding.ActivityAddOnSettingsBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.databinding.ActivityAddOnSettingsBinding p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.Nullable()
    android.view.View parent, @org.jetbrains.annotations.NotNull()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.util.AttributeSet attrs) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u0000 \u00182\u00020\u0001:\u0001\u0018B\u0005\u00a2\u0006\u0002\u0010\u0002J&\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\b\u0010\u0014\u001a\u00020\u0015H\u0016J\u001a\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\r2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\u00048DX\u0084\u0004\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/addons/AddonSettingsActivity$AddonSettingsFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/cookiejarapps/android/smartcookieweb/databinding/FragmentAddOnSettingsBinding;", "addon", "Lmozilla/components/feature/addons/Addon;", "binding", "getBinding", "()Lcom/cookiejarapps/android/smartcookieweb/databinding/FragmentAddOnSettingsBinding;", "engineSession", "Lmozilla/components/concept/engine/EngineSession;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDestroyView", "", "onViewCreated", "view", "Companion", "app_debug"})
    public static final class AddonSettingsFragment extends androidx.fragment.app.Fragment {
        private mozilla.components.feature.addons.Addon addon;
        private mozilla.components.concept.engine.EngineSession engineSession;
        private com.cookiejarapps.android.smartcookieweb.databinding.FragmentAddOnSettingsBinding _binding;
        @org.jetbrains.annotations.NotNull()
        public static final com.cookiejarapps.android.smartcookieweb.addons.AddonSettingsActivity.AddonSettingsFragment.Companion Companion = null;
        
        public AddonSettingsFragment() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        protected final com.cookiejarapps.android.smartcookieweb.databinding.FragmentAddOnSettingsBinding getBinding() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
        android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
        android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
        android.os.Bundle savedInstanceState) {
            return null;
        }
        
        @java.lang.Override()
        public void onViewCreated(@org.jetbrains.annotations.NotNull()
        android.view.View view, @org.jetbrains.annotations.Nullable()
        android.os.Bundle savedInstanceState) {
        }
        
        @java.lang.Override()
        public void onDestroyView() {
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/addons/AddonSettingsActivity$AddonSettingsFragment$Companion;", "", "()V", "create", "Lcom/cookiejarapps/android/smartcookieweb/addons/AddonSettingsActivity$AddonSettingsFragment;", "addon", "Lmozilla/components/feature/addons/Addon;", "app_debug"})
        public static final class Companion {
            
            private Companion() {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.cookiejarapps.android.smartcookieweb.addons.AddonSettingsActivity.AddonSettingsFragment create(@org.jetbrains.annotations.NotNull()
            mozilla.components.feature.addons.Addon addon) {
                return null;
            }
        }
    }
}