package com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001\u0013B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\nH\u0016J\u0018\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u0011\u0010\u0003\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/RecyclerViewItemTouchListener;", "Landroidx/recyclerview/widget/RecyclerView$OnItemTouchListener;", "()V", "gravity", "", "getGravity", "()I", "location", "Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/RecyclerViewItemTouchListener$RecyclerViewTouchLocation;", "onInterceptTouchEvent", "", "rv", "Landroidx/recyclerview/widget/RecyclerView;", "e", "Landroid/view/MotionEvent;", "onRequestDisallowInterceptTouchEvent", "", "disallowIntercept", "onTouchEvent", "RecyclerViewTouchLocation", "app_debug"})
public final class RecyclerViewItemTouchListener implements androidx.recyclerview.widget.RecyclerView.OnItemTouchListener {
    private com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.RecyclerViewItemTouchListener.RecyclerViewTouchLocation location = com.cookiejarapps.android.smartcookieweb.browser.bookmark.ui.RecyclerViewItemTouchListener.RecyclerViewTouchLocation.NONE;
    
    public RecyclerViewItemTouchListener() {
        super();
    }
    
    public final int getGravity() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean onInterceptTouchEvent(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView rv, @org.jetbrains.annotations.NotNull()
    android.view.MotionEvent e) {
        return false;
    }
    
    @java.lang.Override()
    public void onTouchEvent(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView rv, @org.jetbrains.annotations.NotNull()
    android.view.MotionEvent e) {
    }
    
    @java.lang.Override()
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0082\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005\u00a8\u0006\u0006"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/bookmark/ui/RecyclerViewItemTouchListener$RecyclerViewTouchLocation;", "", "(Ljava/lang/String;I)V", "NONE", "START", "END", "app_debug"})
    static enum RecyclerViewTouchLocation {
        /*public static final*/ NONE /* = new NONE() */,
        /*public static final*/ START /* = new START() */,
        /*public static final*/ END /* = new END() */;
        
        RecyclerViewTouchLocation() {
        }
    }
}