package com.cookiejarapps.android.smartcookieweb.search;

import java.lang.System;

@java.lang.SuppressWarnings(value = {"LargeClass", "TooManyFunctions"})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u0015\u001a\u00020\u0016H\u0002J\n\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0002J\b\u0010\u0019\u001a\u00020\fH\u0016J\u0012\u0010\u001a\u001a\u00020\u00162\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\u0012\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J&\u0010\u001f\u001a\u0004\u0018\u00010 2\u0006\u0010!\u001a\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010$2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\u0010\u0010%\u001a\u00020\u00162\u0006\u0010&\u001a\u00020\'H\u0016J\b\u0010(\u001a\u00020\u0016H\u0016J+\u0010)\u001a\u00020\u00162\u0006\u0010*\u001a\u00020+2\f\u0010,\u001a\b\u0012\u0004\u0012\u00020.0-2\u0006\u0010/\u001a\u000200H\u0016\u00a2\u0006\u0002\u00101J\b\u00102\u001a\u00020\u0016H\u0016J\b\u00103\u001a\u00020\u0016H\u0016J\u001a\u00104\u001a\u00020\u00162\u0006\u00105\u001a\u00020 2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\u0010\u00106\u001a\u00020\u00162\u0006\u00105\u001a\u00020 H\u0002J\u0010\u00107\u001a\u00020\f2\u0006\u00108\u001a\u000209H\u0002J\u001a\u0010:\u001a\u00020\u00162\u0006\u0010;\u001a\u0002092\b\u0010<\u001a\u0004\u0018\u00010.H\u0002J\u0010\u0010=\u001a\u00020\u00162\u0006\u0010;\u001a\u000209H\u0002J\u0010\u0010>\u001a\u00020\u00162\u0006\u0010;\u001a\u000209H\u0002R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0010\u001a\u00060\u0011j\u0002`\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006?"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/search/SearchDialogFragment;", "Landroidx/appcompat/app/AppCompatDialogFragment;", "Lmozilla/components/support/base/feature/UserInteractionHandler;", "()V", "_binding", "Lcom/cookiejarapps/android/smartcookieweb/databinding/FragmentSearchDialogBinding;", "awesomeBarView", "Lcom/cookiejarapps/android/smartcookieweb/search/awesomebar/AwesomeBarView;", "binding", "getBinding", "()Lcom/cookiejarapps/android/smartcookieweb/databinding/FragmentSearchDialogBinding;", "dialogHandledAction", "", "firstUpdate", "interactor", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchDialogInteractor;", "store", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentStore;", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchDialogFragmentStore;", "toolbarView", "Lcom/cookiejarapps/android/smartcookieweb/search/toolbar/ToolbarView;", "hideDeviceKeyboard", "", "historyStorageProvider", "Lmozilla/components/concept/storage/HistoryStorage;", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateDialog", "Landroid/app/Dialog;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onDismiss", "dialog", "Landroid/content/DialogInterface;", "onPause", "onRequestPermissionsResult", "requestCode", "", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onStart", "onStop", "onViewCreated", "view", "setupConstraints", "shouldShowAwesomebar", "searchFragmentState", "Lcom/cookiejarapps/android/smartcookieweb/search/SearchFragmentState;", "updateClipboardSuggestion", "searchState", "clipboardUrl", "updateSearchShortcutsIcon", "updateToolbarContentDescription", "app_debug"})
public final class SearchDialogFragment extends androidx.appcompat.app.AppCompatDialogFragment implements mozilla.components.support.base.feature.UserInteractionHandler {
    private com.cookiejarapps.android.smartcookieweb.databinding.FragmentSearchDialogBinding _binding;
    private com.cookiejarapps.android.smartcookieweb.search.SearchDialogInteractor interactor;
    private com.cookiejarapps.android.smartcookieweb.search.SearchFragmentStore store;
    private com.cookiejarapps.android.smartcookieweb.search.toolbar.ToolbarView toolbarView;
    private com.cookiejarapps.android.smartcookieweb.search.awesomebar.AwesomeBarView awesomeBarView;
    private boolean firstUpdate = true;
    private boolean dialogHandledAction = false;
    
    public SearchDialogFragment() {
        super();
    }
    
    private final com.cookiejarapps.android.smartcookieweb.databinding.FragmentSearchDialogBinding getBinding() {
        return null;
    }
    
    @java.lang.Override()
    public void onStart() {
    }
    
    @java.lang.Override()
    public void onStop() {
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.app.Dialog onCreateDialog(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.SuppressWarnings(value = {"LongMethod"})
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final boolean shouldShowAwesomebar(com.cookiejarapps.android.smartcookieweb.search.SearchFragmentState searchFragmentState) {
        return false;
    }
    
    @java.lang.Override()
    public void onPause() {
    }
    
    private final void hideDeviceKeyboard() {
    }
    
    @java.lang.Override()
    public void onDismiss(@org.jetbrains.annotations.NotNull()
    android.content.DialogInterface dialog) {
    }
    
    @java.lang.Override()
    public boolean onBackPressed() {
        return false;
    }
    
    private final mozilla.components.concept.storage.HistoryStorage historyStorageProvider() {
        return null;
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    private final void setupConstraints(android.view.View view) {
    }
    
    private final void updateClipboardSuggestion(com.cookiejarapps.android.smartcookieweb.search.SearchFragmentState searchState, java.lang.String clipboardUrl) {
    }
    
    private final void updateToolbarContentDescription(com.cookiejarapps.android.smartcookieweb.search.SearchFragmentState searchState) {
    }
    
    private final void updateSearchShortcutsIcon(com.cookiejarapps.android.smartcookieweb.search.SearchFragmentState searchState) {
    }
    
    public boolean onHomePressed() {
        return false;
    }
}