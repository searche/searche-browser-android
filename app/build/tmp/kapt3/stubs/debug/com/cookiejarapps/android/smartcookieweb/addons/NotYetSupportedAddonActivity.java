package com.cookiejarapps.android.smartcookieweb.addons;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0007B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0014\u00a8\u0006\b"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/addons/NotYetSupportedAddonActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "NotYetSupportedAddonFragment", "app_debug"})
public final class NotYetSupportedAddonActivity extends androidx.appcompat.app.AppCompatActivity {
    
    public NotYetSupportedAddonActivity() {
        super();
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0005\u0018\u0000 \u001a2\u00020\u00012\u00020\u0002:\u0001\u001aB\u0005\u00a2\u0006\u0002\u0010\u0003J&\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\b\u0010\u0017\u001a\u00020\u0012H\u0016J\u001a\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\n2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/addons/NotYetSupportedAddonActivity$NotYetSupportedAddonFragment;", "Landroidx/fragment/app/Fragment;", "Lmozilla/components/feature/addons/ui/UnsupportedAddonsAdapterDelegate;", "()V", "adapter", "Lmozilla/components/feature/addons/ui/UnsupportedAddonsAdapter;", "addons", "", "Lmozilla/components/feature/addons/Addon;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onUninstallError", "", "addonId", "", "throwable", "", "onUninstallSuccess", "onViewCreated", "view", "Companion", "app_debug"})
    public static final class NotYetSupportedAddonFragment extends androidx.fragment.app.Fragment implements mozilla.components.feature.addons.ui.UnsupportedAddonsAdapterDelegate {
        private java.util.List<mozilla.components.feature.addons.Addon> addons;
        private mozilla.components.feature.addons.ui.UnsupportedAddonsAdapter adapter;
        @org.jetbrains.annotations.NotNull()
        public static final com.cookiejarapps.android.smartcookieweb.addons.NotYetSupportedAddonActivity.NotYetSupportedAddonFragment.Companion Companion = null;
        
        public NotYetSupportedAddonFragment() {
            super();
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
        android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
        android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
        android.os.Bundle savedInstanceState) {
            return null;
        }
        
        @java.lang.Override()
        public void onViewCreated(@org.jetbrains.annotations.NotNull()
        android.view.View view, @org.jetbrains.annotations.Nullable()
        android.os.Bundle savedInstanceState) {
        }
        
        @java.lang.Override()
        public void onUninstallError(@org.jetbrains.annotations.NotNull()
        java.lang.String addonId, @org.jetbrains.annotations.NotNull()
        java.lang.Throwable throwable) {
        }
        
        @java.lang.Override()
        public void onUninstallSuccess() {
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\u0003\u001a\u00020\u00042\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\b\u00a8\u0006\t"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/addons/NotYetSupportedAddonActivity$NotYetSupportedAddonFragment$Companion;", "", "()V", "create", "Lcom/cookiejarapps/android/smartcookieweb/addons/NotYetSupportedAddonActivity$NotYetSupportedAddonFragment;", "addons", "Ljava/util/ArrayList;", "Lmozilla/components/feature/addons/Addon;", "Lkotlin/collections/ArrayList;", "app_debug"})
        public static final class Companion {
            
            private Companion() {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.cookiejarapps.android.smartcookieweb.addons.NotYetSupportedAddonActivity.NotYetSupportedAddonFragment create(@org.jetbrains.annotations.NotNull()
            java.util.ArrayList<mozilla.components.feature.addons.Addon> addons) {
                return null;
            }
        }
    }
}