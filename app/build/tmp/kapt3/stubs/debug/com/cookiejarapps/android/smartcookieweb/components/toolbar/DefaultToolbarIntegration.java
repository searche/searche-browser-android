package com.cookiejarapps.android.smartcookieweb.components.toolbar;

import java.lang.System;

@kotlinx.coroutines.ExperimentalCoroutinesApi()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001BY\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u0016\u00a8\u0006\u0017"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/DefaultToolbarIntegration;", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarIntegration;", "context", "Landroid/content/Context;", "toolbar", "Lmozilla/components/browser/toolbar/BrowserToolbar;", "toolbarMenu", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu;", "domainAutocompleteProvider", "Lmozilla/components/browser/domains/autocomplete/DomainAutocompleteProvider;", "historyStorage", "Lmozilla/components/concept/storage/HistoryStorage;", "lifecycleOwner", "Landroidx/lifecycle/LifecycleOwner;", "sessionId", "", "isPrivate", "", "interactor", "Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarViewInteractor;", "engine", "Lmozilla/components/concept/engine/Engine;", "(Landroid/content/Context;Lmozilla/components/browser/toolbar/BrowserToolbar;Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/ToolbarMenu;Lmozilla/components/browser/domains/autocomplete/DomainAutocompleteProvider;Lmozilla/components/concept/storage/HistoryStorage;Landroidx/lifecycle/LifecycleOwner;Ljava/lang/String;ZLcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserToolbarViewInteractor;Lmozilla/components/concept/engine/Engine;)V", "app_debug"})
public final class DefaultToolbarIntegration extends com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarIntegration {
    
    public DefaultToolbarIntegration(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.toolbar.BrowserToolbar toolbar, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.components.toolbar.ToolbarMenu toolbarMenu, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.domains.autocomplete.DomainAutocompleteProvider domainAutocompleteProvider, @org.jetbrains.annotations.NotNull()
    mozilla.components.concept.storage.HistoryStorage historyStorage, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LifecycleOwner lifecycleOwner, @org.jetbrains.annotations.Nullable()
    java.lang.String sessionId, boolean isPrivate, @org.jetbrains.annotations.NotNull()
    com.cookiejarapps.android.smartcookieweb.components.toolbar.BrowserToolbarViewInteractor interactor, @org.jetbrains.annotations.NotNull()
    mozilla.components.concept.engine.Engine engine) {
        super(null, null, null, null, false, null);
    }
}