package com.cookiejarapps.android.smartcookieweb.components.toolbar;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b6\u0018\u00002\u00020\u0001B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/components/toolbar/BrowserFragmentAction;", "Lmozilla/components/lib/state/Action;", "()V", "app_debug"})
public abstract class BrowserFragmentAction implements mozilla.components.lib.state.Action {
    
    private BrowserFragmentAction() {
        super();
    }
}