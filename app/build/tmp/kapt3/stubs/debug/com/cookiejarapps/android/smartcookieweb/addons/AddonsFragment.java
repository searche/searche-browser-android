package com.cookiejarapps.android.smartcookieweb.addons;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u008e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 @2\u00020\u00012\u00020\u0002:\u0001@B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0010\u0010\u001e\u001a\u00020\u00122\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0010\u0010\u001f\u001a\u00020\u000f2\u0006\u0010 \u001a\u00020!H\u0002J\n\u0010\"\u001a\u0004\u0018\u00010#H\u0002J\n\u0010$\u001a\u0004\u0018\u00010%H\u0002J+\u0010&\u001a\u00020\u00122\f\u0010\'\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010(\u001a\u00020!2\u0006\u0010)\u001a\u00020!H\u0001\u00a2\u0006\u0002\b*J\b\u0010+\u001a\u00020\u000fH\u0002J\u0010\u0010,\u001a\u00020\u00122\u0006\u0010-\u001a\u00020\nH\u0016J\u0018\u0010.\u001a\u00020\u00122\u0006\u0010/\u001a\u0002002\u0006\u00101\u001a\u000202H\u0016J$\u00103\u001a\u00020\u001d2\u0006\u00101\u001a\u0002042\b\u00105\u001a\u0004\u0018\u0001062\b\u00107\u001a\u0004\u0018\u000108H\u0016J\u0010\u00109\u001a\u00020\u00122\u0006\u0010-\u001a\u00020\nH\u0016J\u0016\u0010:\u001a\u00020\u00122\f\u0010;\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0016J\b\u0010<\u001a\u00020\u0012H\u0016J\u001a\u0010=\u001a\u00020\u00122\u0006\u0010\u001c\u001a\u00020\u001d2\b\u00107\u001a\u0004\u0018\u000108H\u0016J\u0010\u0010>\u001a\u00020\u00122\u0006\u0010-\u001a\u00020\nH\u0002J\u0010\u0010?\u001a\u00020\u00122\u0006\u0010-\u001a\u00020\nH\u0002R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\rR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00120\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00120\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006A"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/addons/AddonsFragment;", "Landroidx/fragment/app/Fragment;", "Lmozilla/components/feature/addons/ui/AddonsManagerAdapterDelegate;", "()V", "_binding", "Lcom/cookiejarapps/android/smartcookieweb/databinding/FragmentAddOnsBinding;", "adapter", "Lcom/cookiejarapps/android/smartcookieweb/addons/AddonsAdapter;", "addons", "", "Lmozilla/components/feature/addons/Addon;", "binding", "getBinding", "()Lcom/cookiejarapps/android/smartcookieweb/databinding/FragmentAddOnsBinding;", "isInstallationInProgress", "", "onConfirmInstallationButtonClicked", "Lkotlin/Function2;", "", "onConfirmPermissionButtonClicked", "Lkotlin/Function1;", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "scope", "Lkotlinx/coroutines/CoroutineScope;", "spinner", "Landroid/widget/Spinner;", "bindRecyclerView", "rootView", "Landroid/view/View;", "bindSpinner", "filterAddonByQuery", "query", "", "findPreviousInstallationDialogFragment", "Lmozilla/components/feature/addons/ui/AddonInstallationDialogFragment;", "findPreviousPermissionDialogFragment", "Lmozilla/components/feature/addons/ui/PermissionsDialogFragment;", "installAddonById", "supportedAddons", "id", "url", "installAddonById$app_debug", "isAlreadyADialogCreated", "onAddonItemClicked", "addon", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "inflater", "Landroid/view/MenuInflater;", "onCreateView", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onInstallAddonButtonClicked", "onNotYetSupportedSectionClicked", "unsupportedAddons", "onStart", "onViewCreated", "showInstallationDialog", "showPermissionDialog", "Companion", "app_debug"})
public final class AddonsFragment extends androidx.fragment.app.Fragment implements mozilla.components.feature.addons.ui.AddonsManagerAdapterDelegate {
    private androidx.recyclerview.widget.RecyclerView recyclerView;
    private android.widget.Spinner spinner;
    private final kotlinx.coroutines.CoroutineScope scope = null;
    private com.cookiejarapps.android.smartcookieweb.addons.AddonsAdapter adapter;
    private java.util.List<mozilla.components.feature.addons.Addon> addons;
    private com.cookiejarapps.android.smartcookieweb.databinding.FragmentAddOnsBinding _binding;
    private final kotlin.jvm.functions.Function2<mozilla.components.feature.addons.Addon, java.lang.Boolean, kotlin.Unit> onConfirmInstallationButtonClicked = null;
    private final kotlin.jvm.functions.Function1<mozilla.components.feature.addons.Addon, kotlin.Unit> onConfirmPermissionButtonClicked = null;
    
    /**
     * Whether or not an add-on installation is in progress.
     */
    private boolean isInstallationInProgress = false;
    @org.jetbrains.annotations.NotNull()
    public static final com.cookiejarapps.android.smartcookieweb.addons.AddonsFragment.Companion Companion = null;
    private static final java.lang.String PERMISSIONS_DIALOG_FRAGMENT_TAG = "ADDONS_PERMISSIONS_DIALOG_FRAGMENT";
    private static final java.lang.String INSTALLATION_DIALOG_FRAGMENT_TAG = "ADDONS_INSTALLATION_DIALOG_FRAGMENT";
    
    public AddonsFragment() {
        super();
    }
    
    private final com.cookiejarapps.android.smartcookieweb.databinding.FragmentAddOnsBinding getBinding() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View rootView, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onStart() {
    }
    
    @java.lang.Override()
    public void onCreateOptionsMenu(@org.jetbrains.annotations.NotNull()
    android.view.Menu menu, @org.jetbrains.annotations.NotNull()
    android.view.MenuInflater inflater) {
    }
    
    private final boolean filterAddonByQuery(java.lang.String query) {
        return false;
    }
    
    private final void bindSpinner(android.view.View rootView) {
    }
    
    private final void bindRecyclerView(android.view.View rootView) {
    }
    
    @androidx.annotation.VisibleForTesting()
    public final void installAddonById$app_debug(@org.jetbrains.annotations.NotNull()
    java.util.List<mozilla.components.feature.addons.Addon> supportedAddons, @org.jetbrains.annotations.NotNull()
    java.lang.String id, @org.jetbrains.annotations.NotNull()
    java.lang.String url) {
    }
    
    @java.lang.Override()
    public void onAddonItemClicked(@org.jetbrains.annotations.NotNull()
    mozilla.components.feature.addons.Addon addon) {
    }
    
    @java.lang.Override()
    public void onInstallAddonButtonClicked(@org.jetbrains.annotations.NotNull()
    mozilla.components.feature.addons.Addon addon) {
    }
    
    @java.lang.Override()
    public void onNotYetSupportedSectionClicked(@org.jetbrains.annotations.NotNull()
    java.util.List<mozilla.components.feature.addons.Addon> unsupportedAddons) {
    }
    
    private final boolean isAlreadyADialogCreated() {
        return false;
    }
    
    private final mozilla.components.feature.addons.ui.PermissionsDialogFragment findPreviousPermissionDialogFragment() {
        return null;
    }
    
    private final mozilla.components.feature.addons.ui.AddonInstallationDialogFragment findPreviousInstallationDialogFragment() {
        return null;
    }
    
    private final void showPermissionDialog(mozilla.components.feature.addons.Addon addon) {
    }
    
    private final void showInstallationDialog(mozilla.components.feature.addons.Addon addon) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/addons/AddonsFragment$Companion;", "", "()V", "INSTALLATION_DIALOG_FRAGMENT_TAG", "", "PERMISSIONS_DIALOG_FRAGMENT_TAG", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}