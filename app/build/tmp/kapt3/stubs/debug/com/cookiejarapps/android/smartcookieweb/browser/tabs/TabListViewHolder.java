package com.cookiejarapps.android.smartcookieweb.browser.tabs;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J(\u0010\"\u001a\u00020#2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010$\u001a\u00020%2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010&\u001a\u00020\'H\u0016J\r\u0010(\u001a\u00020#H\u0001\u00a2\u0006\u0002\b)J\r\u0010*\u001a\u00020#H\u0001\u00a2\u0006\u0002\b+J\u0010\u0010,\u001a\u00020#2\u0006\u0010-\u001a\u00020%H\u0016R\u001c\u0010\u0005\u001a\u00020\u00068\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\b\u0007\u0010\b\u001a\u0004\b\t\u0010\nR\u001e\u0010\u000b\u001a\u0004\u0018\u00010\f8\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\b\r\u0010\b\u001a\u0004\b\u000e\u0010\u000fR&\u0010\u0010\u001a\u0004\u0018\u00010\u00118\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\b\u0012\u0010\b\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001c\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001c\u0010\u001d\u001a\u00020\u001e8\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\b\u001f\u0010\b\u001a\u0004\b \u0010!\u00a8\u0006."}, d2 = {"Lcom/cookiejarapps/android/smartcookieweb/browser/tabs/TabListViewHolder;", "Lmozilla/components/browser/tabstray/TabViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "closeView", "Landroidx/appcompat/widget/AppCompatImageButton;", "getCloseView$app_debug$annotations", "()V", "getCloseView$app_debug", "()Landroidx/appcompat/widget/AppCompatImageButton;", "iconView", "Landroid/widget/ImageView;", "getIconView$app_debug$annotations", "getIconView$app_debug", "()Landroid/widget/ImageView;", "styling", "Lmozilla/components/browser/tabstray/TabsTrayStyling;", "getStyling$app_debug$annotations", "getStyling$app_debug", "()Lmozilla/components/browser/tabstray/TabsTrayStyling;", "setStyling$app_debug", "(Lmozilla/components/browser/tabstray/TabsTrayStyling;)V", "tab", "Lmozilla/components/browser/state/state/TabSessionState;", "getTab", "()Lmozilla/components/browser/state/state/TabSessionState;", "setTab", "(Lmozilla/components/browser/state/state/TabSessionState;)V", "titleView", "Landroid/widget/TextView;", "getTitleView$app_debug$annotations", "getTitleView$app_debug", "()Landroid/widget/TextView;", "bind", "", "isSelected", "", "delegate", "Lmozilla/components/browser/tabstray/TabsTray$Delegate;", "showItemAsNotSelected", "showItemAsNotSelected$app_debug", "showItemAsSelected", "showItemAsSelected$app_debug", "updateSelectedTabIndicator", "showAsSelected", "app_debug"})
public final class TabListViewHolder extends mozilla.components.browser.tabstray.TabViewHolder {
    @org.jetbrains.annotations.Nullable()
    private final android.widget.ImageView iconView = null;
    @org.jetbrains.annotations.NotNull()
    private final android.widget.TextView titleView = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.appcompat.widget.AppCompatImageButton closeView = null;
    @org.jetbrains.annotations.Nullable()
    private mozilla.components.browser.state.state.TabSessionState tab;
    @org.jetbrains.annotations.Nullable()
    private mozilla.components.browser.tabstray.TabsTrayStyling styling;
    
    public TabListViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.View itemView) {
        super(null);
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.ImageView getIconView$app_debug() {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getIconView$app_debug$annotations() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getTitleView$app_debug() {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getTitleView$app_debug$annotations() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.appcompat.widget.AppCompatImageButton getCloseView$app_debug() {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getCloseView$app_debug$annotations() {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public mozilla.components.browser.state.state.TabSessionState getTab() {
        return null;
    }
    
    @java.lang.Override()
    public void setTab(@org.jetbrains.annotations.Nullable()
    mozilla.components.browser.state.state.TabSessionState p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final mozilla.components.browser.tabstray.TabsTrayStyling getStyling$app_debug() {
        return null;
    }
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getStyling$app_debug$annotations() {
    }
    
    public final void setStyling$app_debug(@org.jetbrains.annotations.Nullable()
    mozilla.components.browser.tabstray.TabsTrayStyling p0) {
    }
    
    @java.lang.Override()
    public void bind(@org.jetbrains.annotations.NotNull()
    mozilla.components.browser.state.state.TabSessionState tab, boolean isSelected, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.tabstray.TabsTrayStyling styling, @org.jetbrains.annotations.NotNull()
    mozilla.components.browser.tabstray.TabsTray.Delegate delegate) {
    }
    
    @java.lang.Override()
    public void updateSelectedTabIndicator(boolean showAsSelected) {
    }
    
    @androidx.annotation.VisibleForTesting()
    public final void showItemAsSelected$app_debug() {
    }
    
    @androidx.annotation.VisibleForTesting()
    public final void showItemAsNotSelected$app_debug() {
    }
}